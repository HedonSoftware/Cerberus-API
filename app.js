
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

// setting global constants

ROOT_PATH    = __dirname;
LIBRARY_PATH = ROOT_PATH + '/lib';

module.exports = (process.env['NODE_ENV'] === "COVERAGE")
  ? require('./lib-cov/express')
  : require('./lib/express');
