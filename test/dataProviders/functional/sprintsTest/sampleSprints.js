
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var sprint1 = {
  id: "fbb6f47e-261c-4b55-95d2-92374595007f",
  name: "Sprint 0.0.1",
  description: "Sprint 0.0.1",
  startDate: "2014-10-02T00:00:00.000Z",
  endDate: "2014-10-16T00:00:00.000Z",
  status: "active",
  createdAt: "2015-08-23T16:13:51.000Z",
  updatedAt: null
};

var sprint2 = {
  id: "3f2213f2-0c04-44bf-907a-9f167404adff",
  name: "Sprint 0.0.2",
  description: "Sprint 0.0.2",
  startDate: "2014-10-17T00:00:00.000Z",
  endDate: "2014-10-31T00:00:00.000Z",
  status: "active",
  createdAt: "2015-08-23T16:13:51.000Z",
  updatedAt: null
};

var sprint3 = {
  id: "a239fb8d-a632-4f6e-91b1-16cbaa519800",
  name: "Sprint 1.0.0",
  description: "First post-release sprint",
  startDate: "2014-10-05T00:00:00.000Z",
  endDate: "2014-10-19T00:00:00.000Z",
  status: "active",
  createdAt: "2015-08-23T16:13:51.000Z",
  updatedAt: null
};

var sprint4 = {
  id: "72db1cef-4e21-4946-a16e-1e0690d69dc7",
  name: "Sprint 1.0.1",
  description: "Second post-release sprint",
  startDate: "2014-10-20T00:00:00.000Z",
  endDate: "2014-11-03T00:00:00.000Z",
  status: "active",
  createdAt: "2015-08-23T16:13:51.000Z",
  updatedAt: null
};

let project = {
  id: "460ad418-ac09-44bb-9941-a2a645032c01",
  name: "Galaxy",
  code: "GAL",
  description: "Galaxy is a massive project ...",
  boards: {
    "10a98db6-fa2f-458f-ba88-4406bd389fe8": {
      id: "10a98db6-fa2f-458f-ba88-4406bd389fe8",
      name: "Backlog",
      description: "Ideas incubator",
      type: "backlog",
      lanes: {
        "4c573a61-1b3f-413a-8b2e-e224c3e15e7f": {
          id: "4c573a61-1b3f-413a-8b2e-e224c3e15e7f",
          name: "Should",
          description: "Should be done in next sprint",
          sequenceNumber: 2,
          status: "active",
          tags: [
            "db4958e1-ef81-4213-a68a-97f2ba77d7cc"
          ]
        },
        "7e0b33d7-6c35-4727-b382-4b66b8114032": {
          id: "7e0b33d7-6c35-4727-b382-4b66b8114032",
          name: "Could",
          description: "Could be done in next sprint",
          sequenceNumber: 3,
          status: "active",
          tags: [
            "0397c137-94ac-4a1e-8b98-83b603f15a29"
          ]
        },
        "972138be-7fd4-4ea6-aca0-7d03fc5e6242": {
          id: "972138be-7fd4-4ea6-aca0-7d03fc5e6242",
          name: "Won\"t",
          description: "Won\"t be done in next sprint",
          sequenceNumber: 4,
          status: "active",
          tags: [
            "9917c08c-ef33-4ab1-9a2a-8d7a4ba69a33"
          ]
        },
        "9f2dc910-1713-43e2-a6e2-ce87409bdbb4": {
          id: "9f2dc910-1713-43e2-a6e2-ce87409bdbb4",
          name: "Must",
          description: "Must be done in next sprint",
          sequenceNumber: 1,
          status: "active",
          tags: [
            "bdc24220-4bb1-4a9c-bdfa-9752992227e7"
          ]
        },
      },
      status: "active"
    },
    "37680c5c-9540-446a-90c3-152fb1e451d1": {
      id: "37680c5c-9540-446a-90c3-152fb1e451d1",
      name: "Design",
      description: "Graphic design team's board",
      type: "action",
      lanes: {
        "32da7875-56fe-4e29-b9fc-cb84eb9eb103": {
          id: "32da7875-56fe-4e29-b9fc-cb84eb9eb103",
          name: "To do",
          description: "To be started",
          sequenceNumber: 1,
          status: "active",
          tags: [
            "05165da4-3950-48d9-bffb-5aff67edb47b"
          ]
        },
        "53238611-1b1e-4324-a888-b1156212990b": {
          id: "53238611-1b1e-4324-a888-b1156212990b",
          name: "In progress",
          description: "Designing right now",
          sequenceNumber: 2,
          status: "active",
          tags: [
            "87d87f31-ce2b-42a9-8155-b923c548f922"
          ]
        },
        "9fdc3421-eb91-43c7-97b1-388e0031f612": {
          id: "9fdc3421-eb91-43c7-97b1-388e0031f612",
          name: "Done",
          description: "Ended",
          sequenceNumber: 3,
          status: "active",
          tags: [
            "06580431-9b48-4c79-baf6-07259f3975a7"
          ]
        }
      },
      status: "active"
    },
    "3c08ed3c-8257-47e3-804b-fe74b84d9704": {
      id: "3c08ed3c-8257-47e3-804b-fe74b84d9704",
      name: "Development",
      description: "Development team's board",
      type: "action",
      sprints: {
        [sprint1.id]: sprint1,
        [sprint2.id]: sprint2,
        [sprint3.id]: sprint3,
        [sprint4.id]: sprint4
      },
      status: "active"
    },
    "c0dcbf69-f571-49ec-9c47-2d679f678813": {
      id: "c0dcbf69-f571-49ec-9c47-2d679f678813",
      name: "Overview",
      description: "Management team's overview board",
      type: "overview",
      lanes: {
        "191778ef-8824-4fed-9ecf-92c2ef0e6bf5": {
          id: "191778ef-8824-4fed-9ecf-92c2ef0e6bf5",
          name: "Finished",
          description: "Finished",
          sequenceNumber: 3,
          status: "active",
          tags: [
            "06580431-9b48-4c79-baf6-07259f3975a7", // design - done
            "17dde46d-819c-40f5-856e-b94de6d9c40c"  // development - done
          ]
        },
        "5a88bfc1-f3fd-46b2-a83d-a62c87f5c504": {
          id: "5a88bfc1-f3fd-46b2-a83d-a62c87f5c504",
          name: "Progressing",
          description: "Progressing",
          sequenceNumber: 2,
          status: "active",
          tags: [
            "87d87f31-ce2b-42a9-8155-b923c548f922", // design -> in progress
            "631868e9-2cdb-4676-85ec-e8c26a770b47", // development -> in progress
            "aa05b7f8-3117-4611-a73c-c3f62a4950d3", // development -> code review
            "05caba71-c8dd-4c57-9883-d5ae5632198c"  // development -> acceptance testing
          ]
        },
        "a2bcc6c0-e6df-4d0e-bb73-ca39bf8b9adc": {
          id: "a2bcc6c0-e6df-4d0e-bb73-ca39bf8b9adc",
          name: "Waiting",
          description: "Waiting to be started",
          sequenceNumber: 1,
          status: "active",
          tags: [
            "05165da4-3950-48d9-bffb-5aff67edb47b", // design - to do
            "6552011d-ae06-482d-98fc-abf49f220012"  // development - to do
          ]
        }
      },
      status: "active"
    }
  },
  status: "active",
  createdAt: "2015-08-22T05:29:22.000Z",
  updatedAt: null
};

module.exports = {
  "project": project,
  "sprint1": sprint1,
  "sprint2": sprint2,
  "sprint3": sprint3,
  "sprint4": sprint4,
  "default": [
    sprint1,
    sprint2,
    sprint3,
    sprint4
  ],
  "orderByStartDate": [
    sprint1,
    sprint3,
    sprint2,
    sprint4
  ],
  "limitedAndOrdered": [
    sprint1,
    sprint3,
    sprint2
  ],
  "partial1": [
    {
      id: sprint3.id,
      name: sprint3.name,
      description: sprint3.description,
      startDate: sprint3.startDate
    }
  ],
  "partial2": [
    {
      endDate: sprint4.endDate,
      status: sprint4.status,
      createdAt: sprint4.createdAt,
      updatedAt: sprint4.updatedAt
    }
  ],

  // ---------------------------------
  // ----- GETTING SINGLE RECORD -----
  // ---------------------------------

  singleById: sprint4
};
