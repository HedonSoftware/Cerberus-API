
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = function() {

  return r.table("Project").delete().then(function () {

    var project1 = {
      id: "5de514c2-49f6-429e-a478-36bdc206cab4",
      name: "Universe",
      code: "UNI",
      description: "Universe is project separated from Galaxy ...",
      boards: {
        "87238ac5-b919-4156-aac9-fc62508faf37": {
          id: "87238ac5-b919-4156-aac9-fc62508faf37",
          name: "API",
          description: "API team's board",
          type: "action",
          tags: [
            "a239fb8d-a632-4f6e-91b1-16cbaa519800",
            "72db1cef-4e21-4946-a16e-1e0690d69dc7"
          ],
          lanes: {
            "97fe1a52-4bb5-46b4-91f0-1f1940c5fd2d": {
              id: "97fe1a52-4bb5-46b4-91f0-1f1940c5fd2d",
              name: "To do",
              description: "To be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "705b5f6a-2448-4309-98ed-47acc0806886"
              ]
            },
            "8d05d79e-bad2-4f59-82bb-f1c04750bb95": {
              id: "8d05d79e-bad2-4f59-82bb-f1c04750bb95",
              name: "Development",
              description: "Developing right now",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "d91a31c9-aa3b-458d-9143-9fdfaf088181"
              ]
            },
            "9441180a-961f-47b2-8d56-ac7fcff60bb5": {
              id: "9441180a-961f-47b2-8d56-ac7fcff60bb5",
              name: "Review",
              description: "Code review",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "cc1ed2c3-a2f9-4d93-9e93-859140f0e6e7"
              ]
            },
            "2e952528-f3d1-4257-8bdd-520912982a7c": {
              id: "2e952528-f3d1-4257-8bdd-520912982a7c",
              name: "Done",
              description: "Ended",
              sequenceNumber: 4,
              status: "active",
              tags: [
                "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8"
              ]
            }
          },
          status: "active"
        },
        "dccd083d-73a7-47ba-ab4c-051ea4e1921b": {
          id: "dccd083d-73a7-47ba-ab4c-051ea4e1921b",
          name: "Backlog",
          description: "Tickets incubator",
          type: "backlog",
          lanes: {
            "7862aeba-61ab-4d49-9405-7caefb86e3b7": {
              id: "7862aeba-61ab-4d49-9405-7caefb86e3b7",
              name: "Waiting",
              description: "Waiting to be added to sprint",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "e0270979-9ece-4b80-9a24-bed0aded5988"
              ]
            }
          },
          status: "active"
        },
        "f5991337-dfc0-424c-b6f1-34d0fd894b52": {
          id: "f5991337-dfc0-424c-b6f1-34d0fd894b52",
          name: "Frontend",
          description: "Frontend team's board",
          type: "action",
          tags: [
            "fbb6f47e-261c-4b55-95d2-92374595007f",
            "3f2213f2-0c04-44bf-907a-9f167404adff"
          ],
          lanes: {
            "062a2472-ab9b-4d55-b243-b5ea34f03411": {
              id: "062a2472-ab9b-4d55-b243-b5ea34f03411",
              name: "To do",
              description: "To be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "da2f64bd-1d70-4415-a232-95b2f35366ba"
              ]
            },
            "bf99106c-2686-4dcc-9d29-4e4509551a4e": {
              id: "bf99106c-2686-4dcc-9d29-4e4509551a4e",
              name: "In progress",
              description: "Started",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "e12ba8d6-b125-4692-94bb-616f7fbc8ab5"
              ]
            },
            "d0e33e0a-9e5c-4f05-9eea-3c98bc4b4c1a": {
              id: "d0e33e0a-9e5c-4f05-9eea-3c98bc4b4c1a",
              name: "Done",
              description: "Ended",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "261cfeb0-8c47-48ac-acfa-6a59a140235e"
              ]
            }
          },
          status: "active"
        }
      },
      status: "active",
      createdAt: new Date("2011-11-11T15:41:10.000Z"),
      updatedAt: null
    };

    var project2 = {
      id: "460ad418-ac09-44bb-9941-a2a645032c01",
      name: "Galaxy",
      code: "GAL",
      description: "Galaxy is a massive project ...",
      boards: {
        "37680c5c-9540-446a-90c3-152fb1e451d1": {
          id: "37680c5c-9540-446a-90c3-152fb1e451d1",
          name: "Design",
          description: "Graphic design team's board",
          type: "action",
          lanes: {
            "32da7875-56fe-4e29-b9fc-cb84eb9eb103": {
              id: "32da7875-56fe-4e29-b9fc-cb84eb9eb103",
              name: "To do",
              description: "To be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "05165da4-3950-48d9-bffb-5aff67edb47b"
              ]
            },
            "53238611-1b1e-4324-a888-b1156212990b": {
              id: "53238611-1b1e-4324-a888-b1156212990b",
              name: "In progress",
              description: "Designing right now",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "87d87f31-ce2b-42a9-8155-b923c548f922"
              ]
            },
            "9fdc3421-eb91-43c7-97b1-388e0031f612": {
              id: "9fdc3421-eb91-43c7-97b1-388e0031f612",
              name: "Done",
              description: "Ended",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "06580431-9b48-4c79-baf6-07259f3975a7"
              ]
            }
          },
          status: "active"
        },
        "3c08ed3c-8257-47e3-804b-fe74b84d9704": {
          id: "3c08ed3c-8257-47e3-804b-fe74b84d9704",
          name: "Development",
          description: "Development team's board",
          type: "action",
          lanes: {
            "4062c110-a173-4a80-88fa-2fc95c64106b": {
              id: "4062c110-a173-4a80-88fa-2fc95c64106b",
              name: "To do",
              description: "To be developed",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "6552011d-ae06-482d-98fc-abf49f220012"
              ]
            },
            "0b3360c9-9f2e-4409-b69b-931d5c36533d": {
              id: "0b3360c9-9f2e-4409-b69b-931d5c36533d",
              name: "In progress",
              description: "Developing..",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "631868e9-2cdb-4676-85ec-e8c26a770b47"
              ]
            },
            "25fe4e2e-7646-4121-8d20-684389e9fbca": {
              id: "25fe4e2e-7646-4121-8d20-684389e9fbca",
              name: "Code review",
              description: "Peer review",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "aa05b7f8-3117-4611-a73c-c3f62a4950d3"
              ]
            },
            "0d33391e-0402-4855-a0b3-d447dc7586c6": {
              id: "0d33391e-0402-4855-a0b3-d447dc7586c6",
              name: "Acceptance testing",
              description: "User acceptance testing",
              sequenceNumber: 4,
              status: "active",
              tags: [
                "05caba71-c8dd-4c57-9883-d5ae5632198c"
              ]
            },
            "094f1eb5-9d6c-4b8f-8279-ce97d2df2fdd": {
              id: "094f1eb5-9d6c-4b8f-8279-ce97d2df2fdd",
              name: "Done",
              description: "Finished/Approved",
              sequenceNumber: 5,
              status: "active",
              tags: [
                "17dde46d-819c-40f5-856e-b94de6d9c40c"
              ]
            }
          },
          status: "active"
        },
        "c0dcbf69-f571-49ec-9c47-2d679f678813": {
          id: "c0dcbf69-f571-49ec-9c47-2d679f678813",
          name: "Overview",
          description: "Management team's overview board",
          type: "overview",
          lanes: {
            "a2bcc6c0-e6df-4d0e-bb73-ca39bf8b9adc": {
              id: "a2bcc6c0-e6df-4d0e-bb73-ca39bf8b9adc",
              name: "Waiting",
              description: "Waiting to be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "05165da4-3950-48d9-bffb-5aff67edb47b", // design - to do
                "6552011d-ae06-482d-98fc-abf49f220012"  // development - to do
              ]
            },
            "5a88bfc1-f3fd-46b2-a83d-a62c87f5c504": {
              id: "5a88bfc1-f3fd-46b2-a83d-a62c87f5c504",
              name: "Progressing",
              description: "Progressing",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "87d87f31-ce2b-42a9-8155-b923c548f922", // design -> in progress
                "631868e9-2cdb-4676-85ec-e8c26a770b47", // development -> in progress
                "aa05b7f8-3117-4611-a73c-c3f62a4950d3", // development -> code review
                "05caba71-c8dd-4c57-9883-d5ae5632198c"  // development -> acceptance testing
              ]
            },
            "191778ef-8824-4fed-9ecf-92c2ef0e6bf5": {
              id: "191778ef-8824-4fed-9ecf-92c2ef0e6bf5",
              name: "Finished",
              description: "Finished",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "06580431-9b48-4c79-baf6-07259f3975a7", // design - done
                "17dde46d-819c-40f5-856e-b94de6d9c40c"  // development - done
              ]
            }
          },
          status: "active"
        },
        "10a98db6-fa2f-458f-ba88-4406bd389fe8": {
          id: "10a98db6-fa2f-458f-ba88-4406bd389fe8",
          name: "Backlog",
          description: "Ideas incubator",
          type: "backlog",
          lanes: {
            "9f2dc910-1713-43e2-a6e2-ce87409bdbb4": {
              id: "9f2dc910-1713-43e2-a6e2-ce87409bdbb4",
              name: "Must",
              description: "Must be done in next sprint",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "bdc24220-4bb1-4a9c-bdfa-9752992227e7"
              ]
            },
            "4c573a61-1b3f-413a-8b2e-e224c3e15e7f": {
              id: "4c573a61-1b3f-413a-8b2e-e224c3e15e7f",
              name: "Should",
              description: "Should be done in next sprint",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "db4958e1-ef81-4213-a68a-97f2ba77d7cc"
              ]
            },
            "7e0b33d7-6c35-4727-b382-4b66b8114032": {
              id: "7e0b33d7-6c35-4727-b382-4b66b8114032",
              name: "Could",
              description: "Could be done in next sprint",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "0397c137-94ac-4a1e-8b98-83b603f15a29"
              ]
            },
            "972138be-7fd4-4ea6-aca0-7d03fc5e6242": {
              id: "972138be-7fd4-4ea6-aca0-7d03fc5e6242",
              name: "Won\"t",
              description: "Won\"t be done in next sprint",
              sequenceNumber: 4,
              status: "active",
              tags: [
                "9917c08c-ef33-4ab1-9a2a-8d7a4ba69a33"
              ]
            }
          },
          status: "active"
        }
      },
      status: "active",
      createdAt: new Date("2015-08-22T05:29:22.000Z"),
      updatedAt: null
    };

    var project3 = {
      id: "d11f2106-480d-40c5-be32-e6a253be2078",
      name: "Texas",
      code: "Texas is state project",
      description: "Texas is a gov project ...",
      boards: {
        "439b115f-f92b-4d35-9be7-f776fc8fd828": {
          id: "439b115f-f92b-4d35-9be7-f776fc8fd828",
          name: "Backlog",
          description: "Ideas incubator",
          type: "backlog",
          lanes: {
            "db1ee141-8794-4dfa-a759-21f21e2f203a": {
              id: "db1ee141-8794-4dfa-a759-21f21e2f203a",
              name: "Maybe",
              description: "It will maybe done in next sprint",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "0397c137-94ac-4a1e-8b98-83b603f15a29"
              ]
            },
            "b1ec4bef-fe2e-4b0d-b1f2-aaab3855dd17": {
              id: "b1ec4bef-fe2e-4b0d-b1f2-aaab3855dd17",
              name: "Will",
              description: "Will be done",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "bdc24220-4bb1-4a9c-bdfa-9752992227e7"
              ]
            },
            "ef6c99dc-088e-417b-8c01-fd4ab4ff1387": {
              id: "ef6c99dc-088e-417b-8c01-fd4ab4ff1387",
              name: "Won't",
              description: "Won't be done in next sprint",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "9917c08c-ef33-4ab1-9a2a-8d7a4ba69a33"
              ]
            }
          },
          status: "active"
        },
        "c33d5788-73de-4045-8a7a-e50ace1ee9cc": {
          id: "c33d5788-73de-4045-8a7a-e50ace1ee9cc",
          name: "Dev",
          description: "Dev team's board",
          type: "action",
          lanes: {
            "bf262199-8038-44c9-9fdf-85efec882efa": {
              id: "bf262199-8038-44c9-9fdf-85efec882efa",
              name: "Done",
              description: "Finished",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "17dde46d-819c-40f5-856e-b94de6d9c40c"
              ]
            },
            "228ef78e-ea44-4f5a-a6f2-040a690b99a1": {
              id: "228ef78e-ea44-4f5a-a6f2-040a690b99a1",
              name: "In progress",
              description: "Under development..",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "631868e9-2cdb-4676-85ec-e8c26a770b47"
              ]
            },
            "9ebd5345-7b84-4f2d-9581-eea3e8f13586": {
              id: "9ebd5345-7b84-4f2d-9581-eea3e8f13586",
              name: "To do",
              description: "Waiting to be picked up",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "6552011d-ae06-482d-98fc-abf49f220012"
              ]
            }
          },
          status: "active"
        }
      },
      status: "active",
      createdAt: new Date("2005-04-11T21:32:12.000Z"),
      updatedAt: null
    };

    var project4 = {
      id: "11cefac7-ebad-4b3f-b08d-dcbb8e443ed9",
      name: "Trex",
      code: "Trex is state project",
      description: "Trex is a planning project ...",
      boards: {
        "81baa8f4-8449-4002-80d6-cc68f389bcaa": {
          id: "81baa8f4-8449-4002-80d6-cc68f389bcaa",
          name: "Planning",
          description: "Product owner's board",
          type: "action",
          lanes: {},
          status: "active"
        },
        "91f6eea4-de45-42af-b83d-da19bb857c3e": {
          id: "91f6eea4-de45-42af-b83d-da19bb857c3e",
          name: "Backlog",
          description: "Ideas incubator",
          type: "backlog",
          lanes: {
            "12d57bd7-cc4f-482e-a00b-3dbe35a98dac": {
              id: "12d57bd7-cc4f-482e-a00b-3dbe35a98dac",
              name: "Waiting",
              description: "Waiting to be validated",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "bdc24220-4bb1-4a9c-bdfa-9752992227e7"
              ]
            }
          },
          status: "active"
        }
      },
      status: "active",
      createdAt: new Date("2009-11-21T04:51:25.000Z"),
      updatedAt: null
    };

    var project5 = {
      id: "7ab74e35-d06d-48cf-a320-fe1efb5b4275",
      name: "Atom",
      code: "ATM",
      description: "Atom is a chemical project ...",
      boards: {},
      status: "active",
      createdAt: new Date("2013-02-17T09:42:43.000Z"),
      updatedAt: null
    };

    let projects = [
      project1,
      project2,
      project3,
      project4,
      project5
    ];

    return r.table("Project").insert(projects).run();

  });
};
