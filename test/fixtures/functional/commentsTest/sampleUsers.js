
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = function() {

  return r.table("User").delete().then(function () {

    var users = [
        {
          id: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
          username: "unknown",
          password: "",
          email: "",
          firstName: "Unknown",
          middleName: null,
          lastName: "Unknown",
          avatar: "/static/images/avatars/00.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T04:23:12.000Z",
          updatedAt: null
        },
        {
          id: "76039cc1-96eb-4721-81b3-211b6979d018",
          username: "branco.rosenfield",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "branco.rosenfield@hedonsoftware.com",
          firstName: "Branco",
          middleName: null,
          lastName: "Rosenfield",
          avatar: "/static/images/avatars/05.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T17:54:17.000Z",
          updatedAt: null
        },
        {
          id: "d7885ec6-ece3-427a-b15c-c114d27d2f82",
          username: "jerrie.mcginnis",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "jerrie.mcginnis@hedonsoftware.com",
          firstName: "Jerrie",
          middleName: null,
          lastName: "Mcginnis",
          avatar: "/static/images/avatars/86.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T07:34:11.000Z",
          updatedAt: null
        },
        {
          id: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
          username: "horgan.madore",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "horgan.madore@hedonsoftware.com",
          firstName: "Horgan",
          middleName: "Fearnley",
          lastName: "Madore",
          avatar: "/static/images/avatars/11.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T07:42:47.000Z",
          updatedAt: null
        },
        {
          id: "b808972a-ec28-4d69-a194-d1e05c5ff0e8",
          username: "mina.hernadi",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "mina.hernadi@hedonsoftware.com",
          firstName: "Mina",
          middleName: "D",
          lastName: "Hernadi",
          avatar: "/static/images/avatars/04.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T22:24:15.000Z",
          updatedAt: null
        },
        {
          id: "4c374646-ff38-4a8b-adf6-0bf0e3723c75",
          username: "kristy.daniel",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "kristy.daniel@hedonsoftware.com",
          firstName: "Kristy",
          middleName: null,
          lastName: "Daniel",
          avatar: "/static/images/avatars/35.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T05:34:12.000Z",
          updatedAt: null
        },
        {
          id: "1d58b82e-eeec-4065-9386-5309a6aede48",
          username: "edelman.schaefer",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "edelman.schaefer@hedonsoftware.com",
          firstName: "Edelman",
          middleName: null,
          lastName: "Schaefer",
          avatar: "/static/images/avatars/10.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T02:54:32.000Z",
          updatedAt: null
        },
        {
          id: "dbfdc23a-d442-4970-a214-ff14f0d1875c",
          username: "julius.martinez",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "julius.martinez@hedonsoftware.com",
          firstName: "Julius",
          middleName: null,
          lastName: "Martinez",
          avatar: "/static/images/avatars/34.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T04:25:17.000Z",
          updatedAt: null
        },
        {
          id: "7b13132f-6bc2-4841-b223-e726c73a5013",
          username: "zaitlin.soltan",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "zaitlin.soltan@hedonsoftware.com",
          firstName: "Zaitlin",
          middleName: null,
          lastName: "Soltan",
          avatar: "/static/images/avatars/08.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T02:43:12.000Z",
          updatedAt: null
        },
        {
          id: "8e6002c6-e5ef-4552-8bfe-f785a810767a",
          username: "blanchette.chiozzi",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "blanchette.chiozzi@hedonsoftware.com",
          firstName: "Blanchette",
          middleName: null,
          lastName: "Chiozzi",
          avatar: "/static/images/avatars/16.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T10:10:23.000Z",
          updatedAt: null
        },
        {
          id: "254bf500-12f8-406d-98ea-ba87bac3d148",
          username: "fernald.schimmel",
          password: "632144b822d2e62e1cfa7857cf8730d480b99026",
          email: "fernald.schimmel@hedonsoftware.com",
          firstName: "Fernald",
          middleName: null,
          lastName: "Schimmel",
          avatar: "/static/images/avatars/01.png",
          status: "active",
          roleId: "d16db438-35a9-4dcc-946c-c2460aa7dda7",
          createdAt: "2015-08-22T23:16:34.000Z",
          updatedAt: null
        },
        {
          id: "4baf7c31-84a9-4fd5-9584-c401b0030f5b",
          username: "kieran.brouillard",
          password: "6accdad680d6b4bdd7261e5ba71ffd9c3a8a3ec0",
          email: "kieran.brouillard@hedonsoftware.com",
          firstName: "Kieran",
          middleName: null,
          lastName: "Brouillard",
          avatar: "/static/images/avatars/22.png",
          status: "active",
          roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
          createdAt: "2015-08-22T17:38:11.000Z",
          updatedAt: null
        },
    ];

    return r.table("User").insert(users).run();
  });
};
