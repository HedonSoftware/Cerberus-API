
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = function() {

  return r.table("Ticket").delete().then(function () {

    var tickets = [
      {
        id: "f65a5fe6-371a-4216-884a-ab61047ab3db",
        name: "Install Grunt Watch",
        description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven\"t used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you\"re familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-watch --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks(\"grunt-contrib-watch\");</pre><p></p>",
        storyPoints: 8,
        creatorId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
        assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
        workRecords: {
        },
        comments: {
          "ff0931ae-5e8d-4513-a43e-88ccdd0c88ec": {
            id: "ff0931ae-5e8d-4513-a43e-88ccdd0c88ec",
            text: "Dependency on ...",
            userId: "76039cc1-96eb-4721-81b3-211b6979d018",
            status: "active",
            createdAt: new Date("2015-08-22T21:34:32.000Z"),
            updatedAt: null
          }
        },
        tags: [
          "da2f64bd-1d70-4415-a232-95b2f35366ba", // Universe -> Frontend -> To do
          "3f2213f2-0c04-44bf-907a-9f167404adff", // Universe -> Frontend -> Sprint 0.0.2
        ],
        status: "active",
        createdAt: new Date("2015-08-22T21:34:32.000Z"),
        updatedAt: null
      },
      {
        id: "61ad71eb-0ca2-49af-b371-c16599207bbc",
        name: "Concatenate files",
        description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven\"t used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you\"re familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-concat --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks(\"grunt-contrib-concat\");</pre>",
        storyPoints: 3,
        creatorId: "d7885ec6-ece3-427a-b15c-c114d27d2f82",
        assigneeId: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
        workRecords: {
        },
        comments: {
          "7001f385-bcc7-412b-9629-648fa977b22a": {
            id: "7001f385-bcc7-412b-9629-648fa977b22a",
            text: "Should be marked as duplicate ...",
            userId: "b808972a-ec28-4d69-a194-d1e05c5ff0e8",
            status: "active",
            createdAt: new Date("2015-08-22T21:34:32.000Z"),
            updatedAt: null
          },
          "da517538-c7ae-4bd5-9242-71b2a9b98828": {
            id: "da517538-c7ae-4bd5-9242-71b2a9b98828",
            text: "Full regression tests are required ...",
            userId: "76039cc1-96eb-4721-81b3-211b6979d018",
            status: "active",
            createdAt: new Date("2015-08-22T21:34:32.000Z"),
            updatedAt: null
          },
          "753d282f-f03f-405d-b618-9c08dc9f30b7": {
            id: "753d282f-f03f-405d-b618-9c08dc9f30b7",
            text: "Already finished ...",
            userId: "76039cc1-96eb-4721-81b3-211b6979d018",
            status: "active",
            createdAt: "2016-11-04T08:26:41.000Z",
            updatedAt: null
          },
          "9dc06a42-27f2-4541-b123-2376e35e3c44": {
            id: "9dc06a42-27f2-4541-b123-2376e35e3c44",
            text: "To be deleted ...",
            userId: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
            status: "active",
            createdAt: "2014-03-27T15:26:47.000Z",
            updatedAt: null
          }
        },
        tags: [
          "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
          "a239fb8d-a632-4f6e-91b1-16cbaa519800", // Universe -> Api -> Sprint 1.0.0
        ],
        status: "active",
        createdAt: new Date("2015-08-22T21:34:32.000Z"),
        updatedAt: null
      },
      {
        id: "fbf750db-8d63-446e-ba8b-603f2d40c35d",
        name: "Compress CSS files",
        description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven\"t used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you\"re familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-cssmin --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks(\"grunt-contrib-cssmin\");</pre>",
        storyPoints: 13,
        creatorId: "4c374646-ff38-4a8b-adf6-0bf0e3723c75",
        assigneeId: "1d58b82e-eeec-4065-9386-5309a6aede48",
        workRecords: {
        },
        comments: {
        },
        tags: [
          "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
          "a239fb8d-a632-4f6e-91b1-16cbaa519800", // Universe -> Api -> Sprint 1.0.0
        ],
        status: "active",
        createdAt: new Date("2015-08-22T21:34:32.000Z"),
        updatedAt: null
      },
      {
        id: "4b9c8c26-9962-4351-b7f8-2e6e333c9ff5",
        name: "Minify images",
        description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven\"t used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you\"re familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-imagemin --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks(\"grunt-contrib-imagemin\");</pre>",
        storyPoints: 20,
        creatorId: "dbfdc23a-d442-4970-a214-ff14f0d1875c",
        assigneeId: "7b13132f-6bc2-4841-b223-e726c73a5013",
        workRecords: {
          "52cf7be4-bfde-4f4e-96f3-4b9cf83e654f": {
            id: "52cf7be4-bfde-4f4e-96f3-4b9cf83e654f",
            name: "development",
            description: "Adding minify images to build script",
            startDate: new Date("2015-07-15T00:00:00.000Z"),
            time: 120,
            status: "active",
            userId: "8e6002c6-e5ef-4552-8bfe-f785a810767a"
          }
        },
        comments: {
        },
        tags: [
          "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
          "72db1cef-4e21-4946-a16e-1e0690d69dc7", // Universe -> Api -> Sprint 1.0.1
        ],
        status: "active",
        createdAt: new Date("2015-08-22T21:34:32.000Z"),
        updatedAt: null
      },
      {
        id: "563655d2-be47-4efd-b2e7-ebc261d383d7",
        name: "Lint .scss files",
        description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt &gt;= 0.4.0 and scss-lint &gt;= 0.18.0.</p><p><br/></p><p>If you haven\"t used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you\"re familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-scss-lint --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks(\"grunt-scss-lint\");</pre>",
        storyPoints: 28,
        creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
        assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
        workRecords: {
          "917bf55e-91f6-42ef-b66c-991445891d14": {
            id: "917bf55e-91f6-42ef-b66c-991445891d14",
            name: "development",
            description: "Adding linting SCSS files to build script",
            startDate: new Date("2015-07-22T00:00:00.000Z"),
            time: 40,
            status: "active",
            userId: "76039cc1-96eb-4721-81b3-211b6979d018"
          },
          "c75f58e6-6572-46c9-ae68-bb40953c765a": {
            id: "c75f58e6-6572-46c9-ae68-bb40953c765a",
            name: "bugfixing",
            description: "Fixing linting SCSS files in build script",
            startDate: new Date("2015-07-22T00:00:00.000Z"),
            time: 20,
            status: "active",
            userId: "4baf7c31-84a9-4fd5-9584-c401b0030f5b"
          }
        },
        comments: {
        },
        tags: [
          "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
          // no sprints in backlog
        ],
        status: "active",
        createdAt: new Date("2015-08-22T21:34:32.000Z"),
        updatedAt: null
      }
    ];

    return r.table("Ticket").insert(tickets).run();
  });
};
