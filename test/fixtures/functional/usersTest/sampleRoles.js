
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = function() {

  return r.table("AclRole").delete().then(function () {

    var aclRoles = [
      {
        id: "40603aa2-5801-44c0-98b9-ab0be0987fcb",
        name: "user-user-X",
        description: "Admin User Account User-X",
        status: "active",
        createdAt: new Date("2010-09-24T14:50:36.000Z"),
        updatedAt: new Date("2012-05-24T00:01:01.000Z")
      },
      {
        id: "096b009d-015c-4f23-b3ca-a49faae377d8",
        name: "user-admin-A",
        description: "Admin User Account Admin-A",
        status: "active",
        createdAt: new Date("2003-09-04T10:39:57.000Z"),
        updatedAt: new Date("2007-06-28T13:56:27.000Z")
      },
      {
        id: "206adaaa-7ae5-45fc-a586-9eb8385ccc6a",
        name: "apiAccount-user",
        description: "Api User Account",
        status: "active",
        createdAt: new Date("2012-06-07T07:12:47.000Z"),
        updatedAt: new Date("2013-12-23T01:52:43.000Z")
      },
      {
        id: "bb724dd2-badd-4b10-8f4c-3e6489b0b28d",
        name: "apiAccount-admin",
        description: "Api Admin Account",
        status: "active",
        createdAt: new Date("2014-05-11T20:45:17.000Z"),
        updatedAt: new Date("2014-08-04T11:43:20.000Z")
      }
    ];

    return r.table("AclRole").insert(aclRoles).run();

  });
};
