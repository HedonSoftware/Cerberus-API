
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");

var assert  = require("chai").assert;
var app     = require("../../app");
var request = require("supertest");
var _       = require(appRootPath + "/lib/utility/underscore");
var moment  = require("moment");

var TestHelper = require("../../lib/test/testHelper");
var testHelper = new TestHelper(__filename);

var expectedBoards = testHelper.getDataProvider("sampleBoards");

let projectId = "460ad418-ac09-44bb-9941-a2a645032c01";

describe("Boards resources", function () {

  beforeEach(function (done) {
    testHelper.executeFixtureScript("sampleProjects", done);
  });

  describe("when requesting resource /boards", function () {
    it("should respond with boards", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var boards = JSON.parse(res.text);
          _.each(expectedBoards.default, function(result) {
            assert.include(boards, result);
          });
          done();
        });
    });
  });

  describe("when requesting resource /boards with first pack of fields", function () {
    it("should respond with fields (part1) boards", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards" +
          "?conditions[id]=" + expectedBoards.board3.id +
          "&fields[]=id" +
          "&fields[]=name" +
          "&fields[]=description" +
          "&fields[]=type"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var boards = JSON.parse(res.text);
          assert.deepEqual(boards, expectedBoards.partial1);
          done();
        });
    });
  });

  describe("when requesting resource /boards with second pack of fields", function () {
    it("should respond with fields (part2) boards", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards" +
          "?conditions[id]=" + expectedBoards.board4.id +
          "&fields[]=lanes" +
          "&fields[]=status"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var boards = JSON.parse(res.text);
          assert.deepEqual(boards, expectedBoards.partial2);
          done();
        });
    });
  });

  describe("when requesting resource /boards with specified order", function () {
    it("should respond with ordered boards", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards?order[name]=asc")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var boards = JSON.parse(res.text);
          assert.deepEqual(boards, expectedBoards.orderByName);
          done();
        });
    });
  });

  describe("when requesting resource /boards with specified limit", function () {
    it("should respond with limited number of boards", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards?limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var boards = JSON.parse(res.text);
          assert.equal(boards.length, 3);
          done();
        });
    });
  });

  describe("when requesting resource /boards with specified limit and order", function () {
    it("should respond with limited number of ordered boards", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards?order[name]=asc&limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var boards = JSON.parse(res.text);
          assert.deepEqual(boards, expectedBoards.limitedAndOrdered);
          done();
        });
    });
  });

  // --------------------------------------------------------------------------- //
  // -------------------------- GETTING SINGLE RECORD -------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when requesting resource /boards/:id", function () {
    it("should respond with single board", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + expectedBoards.board2.id)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var board = JSON.parse(res.text);
          assert.deepEqual(board, expectedBoards.singleById);
          done();
        });
    });
  });

  describe("when requesting non-existing resource /boards/:id", function () {
    it("should respond with 404", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/9ecc0be2-5874-4673-ad1c-a94e225cecac")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when requesting non-existing resource /boards/:id - penetration testing", function () {
    it("should respond with 400", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- CREATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when posting resource to /boards (with dates)", function () {

    var newBoard = {
      "name": "Qwerty Test test 2",
      "description": "asdfg...",
      "type": "action",
      "status": "pending",
      "createdAt": "2000-10-14 02:44:36",
      "updatedAt": "2006-12-22 10:21:14"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards")
        .send(newBoard)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          var board = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/projects/" + projectId + "/boards/" + board.id);

          newBoard.id = board.id;
          newBoard.lanes = {};
          newBoard.sprints = {};
          newBoard.tags = [];
          newBoard.createdAt = board.createdAt;
          newBoard.updatedAt = null;
          assert.deepEqual(board, newBoard);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(newBoard.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + board.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var board = JSON.parse(res.text);
              assert.deepEqual(board, newBoard);
              request(app)
                .get("/projects/" + projectId)
                .expect("Content-Type", /json/)
                .expect(200)
                .end(function (err, res) {

                  var project = JSON.parse(res.text);

                  let expectedProject = JSON.parse(JSON.stringify(expectedBoards.project));
                  expectedProject.boards[newBoard.id] = newBoard;
                  expectedProject.updatedAt = project.updatedAt;

                  // comparing times
                  let currentTimestamp = moment();
                  let updatedAt = moment(project.updatedAt);

                  // it should be less then 5 seconds difference
                  assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

                  assert.deepEqual(project, expectedProject);
                  done();
                });
            });
        });
    });
  });

  describe("when posting resource to /boards (without dates)", function () {

    var newBoard = {
      "name": "Qwerty Test test 2",
      "description": "asdfg...",
      "type": "action",
      "status": "pending"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards")
        .send(newBoard)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          var board = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/projects/" + projectId + "/boards/" + board.id);

          newBoard.id = board.id;
          newBoard.lanes = {};
          newBoard.sprints = {};
          newBoard.tags = [];
          newBoard.createdAt = board.createdAt;
          newBoard.updatedAt = null;
          assert.deepEqual(board, newBoard);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(newBoard.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + board.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var board = JSON.parse(res.text);
              assert.deepEqual(board, newBoard);
              request(app)
                .get("/projects/" + projectId)
                .expect("Content-Type", /json/)
                .expect(200)
                .end(function (err, res) {

                  var project = JSON.parse(res.text);

                  let expectedProject = JSON.parse(JSON.stringify(expectedBoards.project));
                  expectedProject.boards[newBoard.id] = newBoard;
                  expectedProject.updatedAt = project.updatedAt;

                  // comparing times
                  let currentTimestamp = moment();
                  let updatedAt = moment(project.updatedAt);

                  // it should be less then 5 seconds difference
                  assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

                  assert.deepEqual(project, expectedProject);
                  done();
                });
            });
        });
    });
  });

  describe("when posting resource to /boards (with lanes)", function () {

    var newBoard = {
      "name": "Qwerty Test-teST2",
      "description": "asdfg...",
      "status": "pending",
      "lanes": {
        toDo: {
          id: "toDo",
          name: "To do",
          description: "Tickets to be worked on",
          tags: []
        },
        inProgress: {
          id: "inProgress",
          name: "In progress",
          description: "Working on those",
          tags: []
        },
        done: {
          id: "done",
          name: "Finished",
          description: "Done done tickets",
          tags: []
        }
      }
    };

    it("should respond with 409 and error message", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards")
        .send(newBoard)
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when posting invalid resource to /boards", function () {

    it("should respond with 400", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- UPDATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when patching(updating) resource to /boards/:id (with dates)", function () {

    let doneLaneId = "094f1eb5-9d6c-4b8f-8279-ce97d2df2fdd";
    let acceptanceTestingLaneId = "0d33391e-0402-4855-a0b3-d447dc7586c6";

    var updatedBoard = {
      "name": "qwerty",
      "description": "asdfg...",
      "status": "pending",
      "createdAt": "2012-11-24 10:42:38",
      "updatedAt": "2015-04-08 16:37:48"
    };

    let boardId = expectedBoards.board2.id;

    let expectedBoard = JSON.parse(JSON.stringify(expectedBoards.board2));
    expectedBoard.sprints = {};
    expectedBoard.tags = expectedBoards.board2.tags;
    expectedBoard.name = updatedBoard.name;
    expectedBoard.description = updatedBoard.description;
    expectedBoard.status = updatedBoard.status;

    let expectedProject = JSON.parse(JSON.stringify(expectedBoards.project));
    expectedProject.boards[boardId].name = updatedBoard.name;
    expectedProject.boards[boardId].description = updatedBoard.description;
    expectedProject.boards[boardId].status = updatedBoard.status;

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId)
        .send(updatedBoard)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          updatedBoard.id = boardId;
          var board = JSON.parse(res.text);

          expectedBoard.updatedAt = board.updatedAt;
          assert.deepEqual(board, expectedBoard);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(board.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var board = JSON.parse(res.text);
              assert.deepEqual(board, expectedBoard);
              done();
            });
        });
    });
  });

  describe("when patching(updating) resource to /boards/:id (without dates)", function () {

    let doneLaneId = "094f1eb5-9d6c-4b8f-8279-ce97d2df2fdd";
    let acceptanceTestingLaneId = "0d33391e-0402-4855-a0b3-d447dc7586c6";

    var updatedBoard = {
      "name": "qwerty",
      "description": "asdfg...",
      "status": "pending"
    };

    let boardId = expectedBoards.board2.id;

    let expectedBoard = JSON.parse(JSON.stringify(expectedBoards.board2));
    expectedBoard.sprints = {};
    expectedBoard.tags = expectedBoards.board2.tags;
    expectedBoard.name = updatedBoard.name;
    expectedBoard.description = updatedBoard.description;
    expectedBoard.status = updatedBoard.status;

    let expectedProject = JSON.parse(JSON.stringify(expectedBoards.project));
    expectedProject.boards[boardId].name = updatedBoard.name;
    expectedProject.boards[boardId].description = updatedBoard.description;
    expectedProject.boards[boardId].status = updatedBoard.status;

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId)
        .send(updatedBoard)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          updatedBoard.id = 2;
          var board = JSON.parse(res.text);

          expectedBoard.updatedAt = board.updatedAt;
          assert.deepEqual(board, expectedBoard);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(board.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var board = JSON.parse(res.text);
              assert.deepEqual(board, expectedBoard);
              done();
            });
        });
    });
  });

  describe("when patching(updating) resource to /boards/:id with lanes", function () {

    let doneLaneId = "094f1eb5-9d6c-4b8f-8279-ce97d2df2fdd";
    let acceptanceTestingLaneId = "0d33391e-0402-4855-a0b3-d447dc7586c6";

    var updatedBoard = {
      "name": "qwerty",
      "description": "asdfg...",
      "status": "pending",
      "lanes": {
        [acceptanceTestingLaneId]: {
          "name": "Acceptance testing Updated"
        },
        [doneLaneId]: {
          "name": "Done Updated"
        }
      }
    };

    let boardId = expectedBoards.board2.id;

    it("should respond with 409 and error message", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId)
        .send(updatedBoard)
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when patching(updating) invalid resource to /boards/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/2")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  describe("when patching(updating) non-existing resource to /boards/:id", function () {

    var newBoard = {
      "name": "qwerty",
      "description": "asdfg...",
      "status": "pending",
      "type": "backlog"
    };

    it("should respond with 409 and error message", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/637174f8-3a86-4ba3-8a17-1a7c60be5e25")
        .send(newBoard)
        .expect("Content-Type", /json/)
        .expect(409, done)
    });
  });

  describe("when patching(updating) invalid non-existing resource to /boards/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/637174f8-3a86-4ba3-8a17-1a7c60be5e25")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- DELETING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when deleting resource /boards/:id", function () {

    let expectedProject = JSON.parse(JSON.stringify(expectedBoards.project));
    delete expectedProject.boards['c0dcbf69-f571-49ec-9c47-2d679f678813'];

    it("should respond with 204 and updated resource", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/c0dcbf69-f571-49ec-9c47-2d679f678813")
        .expect("Content-Type", /json/)
        .expect(204)
        .end(function (err, res) {
          request(app)
            .get("/projects/" + projectId + "/boards/c0dcbf69-f571-49ec-9c47-2d679f678813")
            .expect("Content-Type", /json/)
            .expect(404)
            .end(function (err, res) {

              request(app)
                .get("/projects/" + projectId)
                .expect(200)
                .end(function (err, res) {

                  var project = JSON.parse(res.text);
                  assert.deepEqual(project, expectedProject);
                  done();
                });
            });
        });
    });
  });

  describe("when deleting non-existing resource /boards/:id", function () {

    it("should respond with 404", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/637174f8-3a86-4ba3-8a17-1a7c60be5e25")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when deleting non-existing resource /boards/:id - penetration testing", function () {

    it("should respond with 400", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });
});
