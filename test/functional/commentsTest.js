
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");

var assert  = require("chai").assert;
var app     = require("../../app");
var request = require("supertest");
var moment  = require("moment");
var _       = require("underscore");

var TestHelper = require("../../lib/test/testHelper");
var testHelper = new TestHelper(__filename);

var expectedComments = testHelper.getDataProvider("sampleTickets");

var ticketId = "61ad71eb-0ca2-49af-b371-c16599207bbc";
var ticketWithoutCommentsId = "fbf750db-8d63-446e-ba8b-603f2d40c35d";

var commentId = "da517538-c7ae-4bd5-9242-71b2a9b98828";

describe("Comments resources", function () {

  beforeEach(function (done) {
    testHelper.executeFixtureScript("sampleTickets", done);
  });

  describe("when requesting resource /tickets/:ticketId/comments", function () {
    it("should respond with comments", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          _.each(expectedComments.default, function(result) {
            assert.include(comments, result);
          });
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments on ticket without comments", function () {
    it("should respond with empty array", function (done) {
      request(app)
        .get("/tickets/" + ticketWithoutCommentsId + "/comments")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.deepEqual(comments, []);
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments", function () {
    it("should respond with comments", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments" +
          "?fields[]=c.*"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          _.each(expectedComments.default, function(result) {
            assert.include(comments, result);
          });
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments with first pack of fields", function () {
    it("should respond with fields (part1) comments", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments" +
          "?conditions[id]=753d282f-f03f-405d-b618-9c08dc9f30b7" +
          "&fields[]=id" +
          "&fields[]=text" +
          "&fields[]=userId"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.deepEqual(comments, expectedComments.partial1);
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments with second pack of fields", function () {
    it("should respond with fields (part2) comments", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments" +
          "?conditions[id]=9dc06a42-27f2-4541-b123-2376e35e3c44" +
          "&fields[]=status" +
          "&fields[]=updatedAt" +
          "&fields[]=createdAt"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.deepEqual(comments, expectedComments.partial2);
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments with pack of fields on ticket without comments", function () {
    it("should respond with empty array", function (done) {
      request(app)
        .get("/tickets/" + ticketWithoutCommentsId + "/comments" +
          "?conditions[id]=9dc06a42-27f2-4541-b123-2376e35e3c44" +
          "&fields[]=status" +
          "&fields[]=updatedAt" +
          "&fields[]=createdAt"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.deepEqual(comments, []);
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments with specified order", function () {
    it("should respond with ordered comments", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments?order[text]=asc")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.deepEqual(comments, expectedComments.orderByText);
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments with specified limit", function () {
    it("should respond with limited number of comments", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments?limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.equal(comments.length, 3);
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments with specified limit and order", function () {
    it("should respond with limited number of ordered comments", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments?order[text]=asc&limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.deepEqual(comments, expectedComments.limitedAndOrdered);
          done();
      });
    });
  });

  describe("when requesting resource /tickets/:ticketId/comments with specified join on user, fields and condition", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    it("should respond with comments joined with user", function (done) {
      request(app)
        .get(
          "/tickets/" + ticketId + "/comments" +
          "?fields[]=c.id" +
          "&fields[]=c.text" +
          "&fields[]=c.userId" +
          "&fields[u.username]=user" +
          "&joins[]=user" +
          "&conditions[c.id][eq]=9dc06a42-27f2-4541-b123-2376e35e3c44"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comments = JSON.parse(res.text);
          assert.deepEqual(comments, expectedComments.joinedWithUser);
          done();
      });
    });
  });

  // --------------------------------------------------------------------------- //
  // -------------------------- GETTING SINGLE RECORD -------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when requesting resource /tickets/:ticketId/comments/:id", function () {
    it("should respond with single comment", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments/9dc06a42-27f2-4541-b123-2376e35e3c44")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var comment = JSON.parse(res.text);
          assert.deepEqual(comment, expectedComments.singleById);
          done();
      });
    });
  });

  describe("when requesting non-existing resource /tickets/:ticketId/comments/:id", function () {
    it("should respond with 404", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments/12345678-9d55-44f0-a876-6df90d7967a8")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when requesting non-existing resource /tickets/:ticketId/comments/:id - penetration testing", function () {
    it("should respond with 400", function (done) {
      request(app)
        .get("/tickets/" + ticketId + "/comments/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- CREATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when posting resource to /tickets/:ticketId/comments (with dates)", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var newComment = {
      "text": "New comment Xyz ...",
      "userId": "1d58b82e-eeec-4065-9386-5309a6aede48",
      "status": "pending",
      "createdAt": "1976-02-22 06:33:02",
      "updatedAt": "1990-04-06 09:16:50"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/tickets/" + ticketId + "/comments")
        .send(newComment)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          var comment = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/tickets/" + ticketId + "/comments/" + comment.id);

          newComment.id = comment.id;
          newComment.createdAt = comment.createdAt;
          newComment.updatedAt = null;
          assert.deepEqual(comment, newComment);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(comment.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/tickets/" + ticketId + "/comments/" + comment.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var comment = JSON.parse(res.text);
              assert.deepEqual(comment, newComment);

              // comparing times
              let currentTimestamp = moment();
              let createdAt = moment(comment.createdAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when posting resource to /tickets/:ticketId/comments (without dates)", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var newComment = {
      "text": "New comment Xyz ...",
      "userId": "07c1c04b-7926-44f2-bceb-8a13bd6c3b9d",
      "status": "pending"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/tickets/" + ticketId + "/comments")
        .send(newComment)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          var comment = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/tickets/" + ticketId + "/comments/" + comment.id);

          newComment.id = comment.id;
          newComment.createdAt = comment.createdAt;
          newComment.updatedAt = null;
          assert.deepEqual(comment, newComment);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(comment.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/tickets/" + ticketId + "/comments/" + comment.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var comment = JSON.parse(res.text);
              assert.deepEqual(comment, newComment);

              // comparing times
              let currentTimestamp = moment();
              let createdAt = moment(comment.createdAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when posting invalid comment resource to /tickets/:ticketId/comments", function () {

    it("should respond with 400", function (done) {
      request(app)
        .post("/tickets/" + ticketId + "/comments")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- UPDATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when patching(updating) resource to /tickets/:ticketId/comments/:id (with dates)", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var updatedComment = {
      "text": "Updated comment Xyz ...",
      "userId": "dbfdc23a-d442-4970-a214-ff14f0d1875c",
      "status": "pending",
      "createdAt": "1976-02-22 06:33:02",
      "updatedAt": "1990-04-06 09:16:50"
    };

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/tickets/" + ticketId + "/comments/" + commentId)
        .send(updatedComment)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {

          var comment = JSON.parse(res.text);

          updatedComment.id = commentId;
          updatedComment.createdAt = "2015-08-22T21:34:32.000Z";
          updatedComment.updatedAt = comment.updatedAt;

          assert.deepEqual(comment, updatedComment);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(comment.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/tickets/" + ticketId + "/comments/" + commentId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var comment = JSON.parse(res.text);
              assert.deepEqual(comment, updatedComment);
              done();
          });
      });
    });
  });

  describe("when patching(updating) resource to /tickets/:ticketId/comments/:id (default dates)", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var updatedComment = {
      "text": "Updated comment Xyz ...",
      "userId": "8e6002c6-e5ef-4552-8bfe-f785a810767a",
      "status": "pending"
    };

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/tickets/" + ticketId + "/comments/" + commentId)
        .send(updatedComment)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {

          var comment = JSON.parse(res.text);

          updatedComment.id = commentId;
          updatedComment.createdAt = "2015-08-22T21:34:32.000Z";
          updatedComment.updatedAt = comment.updatedAt;

          // comparing times
          var currentTimestamp = moment();
          var updatedAt = moment(comment.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);


          assert.deepEqual(comment, updatedComment);

          request(app)
            .get("/tickets/" + ticketId + "/comments/" + commentId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var comment = JSON.parse(res.text);

              // overwriting updatedAt (created should stay as it was)
              updatedComment.updatedAt = comment.updatedAt;

              assert.deepEqual(comment, updatedComment);

              // comparing times
              var currentTimestamp = moment();
              var updatedAt = moment(comment.updatedAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);
              done();
          });
      });
    });
  });

  describe("when patching(updating) invalid resource to /tickets/:ticketId/comments/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/tickets/" + ticketId + "/comments/" + commentId)
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  describe("when patching(updating) non-existing resource to /tickets/:ticketId/comments/:id", function () {

    var newComment = {
      "text": "Updated comment Xyz ..."
    };

    it("should respond with 200 and created resource", function (done) {
      request(app)
        .patch("/tickets/" + ticketId + "/comments/12345678-1234-1234-1234-123456789012")
        .send(newComment)
        .expect("Content-Type", /json/)
        .expect(409)
        .end(function (err, res) {
          var errorMessage = JSON.parse(res.text);
          assert.deepEqual(errorMessage, {
            type: "GatewayError",
            message: "Specified comment doesn\'t exist",
            errorCode: 409,
            data: {
              id: "12345678-1234-1234-1234-123456789012"
            }
          });
          done();

       });
    });
  });

  describe("when puting invalid resource to /tickets/:ticketId/comments/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/tickets/" + ticketId + "/comments/" + commentId)
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- DELETING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when deleting resource /tickets/:ticketId/comments/:id", function () {

    it("should respond with 204 and updated resource", function (done) {
      request(app)
        .del("/tickets/" + ticketId + "/comments/" + commentId)
        .expect("Content-Type", /json/)
        .expect(204)
        .end(function (err, res) {
        request(app)
          .get("/tickets/" + ticketId + "/comments/" + commentId)
          .expect("Content-Type", /json/)
          .expect(404, done);
      });
    });
  });

  describe("when deleting non-existing resource /tickets/:ticketId/comments/:id", function () {

    it("should respond with 404", function (done) {
      request(app)
        .del("/tickets/" + ticketId + "/comments/12345678-1234-1234-1234-123456789012")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when deleting non-existing resource /tickets/:ticketId/comments/:id - penetration testing", function () {

    it("should respond with 400", function (done) {
      request(app)
        .del("/tickets/" + ticketId + "/comments/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });
});
