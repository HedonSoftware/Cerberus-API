
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

let appRootPath = require("app-root-path");

let app     = require("../../app");
let request = require("supertest");
let assert  = require("chai").assert;
let moment  = require("moment");
let _       = require(appRootPath + "/lib/utility/underscore");

let TestHelper = require("../../lib/test/testHelper");
let testHelper = new TestHelper(__filename);

let expectedProjects = testHelper.getDataProvider("sampleProjects");

describe("Projects resource", function () {

  beforeEach(function (done) {
    testHelper.executeFixtureScript("sampleProjects", done);
  });

  describe("when requesting resource /projects", function () {
    it("should respond with all projects", function (done) {
      request(app)
        .get("/projects")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let projects = JSON.parse(res.text);

          _.each(expectedProjects.default, function(result) {
            assert.include(projects, result);
          });

          done();
      });
    });
  });

  describe("when requesting resource /projects", function () {
    it("should respond with projects", function (done) {
      request(app)
        .get("/projects" +
          "?fields[]=p.*"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let projects = JSON.parse(res.text);

          _.each(expectedProjects.default, function(result) {
            assert.include(projects, result);
          });

          done();
      });
    });
  });

  describe("when requesting resource /projects with first pack of fields", function () {
    it("should respond with fields (part1) projects", function (done) {
      request(app)
        .get("/projects" +
          "?conditions[id]=11cefac7-ebad-4b3f-b08d-dcbb8e443ed9" +
          "&fields[]=id" +
          "&fields[]=name" +
          "&fields[]=code" +
          "&fields[]=description")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let project = JSON.parse(res.text);
          assert.deepEqual(project, expectedProjects.partial1);
          done();
      });
    });
  });

  describe("when requesting resource /projects with second pack of fields", function () {
    it("should respond with fields (part2) projects", function (done) {
      request(app)
        .get("/projects" +
          "?conditions[id]=d11f2106-480d-40c5-be32-e6a253be2078" +
          "&fields[]=boards" +
          "&fields[]=status" +
          "&fields[]=createdAt" +
          "&fields[]=updatedAt")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let project = JSON.parse(res.text);
          assert.deepEqual(project, expectedProjects.partial2);
          done();
      });
    });
  });

  describe("when requesting resource /projects with specified order", function () {
    it("should respond with ordered projects", function (done) {
      request(app)
        .get("/projects?order[name]=asc")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let projects = JSON.parse(res.text);
          assert.deepEqual(projects, expectedProjects.orderByName);
          done();
      });
    });
  });

  describe("when requesting resource /projects with specified limit", function () {
    it("should respond with limited number of projects", function (done) {
      request(app)
        .get("/projects?limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let projects = JSON.parse(res.text);
          assert.equal(projects.length, 3);
          done();
      });
    });
  });

  describe("when requesting resource /projects with specified limit and order", function () {
    it("should respond with limited number of ordered projects", function (done) {
      request(app)
        .get("/projects?order[name]=asc&limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let projects = JSON.parse(res.text);
          assert.deepEqual(projects, expectedProjects.limitedAndOrdered);
          done();
      });
    });
  });

  // --------------------------------------------------------------------------- //
  // -------------------------- GETTING SINGLE RECORD -------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when requesting resource /projects/:id", function () {
    it("should respond with single project", function (done) {
      request(app)
        .get("/projects/11cefac7-ebad-4b3f-b08d-dcbb8e443ed9")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let project = JSON.parse(res.text);
          assert.deepEqual(project, expectedProjects.singleById);
          done();
      });
    });
  });

  describe("when requesting non-existing resource /projects/:id", function () {
    it("should respond with 404", function (done) {
      request(app)
        .get("/projects/12345678-1234-1234-1234-123456789012")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when requesting non-existing resource /projects/:id - penetration testing", function () {
    it("should respond with 400", function (done) {
      request(app)
        .get("/projects/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- CREATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when posting resource to /projects with dates", function () {

    let newProject = {
      name: "New project Abc",
      code: "NABC",
      description: "New project Abc is...",
      status: "inactive",
      createdAt: "2000-10-14 02:44:36",
      updatedAt: "2006-12-22 10:21:14"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects")
        .send(newProject)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          let project = JSON.parse(res.text);

          let location = res.header.location;
          assert.equal(location, "/projects/" + project.id);

          newProject.id = project.id;
          newProject.boards = {};
          newProject.createdAt = project.createdAt;
          newProject.updatedAt = null;
          assert.deepEqual(project, newProject);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(newProject.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + project.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {

              let project = JSON.parse(res.text);
              assert.deepEqual(project, newProject);

              // comparing times
              let currentTimestamp = moment();
              let createdAt = moment(project.createdAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(createdAt) < 5000, true);
              done();
          });
      });
    });
  });

  describe("when posting resource to /projects (default dates test)", function () {

    let newProject = {
      name: "New project Abc",
      code: "NABC",
      description: "New project Abc is...",
      status: "inactive"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects")
        .send(newProject)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {
          let project = JSON.parse(res.text);

          let location = res.header.location;
          assert.equal(location, "/projects/" + project.id);

          newProject.id = project.id;
          newProject.boards = {};

          // overwriting
          newProject.createdAt = project.createdAt;
          newProject.updatedAt = null;

          assert.deepEqual(project, newProject);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(newProject.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + project.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              let project = JSON.parse(res.text);
              assert.deepEqual(project, newProject);

              // comparing times
              assert.equal(newProject.updatedAt, null);
              let currentTimestamp = moment();
              let createdAt = moment(newProject.createdAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(createdAt) < 5000, true);
              done();
          });
      });
    });
  });

  describe("when posting project resource with boards to /projects", function () {

    let newProject = {
      name: "New project Abc",
      code: "NABC",
      boards: {
        testId: {
          id: "testId",
          name: "xyz"
        }
      },
      description: "New project Abc is...",
      status: "inactive"
    };

    it("should respond with 409", function (done) {
      request(app)
        .post("/projects")
        .send(newProject)
        .expect("Content-Type", /json/)
        .expect(409)
        .end(done);
    });
  });

  describe("when posting invalid project resource to /projects", function () {

    it("should respond with 400", function (done) {
      request(app)
        .post("/projects")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  describe("when posting invalid json to /projects", function () {

    it("should respond with 400", function (done) {
      request(app)
        .post("/projects")
        .send("{\"still looks fine\" : 1, \"butNotHere\" :\"\"}")
        .expect("Content-Type", /json/)
        .expect(400, done); // it"s not possible to send invalid json via supertest...
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- UPDATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when patching(updating) resource to /projects/:id (with dates)", function () {

    let updatedProject = {
      name: "Updated project Abc",
      code: "NABC",
      description: "Updated project Abc is...",
      status: "deleted",
      createdAt: "2000-10-14 02:44:36",
      updatedAt: "2006-12-22 10:21:14"
    };

    let expectedUpdatedProject = JSON.parse(JSON.stringify(expectedProjects.project2));
    expectedUpdatedProject.name = updatedProject.name;
    expectedUpdatedProject.code = updatedProject.code;
    expectedUpdatedProject.description = updatedProject.description;
    expectedUpdatedProject.status= updatedProject.status;

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
        .send(updatedProject)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          let project = JSON.parse(res.text);

          expectedUpdatedProject.updatedAt = project.updatedAt;

          assert.deepEqual(project, expectedUpdatedProject);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(project.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/" + project.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              let project = JSON.parse(res.text);
              assert.deepEqual(project, expectedUpdatedProject);
              done();
          });
      });
    });
  });

  describe("when patching(updating) resource to /projects/:id (default dates)", function () {

    let updatedProject = {
      name: "Updated project Abc",
      code: "NABC",
      description: "Updated project Abc is...",
      status: "deleted"
    };

    let expectedUpdatedProject = JSON.parse(JSON.stringify(expectedProjects.project2));
    expectedUpdatedProject.name = updatedProject.name;
    expectedUpdatedProject.code = updatedProject.code;
    expectedUpdatedProject.description = updatedProject.description;
    expectedUpdatedProject.status= updatedProject.status;

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
        .send(updatedProject)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {

          let project = JSON.parse(res.text);

          expectedUpdatedProject.updatedAt = project.updatedAt;

          assert.deepEqual(project, expectedUpdatedProject);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(project.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              let project = JSON.parse(res.text);

              assert.deepEqual(project, expectedUpdatedProject);

              // comparing times
              let currentTimestamp = moment();
              let updatedAt = moment(project.updatedAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);
              done();
          });
      });
    });
  });

  describe("when patching(updating) resource to /projects/:id including boards", function () {

    let designId = "37680c5c-9540-446a-90c3-152fb1e451d1";
    let toDoLaneId = "32da7875-56fe-4e29-b9fc-cb84eb9eb103";
    let planningLaneId = "0255039a-d375-4c26-8917-dd39ecbfd735";

    let updatedProject = {
      name: "Updated project Abc",
      code: "NABC",
      boards: {
        [designId]: {
          description: "Updated design board",
          lanes: {
            [planningLaneId]: {
              id: planningLaneId,
              name: "Planning",
              description: "Scheduled for planning",
              sequenceNumber: 0,
              status: "active",
              tags: [
                "05165da4-3950-48d9-bffb-5aff67edb47b"
              ]
            },
            "32da7875-56fe-4e29-b9fc-cb84eb9eb103": {
              description: "Approved to be worked on"
            }
          }
        }
      },
      description: "Updated project Abc is...",
      status: "deleted"
    };

    let expectedUpdatedProject = JSON.parse(JSON.stringify(expectedProjects.project2));
    expectedUpdatedProject.name = updatedProject.name;
    expectedUpdatedProject.code = updatedProject.code;
    expectedUpdatedProject.description = updatedProject.description;
    expectedUpdatedProject.status= updatedProject.status;
    expectedUpdatedProject.boards[designId].description = updatedProject.boards[designId].description;
    expectedUpdatedProject.boards[designId].lanes[toDoLaneId].description = updatedProject.boards[designId].lanes[toDoLaneId].description;
    expectedUpdatedProject.boards[designId].lanes[planningLaneId] = updatedProject.boards[designId].lanes[planningLaneId];

    it("should respond with 409 and updated resource", function (done) {
      request(app)
        .patch("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
        .send(updatedProject)
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when patching(updating) invalid resource to /projects/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  describe("when patching(updating) non-existing resource to /projects/:id", function () {

    let newProject = {
      name: "Updated project Abc",
      code: "UABC",
      description: "Updated project Abc is...",
      status: "deleted",
      createdAt: "2000-10-14 02:44:36",
      updatedAt: "2006-12-22 10:21:14"
    };

    it("should respond with 200 and created resource", function (done) {
      request(app)
        .patch("/projects/12345678-1234-1234-1234-123456789012")
        .send(newProject)
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when patching(updating) invalid non-existing resource to /projects/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/10")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- REPLACING RECORD ---------------------------- //
  // --------------------------------------------------------------------------- //
  // ------------- Replacing is disabled as it doesn't make sense ------------- //
  // -------------------------------------------------------------------------- //

  // describe("when puting(replacing) resource to /projects/:id (with dates)", function () {

  //   let updatedProject = {
  //     name: "Updated project Abc",
  //     code: "NABC",
  //     description: "Updated project Abc is...",
  //     status: "deleted",
  //     createdAt: "2000-10-14 02:44:36",
  //     updatedAt: "2006-12-22 10:21:14"
  //   };

  //   it("should respond with 200 and updated resource", function (done) {
  //     request(app)
  //       .put("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
  //       .send(updatedProject)
  //       .expect("Content-Type", /json/)
  //       .expect(200)
  //       .end(function (err, res) {
  //         let project = JSON.parse(res.text);

  //         updatedProject.id = project.id;
  //         updatedProject.createdAt = "2015-08-22T05:29:22.000Z";
  //         updatedProject.updatedAt = project.updatedAt;

  //         assert.deepEqual(project, updatedProject);

  //         // comparing times
  //         let currentTimestamp = moment();
  //         let updatedAt = moment(project.updatedAt);

  //         // it should be less then 5 seconds difference
  //         assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

  //         request(app)
  //           .get("/projects/" + project.id)
  //           .expect("Content-Type", /json/)
  //           .expect(200)
  //           .end(function (err, res) {
  //             let project = JSON.parse(res.text);
  //             assert.deepEqual(project, updatedProject);

  //             // comparing times
  //             let currentTimestamp = moment();
  //             let updatedAt = moment(project.updatedAt);

  //             // it should be less then 5 seconds difference
  //             assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

  //             done();
  //         });
  //     });
  //   });
  // });

  // describe("when puting(replacing) resource to /projects/:id (default dates)", function () {

  //   let projectId = "460ad418-ac09-44bb-9941-a2a645032c01";

  //   let updatedProject = {
  //     name: "Updated project Abc",
  //     code: "NABC",
  //     description: "Updated project Abc is...",
  //     status: "deleted"
  //   };

  //   it("should respond with 200 and updated resource", function (done) {
  //     request(app)
  //       .put("/projects/" + projectId)
  //       .send(updatedProject)
  //       .expect("Content-Type", /json/)
  //       .expect(200)
  //       .end(function (err, res) {

  //         let project = JSON.parse(res.text);

  //         updatedProject.id = projectId;
  //         updatedProject.createdAt = "2015-08-22T05:29:22.000Z";
  //         updatedProject.updatedAt = project.updatedAt;

  //         // comparing times
  //         let currentTimestamp = moment();
  //         let updatedAt = moment(project.updatedAt);

  //         // it should be less then 5 seconds difference
  //         assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

  //         assert.deepEqual(project, updatedProject);

  //         request(app)
  //           .get("/projects/" + projectId)
  //           .expect("Content-Type", /json/)
  //           .expect(200)
  //           .end(function (err, res) {
  //             let project = JSON.parse(res.text);

  //             // overwriting updatedAt (created should stay as it was)
  //             updatedProject.updatedAt = project.updatedAt;

  //             assert.deepEqual(project, updatedProject);

  //             // comparing times
  //             let currentTimestamp = moment();
  //             let updatedAt = moment(project.updatedAt);

  //             // it should be less then 5 seconds difference
  //             assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);
  //             done();
  //         });
  //     });
  //   });
  // });

  // describe("when puting(replacing) resource to /projects/:id with board", function () {

  //   let updatedProject = {
  //     name: "Updated project Abc",
  //     code: "NABC",
  //     description: "Updated project Abc is...",
  //     boards: {
  //       xyz: {
  //         id: "xyz",
  //         name: "some Xyz",
  //         lanes: {}
  //       }
  //     },
  //     status: "deleted",
  //     createdAt: "2000-10-14 02:44:36",
  //     updatedAt: "2006-12-22 10:21:14"
  //   };

  //   it("should respond with 409 and updated resource", function (done) {
  //     request(app)
  //       .put("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
  //       .send(updatedProject)
  //       .expect("Content-Type", /json/)
  //       .expect(409, done);
  //   });
  // });

  // describe("when puting(replacing) invalid resource to /projects/:id", function () {

  //   it("should respond with 400", function (done) {
  //     request(app)
  //       .put("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
  //       .send("invalid")
  //       .expect("Content-Type", /json/)
  //       .expect(400)
  //       .end(done);
  //   });
  // });

  // describe("when puting(replacing) non-existing resource to /projects/:id", function () {

  //   let newProject = {
  //     name: "Updated project Abc",
  //     code: "UABC",
  //     description: "Updated project Abc is...",
  //     status: "deleted",
  //     createdAt: "2000-10-14 02:44:36",
  //     updatedAt: "2006-12-22 10:21:14"
  //   };

  //   it("should respond with 200 and created resource", function (done) {
  //     request(app)
  //       .put("/projects/12345678-1234-1234-1234-123456789012")
  //       .send(newProject)
  //       .expect("Content-Type", /json/)
  //       .expect(409, done);
  //   });
  // });

  // describe("when puting(replacing) invalid non-existing resource to /projects/:id", function () {

  //   it("should respond with 400", function (done) {
  //     request(app)
  //       .put("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
  //       .send("invalid")
  //       .expect("Content-Type", /json/)
  //       .expect(400)
  //       .end(done);
  //   });
  // });

  // --------------------------------------------------------------------------- //
  // ----------------------------- DELETING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when deleting resource /projects/:id", function () {

    it("should delete it and respond with 204", function (done) {
      request(app)
        .del("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
        .expect("Content-Type", /json/)
        .expect(204)
        .end(function (err, res) {
          request(app)
            .get("/projects/460ad418-ac09-44bb-9941-a2a645032c01")
            .expect("Content-Type", /json/)
            .expect(404, done);
        });
    });
  });

  describe("when deleting non-existing resource /projects/:id", function () {

    it("should respond with 404", function (done) {
      request(app)
        .del("/projects/12345678-4ce6-41c4-8f5c-b84a186069ca")
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when deleting non-existing resource /projects/:id - penetration testing", function () {

    it("should respond with 400", function (done) {
      request(app)
        .del("/projects/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });
});
