
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var assert      = require("chai").assert;
var app         = require("../../app");
var request     = require("supertest");
var _           = require(appRootPath + "/lib/utility/underscore");
var moment      = require("moment");

var TestHelper = require("../../lib/test/testHelper");
var testHelper = new TestHelper(__filename);

var expectedTickets = testHelper.getDataProvider("sampleTickets");

describe("Tickets resources", function () {

  beforeEach(function (done) {
    testHelper.executeFixtureScript("sampleTickets", done);
  });

  describe("when requesting resource /tickets", function () {
    it("should respond with tickets", function (done) {
      request(app)
        .get("/tickets")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          _.each(expectedTickets.default, function(result) {
            assert.include(tickets, result);
          });
          done();
      });
    });
  });

  describe("when requesting resource /tickets with first pack of fields", function () {
    it("should respond with fields (part1) tickets", function (done) {
      request(app)
        .get("/tickets?conditions[id]=fbf750db-8d63-446e-ba8b-603f2d40c35d" +
          "&fields[]=id" +
          "&fields[]=name" +
          "&fields[]=description" +
          "&fields[]=storyPoints" +
          "&fields[]=creatorId" +
          "&fields[]=assigneeId"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          assert.deepEqual(tickets, expectedTickets.partial1);
          done();
      });
    });
  });

  describe("when requesting resource /tickets with second pack of fields", function () {
    it("should respond with fields (part2) tickets", function (done) {
      request(app)
        .get("/tickets?conditions[id]=4b9c8c26-9962-4351-b7f8-2e6e333c9ff5" +
          "&fields[]=comments" +
          "&fields[]=workRecords" +
          "&fields[]=tags" +
          "&fields[]=status" +
          "&fields[]=createdAt" +
          "&fields[]=updatedAt"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          assert.deepEqual(tickets, expectedTickets.partial2);
          done();
      });
    });
  });

  describe("when requesting resource /tickets with specified order", function () {
    it("should respond with ordered tickets", function (done) {
      request(app)
        .get("/tickets?order[name]=asc")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          assert.deepEqual(tickets, expectedTickets.orderByText);
          done();
      });
    });
  });

  describe("when requesting resource /tickets with specified limit", function () {
    it("should respond with limited number of tickets", function (done) {
      request(app)
        .get("/tickets?limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          assert.equal(tickets.length, 3);
          done();
      });
    });
  });

  describe("when requesting resource /tickets with specified limit and order", function () {
    it("should respond with limited number of ordered tickets", function (done) {
      request(app)
        .get("/tickets?order[name]=asc&limit=3")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          assert.deepEqual(tickets, expectedTickets.limitedAndOrdered);
          done();
      });
    });
  });

  describe("when requesting resource /tickets with specified join on user(creator), fields and condition", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    it("should respond with tickets joined with users(creators)", function (done) {
      request(app)
        .get(
          "/tickets" +
          "?fields[]=t.id" +
          "&fields[]=t.name" +
          "&fields[]=t.creatorId" +
          "&fields[uc.username]=creator" +
          "&joins[]=creator" +
          "&conditions[t.id]=4b9c8c26-9962-4351-b7f8-2e6e333c9ff5"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
        var tickets = JSON.parse(res.text);
        assert.deepEqual(tickets, expectedTickets.joinedWithCreators);
        done();
      });
    });
  });

  describe("when requesting resource /tickets with specified join on user(assignees), fields and condition", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    it("should respond with tickets joined with users(assignees)", function (done) {
      request(app)
        .get(
          "/tickets" +
          "?fields[]=t.id" +
          "&fields[]=t.name" +
          "&fields[]=t.assigneeId" +
          "&fields[ua.username]=assignee" +
          "&joins[]=assignee" +
          "&conditions[t.id]=4b9c8c26-9962-4351-b7f8-2e6e333c9ff5"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          assert.deepEqual(tickets, expectedTickets.joinedWithAssignees);
          done();
      });
    });
  });

  describe("when requesting resource /tickets with specified join on all linked resources, fields and condition", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    it("should respond with tickets joined with all linked resources", function (done) {
      request(app)
        .get(
          "/tickets" +
          "?fields[]=t.id" +
          "&fields[]=t.name" +
          "&fields[]=t.description" +
          "&fields[uc.username]=creator" +
          "&fields[ua.username]=assignee" +
          "&joins[]=creator" +
          "&joins[]=assignee" +
          "&conditions[t.id]=4b9c8c26-9962-4351-b7f8-2e6e333c9ff5"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var tickets = JSON.parse(res.text);
          assert.deepEqual(tickets, expectedTickets.joinedWithAllLinked);
          done();
      });
    });
  });

  // --------------------------------------------------------------------------- //
  // -------------------------- GETTING SINGLE RECORD -------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when requesting resource /tickets/:id", function () {
    it("should respond with single ticket", function (done) {
      request(app)
        .get("/tickets/4b9c8c26-9962-4351-b7f8-2e6e333c9ff5")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var ticket = JSON.parse(res.text);
          assert.deepEqual(ticket, expectedTickets.singleById);
          done();
      });
    });
  });

  describe("when requesting non-existing resource /tickets/:id", function () {
    it("should respond with 404", function (done) {
      request(app)
        .get("/tickets/12345678-1234-1234-1234-123456789012")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when requesting non-existing resource /tickets/:id - penetration testing", function () {
    it("should respond with 400", function (done) {
      request(app)
        .get("/tickets/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- CREATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when posting resource to /tickets (with dates)", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var newTicket = {
      name: "New ticket",
      description: "Some long description",
      storyPoints: 17,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active",
      createdAt: "2015-08-22 21:34:32",
      updatedAt: "2015-08-23 15:53:34"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/tickets")
        .send(newTicket)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          var ticket = JSON.parse(res.text);
          newTicket.id = ticket.id;
          newTicket.createdAt = ticket.createdAt;
          newTicket.updatedAt = null;
          newTicket.comments = {};
          newTicket.workRecords = {};

          var location = res.header.location;
          assert.equal(location, "/tickets/" + ticket.id);

          assert.deepEqual(ticket, newTicket);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(ticket.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/tickets/" + ticket.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var ticket = JSON.parse(res.text);
              assert.deepEqual(ticket, newTicket);
              done();
          });
      });
    });
  });

  describe("when posting resource to /tickets without dates", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var newTicket = {
      name: "New ticket",
      description: "Some long description",
      storyPoints: 17,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/tickets")
        .send(newTicket)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          var ticket = JSON.parse(res.text);
          newTicket.id = ticket.id;
          newTicket.createdAt = ticket.createdAt;
          newTicket.updatedAt = null;
          newTicket.comments = {};
          newTicket.workRecords = {};

          var location = res.header.location;
          assert.equal(location, "/tickets/" + ticket.id);

          assert.deepEqual(ticket, newTicket);

          request(app)
            .get("/tickets/" + ticket.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var ticket = JSON.parse(res.text);
              assert.deepEqual(ticket, newTicket);
              done();
          });
      });
    });
  });

  describe("when posting resource to /tickets with comment", function () {

    var newTicket = {
      name: "New ticket",
      comments: {
        "12345": {
          id: "12345",
          text: "some comment"
        }
      }
    };

    it("should respond with 409 and error response", function (done) {
      request(app)
        .post("/tickets")
        .send(newTicket)
        .expect("Content-Type", /json/)
        .expect(409)
        .end(function (err, res) {
          var errorResponse = JSON.parse(res.text);

          newTicket.id = errorResponse.data.id;
          newTicket.status = "active";
          newTicket.tags = null;
          newTicket.workRecords = {};
          newTicket.createdAt = errorResponse.data.createdAt;
          newTicket.updatedAt = null;

          assert.deepEqual({
            type: 'ServiceError',
            message: "Unable to insert ticket with comments. Use /tickets/:ticketId/comments to insert comment",
            errorCode: 409,
            data: newTicket
          }, errorResponse);
          done();
        });
    });
  });

  describe("when posting resource to /tickets with work records", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var newTicket = {
      name: "New ticket",
      workRecords: {
        "12345": {
          id: "12345",
          text: "some comment"
        }
      }
    };

    it("should respond with 409 and error response", function (done) {
      request(app)
        .post("/tickets")
        .send(newTicket)
        .expect("Content-Type", /json/)
        .expect(409)
        .end(function (err, res) {
          var errorResponse = JSON.parse(res.text);

          newTicket.id = errorResponse.data.id;
          newTicket.comments = {};
          newTicket.status = "active";
          newTicket.tags = null;
          newTicket.createdAt = errorResponse.data.createdAt;
          newTicket.updatedAt = null;

          assert.deepEqual({
            type: 'ServiceError',
            message: "Unable to insert ticket with work records. Use /tickets/:ticketId/work-records to insert work records",
            errorCode: 409,
            data: newTicket
          }, errorResponse);
          done();
        });
    });
  });

  describe("when posting invalid ticket resource to /tickets", function () {

    it("should respond with 400", function (done) {
      request(app)
        .post("/tickets")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- UPDATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when patching(updating) resource to /tickets/:id without dates", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var updatedTicket = {
      name: "updated ticket",
      description: "Some long description",
      storyPoints: 17,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active"
    };

    let expectedUpdatedTicket = JSON.parse(JSON.stringify(expectedTickets.ticket2));
    expectedUpdatedTicket.name = updatedTicket.name;
    expectedUpdatedTicket.description = updatedTicket.description;
    expectedUpdatedTicket.storyPoints = updatedTicket.storyPoints;
    expectedUpdatedTicket.creatorId = updatedTicket.creatorId;
    expectedUpdatedTicket.assigneeId = updatedTicket.assigneeId;
    expectedUpdatedTicket.tags = updatedTicket.tags;
    expectedUpdatedTicket.status = updatedTicket.status;

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/tickets/61ad71eb-0ca2-49af-b371-c16599207bbc")
        .send(updatedTicket)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          updatedTicket.id = "61ad71eb-0ca2-49af-b371-c16599207bbc";
          var ticket = JSON.parse(res.text);

          expectedUpdatedTicket.updatedAt = ticket.updatedAt;

          assert.deepEqual(ticket, expectedUpdatedTicket);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(ticket.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/tickets/61ad71eb-0ca2-49af-b371-c16599207bbc")
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var ticket = JSON.parse(res.text);
              assert.deepEqual(ticket, expectedUpdatedTicket);

              // comparing times
              let currentTimestamp = moment();
              let updatedAt = moment(ticket.updatedAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when patching(updating) resource to /tickets/:id with dates", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var updatedTicket = {
      name: "updated ticket",
      description: "Some long description",
      storyPoints: 17,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active",
      createdAt: "2000-10-14 02:44:36",
      updatedAt: "2006-12-22 10:21:14"
    };

    let expectedUpdatedTicket = JSON.parse(JSON.stringify(expectedTickets.ticket2));
    expectedUpdatedTicket.name = updatedTicket.name;
    expectedUpdatedTicket.description = updatedTicket.description;
    expectedUpdatedTicket.storyPoints = updatedTicket.storyPoints;
    expectedUpdatedTicket.creatorId = updatedTicket.creatorId;
    expectedUpdatedTicket.assigneeId = updatedTicket.assigneeId;
    expectedUpdatedTicket.tags = updatedTicket.tags;
    expectedUpdatedTicket.status = updatedTicket.status;

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/tickets/61ad71eb-0ca2-49af-b371-c16599207bbc")
        .send(updatedTicket)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          updatedTicket.id = "61ad71eb-0ca2-49af-b371-c16599207bbc";
          var ticket = JSON.parse(res.text);

          expectedUpdatedTicket.updatedAt = ticket.updatedAt;

          assert.deepEqual(ticket, expectedUpdatedTicket);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(ticket.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/tickets/61ad71eb-0ca2-49af-b371-c16599207bbc")
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var ticket = JSON.parse(res.text);
              assert.deepEqual(ticket, expectedUpdatedTicket);

              // comparing times
              let currentTimestamp = moment();
              let updatedAt = moment(ticket.updatedAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when patching(updating) resource to /tickets/:id with comments", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var updatedTicket = {
      name: "updated ticket",
      comments: {
        "23454-3434": {
          id: "23454-3434",
          text: 'test'
        }
      }
    };

    it("should respond with 409 and error message", function (done) {
      request(app)
        .patch("/tickets/61ad71eb-0ca2-49af-b371-c16599207bbc")
        .send(updatedTicket)
        .expect("Content-Type", /json/)
        .expect(409)
        .end(function (err, res) {
          var errorResponse = JSON.parse(res.text);

          updatedTicket.id = "61ad71eb-0ca2-49af-b371-c16599207bbc";
          updatedTicket.status = "active";
          updatedTicket.tags = null;
          updatedTicket.workRecords = {};
          updatedTicket.createdAt = errorResponse.data.createdAt;
          updatedTicket.updatedAt = null;

          assert.deepEqual({
            type: 'ServiceError',
            message: "Unable to update ticket with comments. Use /tickets/:ticketId/comments to update comment",
            errorCode: 409,
            data: updatedTicket
          }, errorResponse);
          done();
        });
    });
  });

  describe("when patching(updating) resource to /tickets/:id with work records", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var updatedTicket = {
      name: "updated ticket",
      workRecords: {
        "23454-3434": {
          id: "23454-3434",
          name: 'test'
        }
      }
    };

    it("should respond with 409 and error message", function (done) {
      request(app)
        .patch("/tickets/61ad71eb-0ca2-49af-b371-c16599207bbc")
        .send(updatedTicket)
        .expect("Content-Type", /json/)
        .expect(409)
        .end(function (err, res) {
          var errorResponse = JSON.parse(res.text);

          updatedTicket.id = "61ad71eb-0ca2-49af-b371-c16599207bbc";
          updatedTicket.comments = {};
          updatedTicket.status = "active";
          updatedTicket.tags = null;
          updatedTicket.createdAt = errorResponse.data.createdAt;
          updatedTicket.updatedAt = null;

          assert.deepEqual({
            type: 'ServiceError',
            message: "Unable to update ticket with work records. Use /tickets/:ticketId/work-records to update work records",
            errorCode: 409,
            data: updatedTicket
          }, errorResponse);
          done();
        });
    });
  });

  describe("when patching(updating) invalid ticket resource to /tickets/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/tickets/61ad71eb-0ca2-49af-b371-c16599207bbc")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  describe("when patching(updating) non-existing resource to /tickets/:id", function () {

    before(function (done) {
      testHelper.executeFixtureScript("sampleUsers", done);
    });

    var updatedTicket = {
      name: "updated ticket",
      description: "Some long description",
      storyPoints: 17,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active"
    };

    it("should respond with 200 and created resource", function (done) {
      request(app)
        .patch("/tickets/12345678-1234-1234-1234-123456789123")
        .send(updatedTicket)
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when patching(updating) invalid non-existing ticket resource to /tickets/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/tickets/12345678-1234-1234-1234-123456789123")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- DELETING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when deleting resource /tickets/:id", function () {

    it("should respond with 204 and no content", function (done) {
      request(app)
        .del("/tickets/fbf750db-8d63-446e-ba8b-603f2d40c35d")
        .expect("Content-Type", /json/)
        .expect(204)
        .end(function (err, res) {
        request(app)
          .get("/tickets/fbf750db-8d63-446e-ba8b-603f2d40c35d")
          .expect("Content-Type", /json/)
          .expect(404, done);
      });
    });
  });

  describe("when deleting non-existing resource /tickets/:id", function () {

    it("should respond with 404", function (done) {
      request(app)
        .del("/tickets/12345678-1234-1234-1234-123456789123")
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when deleting non-existing resource /tickets/:id - penetration testing", function () {

    it("should respond with 400", function (done) {
      request(app)
        .del("/tickets/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });
});
