
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");

var assert  = require("chai").assert;
var app     = require("../../app");
var request = require("supertest");
var moment  = require("moment");
var _       = require(appRootPath + "/lib/utility/underscore");

var TestHelper = require("../../lib/test/testHelper");
var testHelper = new TestHelper(__filename);

var expectedSprints = testHelper.getDataProvider("sampleSprints");

let projectId = "460ad418-ac09-44bb-9941-a2a645032c01";
let boardId = "3c08ed3c-8257-47e3-804b-fe74b84d9704";


describe("Sprint resources", function () {

  beforeEach(function (done) {
    testHelper.executeFixtureScript("sampleProjects", done);
  });

  describe("when requesting resource /project/:projectId/boards/:boardId/sprints", function () {
    it("should respond with sprints", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/sprints")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var sprints = JSON.parse(res.text);
          _.each(expectedSprints.default, function(result) {
            assert.include(sprints, result);
          });
          done();
      });
    });
  });

  describe("when requesting resource /sprints with first pack of fields", function () {
    it("should respond with fields (part1) sprints", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/sprints" +
          "?conditions[id]=" + expectedSprints.sprint3.id +
          "&fields[]=id" +
          "&fields[]=name" +
          "&fields[]=description" +
          "&fields[]=startDate"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var sprint = JSON.parse(res.text);
          assert.deepEqual(sprint, expectedSprints.partial1);
          done();
      });
    });
  });

  describe("when requesting resource /sprints with second pack of fields", function () {
    it("should respond with fields (part2) sprints", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/sprints" +
          "?conditions[id]=" + expectedSprints.sprint4.id +
          "&fields[]=endDate" +
          "&fields[]=status" +
          "&fields[]=createdAt" +
          "&fields[]=updatedAt"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var sprint = JSON.parse(res.text);
          assert.deepEqual(sprint, expectedSprints.partial2);
          done();
      });
    });
  });

  describe("when requesting resource /sprints with specified order", function () {
    it("should respond with ordered sprints", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/sprints" +
          "?order[startDate]=asc" +
          "&order[name]=asc"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var sprints = JSON.parse(res.text);
          assert.deepEqual(sprints, expectedSprints.orderByStartDate);
          done();
      });
    });
  });

  describe("when requesting resource /sprints with specified limit", function () {
    it("should respond with limited number of sprints", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/sprints" +
          "?limit=3"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var sprints = JSON.parse(res.text);
          assert.deepEqual(sprints.length, 3);
          done();
      });
    });
  });

  describe("when requesting resource /sprints with specified limit and order", function () {
    it("should respond with limited number of ordered sprints", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/sprints" +
          "?order[startDate]=asc" +
          "&limit=3"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var sprints = JSON.parse(res.text);
          assert.deepEqual(sprints, expectedSprints.limitedAndOrdered);
          done();
      });
    });
  });

  // --------------------------------------------------------------------------- //
  // -------------------------- GETTING SINGLE RECORD -------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when requesting resource /sprints/:id", function () {
    it("should respond with single sprint", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId +
          "/sprints/" + expectedSprints.sprint4.id)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var sprint = JSON.parse(res.text);
          assert.deepEqual(sprint, expectedSprints.singleById);
          done();
      });
    });
  });

  describe("when requesting non-existing resource /sprints/:id", function () {
    it("should respond with 404", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId +
          "/sprints/123493a5-2e83-46de-be32-c434d68aa789")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when requesting non-existing resource /sprints/:id - penetration testing", function () {
    it("should respond with 400", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId +
          "/sprints/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- CREATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when posting resource to /sprints with dates", function () {

    var newSprint = {
      "name": "new sprint 123",
      "description": "This is long description of new sprint 123...",
      "startDate": new Date("2015-08-03T00:00:00.000Z"),
      "endDate": new Date("2015-09-03T17:30:00.000Z"),
      "status": "active",
      "createdAt": new Date("2015-09-22T16:13:51.000Z"),
      "updatedAt": new Date("2015-10-15T14:31:55.000Z")
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards/" + boardId + "/sprints")
        .send(newSprint)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {
          var sprint = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprint.id);

          newSprint.id = sprint.id;
          newSprint.startDate = "2015-08-03T00:00:00.000Z";
          newSprint.endDate = "2015-09-03T17:30:00.000Z";
          newSprint.createdAt = sprint.createdAt;
          newSprint.updatedAt = null;
          assert.deepEqual(sprint, newSprint);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(newSprint.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprint.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var sprint = JSON.parse(res.text);
              assert.deepEqual(sprint, newSprint);

              // comparing times
              let currentTimestamp = moment();
              let createdAt = moment(sprint.createdAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when posting resource to /sprints without dates", function () {

    var newSprint = {
      "name": "new sprint 123",
      "description": "This is long description of new sprint 123...",
      "startDate": new Date("2015-08-03T00:00:00.000Z"),
      "endDate": new Date("2015-09-03T17:30:00.000Z"),
      "status": "active",
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards/" + boardId + "/sprints")
        .send(newSprint)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {

          var sprint = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprint.id);

          newSprint.id = sprint.id;
          newSprint.startDate = "2015-08-03T00:00:00.000Z";
          newSprint.endDate = "2015-09-03T17:30:00.000Z";
          newSprint.createdAt = sprint.createdAt;
          newSprint.updatedAt = null;
          assert.deepEqual(sprint, newSprint);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(newSprint.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprint.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var sprint = JSON.parse(res.text);
              assert.deepEqual(sprint, newSprint);

              // comparing times
              let currentTimestamp = moment();
              let createdAt = moment(sprint.createdAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when posting invalid sprint resource to /sprints", function () {

    it("should respond with 400", function (done) {
      request(app)
      .post("/projects/" + projectId + "/boards/" + boardId + "/sprints")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- UPDATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when patching(updating) resource to /sprints/:id with dates", function () {

    let sprintId = "a239fb8d-a632-4f6e-91b1-16cbaa519800";

    var updatedSprint = {
      "name": "updated sprint 123",
      "description": "This is long UPDATED description of sprint 123...",
      "startDate": new Date("2015-08-03T09:00:00.000Z"),
      "endDate": new Date("2015-09-03T17:00:00.000Z"),
      "status": "active",
      "createdAt": new Date("2015-09-22T16:13:51.000Z"),
      "updatedAt": new Date("2015-10-15T14:31:55.000Z")
    };

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprintId)
        .send(updatedSprint)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {

          var sprint = JSON.parse(res.text);

          updatedSprint.id = sprint.id;
          updatedSprint.startDate = "2015-08-03T09:00:00.000Z";
          updatedSprint.endDate = "2015-09-03T17:00:00.000Z";
          updatedSprint.createdAt = "2015-08-23T16:13:51.000Z";
          updatedSprint.updatedAt = sprint.updatedAt;

          assert.deepEqual(sprint, updatedSprint);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(updatedSprint.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprintId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var sprint = JSON.parse(res.text);
              assert.deepEqual(sprint, updatedSprint);

              // comparing times
              let currentTimestamp = moment();
              let updatedAt = moment(updatedSprint.updatedAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when patching(updating) resource to /sprints/:id without dates", function () {

    let sprintId = "a239fb8d-a632-4f6e-91b1-16cbaa519800";

    var updatedSprint = {
      "name": "updated sprint 123",
      "description": "This is long UPDATED description of sprint 123...",
      "startDate": new Date("2015-08-03T09:00:00.000Z"),
      "endDate": new Date("2015-09-03T17:00:00.000Z"),
      "status": "active"
    };

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprintId)
        .send(updatedSprint)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {

          var sprint = JSON.parse(res.text);

          updatedSprint.id = sprint.id;
          updatedSprint.startDate = "2015-08-03T09:00:00.000Z";
          updatedSprint.endDate = "2015-09-03T17:00:00.000Z";
          updatedSprint.createdAt = "2015-08-23T16:13:51.000Z";
          updatedSprint.updatedAt = sprint.updatedAt;

          assert.deepEqual(sprint, updatedSprint);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(updatedSprint.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprintId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var sprint = JSON.parse(res.text);
              assert.deepEqual(sprint, updatedSprint);

              // comparing times
              let currentTimestamp = moment();
              let updatedAt = moment(updatedSprint.updatedAt);

              // it should be less then 5 seconds difference
              assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

              done();
          });
      });
    });
  });

  describe("when patching(updating) invalid resource to /sprints/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/sprints/0b3360c9-9f2e-4409-b69b-931d5c36533d")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  describe("when patching(updating) non-existing resource to /sprints/:id", function () {

    var newSprint = {
      "name": "new sprint 123",
      "description": "This is long description of new sprint 123...",
      "sequenceNumber": 3,
      "status": "deleted",
      "tags": [
        "b2b3181c-5f41-49c0-a99f-b176d3526465",
        "cf550d60-3b86-44e7-9c46-f82ffd7efa42"
      ]
    };

    it("should respond with 409 and error message", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/sprints/123493a5-2e83-46de-be32-c434d68aa789")
        .send(newSprint)
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when puting invalid non-existing resource to /sprints/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/sprints/10")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- DELETING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when deleting resource /sprints/:id", function () {

    let sprintId = "72db1cef-4e21-4946-a16e-1e0690d69dc7";

    let expectedProject = JSON.parse(JSON.stringify(expectedSprints.project));
    delete expectedProject.boards[boardId].sprints[sprintId];

    it("should respond with 204 and deleted resource", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprintId)
        .expect("Content-Type", /json/)
        .expect(204)
        .end(function (err, res) {
          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/sprints/" + sprintId)
            .expect("Content-Type", /json/)
            .expect(404)
            .end(function (err, res) {

                request(app)
                  .get("/projects/" + projectId)
                  .expect(200)
                  .end(function (err, res) {
                    var project = JSON.parse(res.text);
                    assert.deepEqual(project, expectedProject);
                    done();
                  });
              });
      });
    });
  });

  describe("when deleting non-existing resource /sprints/:id", function () {

    it("should respond with 404", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/" + boardId + "/sprints/94e75c05-003b-493c-99e4-4650ef7836b0")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when deleting non-existing resource /sprints/:id - penetration testing", function () {

    it("should respond with 400", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/" + boardId + "/sprints/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });
});
