
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");

var assert  = require("chai").assert;
var app     = require("../../app");
var request = require("supertest");
var moment  = require("moment");
var _       = require(appRootPath + "/lib/utility/underscore");

var TestHelper = require("../../lib/test/testHelper");
var testHelper = new TestHelper(__filename);

var expectedLanes = testHelper.getDataProvider("sampleLanes");

let projectId = "460ad418-ac09-44bb-9941-a2a645032c01";
let boardId = "3c08ed3c-8257-47e3-804b-fe74b84d9704";


describe("Lanes resources", function () {

  beforeEach(function (done) {
    testHelper.executeFixtureScript("sampleProjects", done);
  });

  describe("when requesting resource /project/:projectId/boards/:boardId/lanes", function () {
    it("should respond with lanes", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var lanes = JSON.parse(res.text);
          _.each(expectedLanes.default, function(result) {
            assert.include(lanes, result);
          });
          done();
      });
    });
  });

  describe("when requesting resource /lanes with first pack of fields", function () {
    it("should respond with fields (part1) lanes", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes" +
          "?conditions[id]=" + expectedLanes.lane3.id +
          "&fields[]=id" +
          "&fields[]=name" +
          "&fields[]=description"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var lane = JSON.parse(res.text);
          assert.deepEqual(lane, expectedLanes.partial1);
          done();
      });
    });
  });

  describe("when requesting resource /lanes with second pack of fields", function () {
    it("should respond with fields (part2) lanes", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes" +
          "?conditions[id]=" + expectedLanes.lane4.id +
          "&fields[]=status" +
          "&fields[]=sequenceNumber" +
          "&fields[]=tags"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var lane = JSON.parse(res.text);
          assert.deepEqual(lane, expectedLanes.partial2);
          done();
      });
    });
  });

  describe("when requesting resource /lanes with specified order", function () {
    it("should respond with ordered lanes", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes" +
          "?order[sequenceNumber]=asc" +
          "&order[name]=asc"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var lanes = JSON.parse(res.text);
          assert.deepEqual(lanes, expectedLanes.orderBySequenceNumber);
          done();
      });
    });
  });

  describe("when requesting resource /lanes with specified limit", function () {
    it("should respond with limited number of lanes", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes" +
          "?limit=3"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var lanes = JSON.parse(res.text);
          assert.deepEqual(lanes.length, 3);
          done();
      });
    });
  });

  describe("when requesting resource /lanes with specified limit and order", function () {
    it("should respond with limited number of ordered lanes", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes" +
          "?order[sequenceNumber]=asc" +
          "&order[name]=asc" +
          "&limit=3"
        )
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          var lanes = JSON.parse(res.text);
          assert.deepEqual(lanes, expectedLanes.limitedAndOrdered);
          done();
      });
    });
  });

  // --------------------------------------------------------------------------- //
  // -------------------------- GETTING SINGLE RECORD -------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when requesting resource /lanes/:id", function () {
    it("should respond with single lane", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/25fe4e2e-7646-4121-8d20-684389e9fbca")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
        var lane = JSON.parse(res.text);
        assert.deepEqual(lane, expectedLanes.singleById);
        done();
      });
    });
  });

  describe("when requesting non-existing resource /lanes/:id", function () {
    it("should respond with 404", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/4924d508-4a56-460d-ad0f-eb21585be0dd")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when requesting non-existing resource /lanes/:id - penetration testing", function () {
    it("should respond with 400", function (done) {
      request(app)
        .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- CREATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when posting resource to /lanes (with dates)", function () {

    var newLane = {
      "name": "new lane 123",
      "description": "This is long description of new lane 123...",
      "sequenceNumber": 3,
      "status": "deleted",
      "tags": [
        "b2b3181c-5f41-49c0-a99f-b176d3526465",
        "cf550d60-3b86-44e7-9c46-f82ffd7efa42"
      ],
      "createdAt": "1976-02-22 06:33:02",
      "updatedAt": "1990-04-06 09:16:50"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards/" + boardId + "/lanes")
        .send(newLane)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {
          var lane = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/projects/" + projectId + "/boards/" + boardId + "/lanes/" + lane.id);

          newLane.id = lane.id;
          newLane.createdAt = lane.createdAt;
          newLane.updatedAt = null;
          assert.deepEqual(lane, newLane);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(lane.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + lane.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
            var lane = JSON.parse(res.text);
            assert.deepEqual(lane, newLane);
            done();
          });
      });
    });
  });

  describe("when posting resource to /lanes (without dates)", function () {

    var newLane = {
      "name": "new lane 123",
      "description": "This is long description of new lane 123...",
      "sequenceNumber": 3,
      "status": "deleted",
      "tags": [
        "b2b3181c-5f41-49c0-a99f-b176d3526465",
        "cf550d60-3b86-44e7-9c46-f82ffd7efa42"
      ]
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards/" + boardId + "/lanes")
        .send(newLane)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {
          var lane = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/projects/" + projectId + "/boards/" + boardId + "/lanes/" + lane.id);

          newLane.id = lane.id;
          newLane.createdAt = lane.createdAt;
          newLane.updatedAt = null;
          assert.deepEqual(lane, newLane);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(lane.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + lane.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
            var lane = JSON.parse(res.text);
            assert.deepEqual(lane, newLane);
            done();
          });
      });
    });
  });

  describe("when posting resource to /lanes (without tags)", function () {

    var newLane = {
      "name": "new lane 123",
      "description": "This is long description of new lane 123...",
      "sequenceNumber": 3,
      "status": "deleted"
    };

    it("should respond with 201 and URL to newly created resource", function (done) {
      request(app)
        .post("/projects/" + projectId + "/boards/" + boardId + "/lanes")
        .send(newLane)
        .expect("Content-Type", /json/)
        .expect(201)
        .end(function (err, res) {
          var lane = JSON.parse(res.text);

          var location = res.header.location;
          assert.equal(location, "/projects/" + projectId + "/boards/" + boardId + "/lanes/" + lane.id);

          newLane.id = lane.id;
          newLane.createdAt = lane.createdAt;
          newLane.updatedAt = null;
          newLane.tags = [];
          assert.deepEqual(lane, newLane);

          // comparing times
          let currentTimestamp = moment();
          let createdAt = moment(lane.createdAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(createdAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + lane.id)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
            var lane = JSON.parse(res.text);
            assert.deepEqual(lane, newLane);
            done();
          });
      });
    });
  });

  describe("when posting invalid lane resource to /lanes", function () {

    it("should respond with 400", function (done) {
      request(app)
      .post("/projects/" + projectId + "/boards/" + boardId + "/lanes")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- UPDATING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when patching(updating) resource to /lanes/:id (with dates)", function () {

    let laneId = "0b3360c9-9f2e-4409-b69b-931d5c36533d";

    var updatedLane = {
      "name": "new lane 123",
      "description": "This is long description of new lane 123...",
      "sequenceNumber": 3,
      "status": "deleted",
      "tags": [],
      "createdAt": "1976-02-22 06:33:02",
      "updatedAt": "1990-04-06 09:16:50"
    };

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + laneId)
        .send(updatedLane)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {

          updatedLane.id = laneId;
          var lane = JSON.parse(res.text);

          updatedLane.createdAt = "2011-02-03T09:52:19.000Z";
          updatedLane.updatedAt = lane.updatedAt;
          assert.deepEqual(updatedLane, lane);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(lane.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + laneId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var lane = JSON.parse(res.text);
              assert.deepEqual(lane, updatedLane);
              done();
          });
      });
    });
  });

  describe("when patching(updating) resource to /lanes/:id (without dates and tags)", function () {

    let laneId = "0b3360c9-9f2e-4409-b69b-931d5c36533d";

    var updatedLane = {
      "name": "new lane 123",
      "description": "This is long description of new lane 123...",
      "sequenceNumber": 3,
      "status": "deleted"
    };

    it("should respond with 200 and updated resource", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + laneId)
        .send(updatedLane)
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {

          updatedLane.id = laneId;
          var lane = JSON.parse(res.text);

          updatedLane.createdAt = "2011-02-03T09:52:19.000Z";
          updatedLane.updatedAt = lane.updatedAt;
          updatedLane.tags = ["631868e9-2cdb-4676-85ec-e8c26a770b47"];
          assert.deepEqual(updatedLane, lane);

          // comparing times
          let currentTimestamp = moment();
          let updatedAt = moment(lane.updatedAt);

          // it should be less then 5 seconds difference
          assert.equal(currentTimestamp.diff(updatedAt) < 5000, true);

          request(app)
            .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + laneId)
            .expect("Content-Type", /json/)
            .expect(200)
            .end(function (err, res) {
              var lane = JSON.parse(res.text);
              assert.deepEqual(lane, updatedLane);
              done();
          });
      });
    });
  });

  describe("when patching(updating) invalid resource to /lanes/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/lanes/0b3360c9-9f2e-4409-b69b-931d5c36533d")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  describe("when patching(updating) non-existing resource to /lanes/:id", function () {

    var newLane = {
      "name": "new lane 123",
      "description": "This is long description of new lane 123...",
      "sequenceNumber": 3,
      "status": "deleted",
      "tags": [
        "b2b3181c-5f41-49c0-a99f-b176d3526465",
        "cf550d60-3b86-44e7-9c46-f82ffd7efa42"
      ]
    };

    it("should respond with 200 and created resource", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/lanes/123493a5-2e83-46de-be32-c434d68aa789")
        .send(newLane)
        .expect("Content-Type", /json/)
        .expect(409, done);
    });
  });

  describe("when puting invalid non-existing resource to /lanes/:id", function () {

    it("should respond with 400", function (done) {
      request(app)
        .patch("/projects/" + projectId + "/boards/" + boardId + "/lanes/10")
        .send("invalid")
        .expect("Content-Type", /json/)
        .expect(400)
        .end(done);
    });
  });

  // --------------------------------------------------------------------------- //
  // ----------------------------- DELETING RECORD ----------------------------- //
  // --------------------------------------------------------------------------- //

  describe("when deleting resource /lanes/:id", function () {

    let laneId = "0d33391e-0402-4855-a0b3-d447dc7586c6";

    let expectedProject = JSON.parse(JSON.stringify(expectedLanes.project));
    delete expectedProject.boards[boardId].lanes[laneId];

    it("should respond with 204 and deleted resource", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + laneId)
        .expect("Content-Type", /json/)
        .expect(204)
        .end(function (err, res) {
        request(app)
          .get("/projects/" + projectId + "/boards/" + boardId + "/lanes/" + laneId)
          .expect("Content-Type", /json/)
          .expect(404)
          .end(function (err, res) {

              request(app)
                .get("/projects/" + projectId)
                .expect(200)
                .end(function (err, res) {

                  var project = JSON.parse(res.text);
                  assert.deepEqual(project, expectedProject);
                  done();
                });
            });
      });
    });
  });

  describe("when deleting non-existing resource /lanes/:id", function () {

    it("should respond with 404", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/" + boardId + "/lanes/94e75c05-003b-493c-99e4-4650ef7836b0")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });
  });

  describe("when deleting non-existing resource /lanes/:id - penetration testing", function () {

    it("should respond with 400", function (done) {
      request(app)
        .del("/projects/" + projectId + "/boards/" + boardId + "/lanes/12\"%20OR%201=1") // 12" OR 1=1
        .expect("Content-Type", /json/)
        .expect(400, done);
    });
  });
});
