
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var logger       = require("../logger/logger");
var Request      = require("../request/request");
var RequestError = require("../request/requestError");

module.exports.validate = function (req, res, next) {
  var request = new Request();
  request.reset();

  var util = require("util");
  logger.info("RequestMiddleware::validate - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query
  });

  try {

    request.inflate(req.query);

  } catch (err) {

    logger.error(err);

    if (err instanceof RequestError) {
      return res.status(err.statusCode).json(err.exportDetails());
    }

    return res.status(500).json("Internal Server Error");
  }

  req.query = request;
  next();
};
