
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var logger = require("../logger/logger");

module.exports = function (req, res) {
  logger.error("NotFoundMiddleware: Non matching routes found", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });
  res.status(404).json("Not Found");
};
