
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var _           = require(appRootPath + "/lib/utility/underscore");
var logger      = require(appRootPath + "/lib/logger/logger");

module.exports.id = function (req, res, next, id) {
  var pattern = /^[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}$/;
  if (id.match(pattern) === null) {
    logger.info("Invalid id provided", id);
    return res.status(400).json("Bad Request");
  }

  next();
};

module.exports.requestData = function (req, res, next) {
  if (_.isEmpty(req.body)) {
    logger.info("Invalid body provided", req.body);
    return res.status(400).json("Bad Request");
  }

  next();
};
