
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var winston     = require("winston");
var config      = require("../config/config");
var _           = require(appRootPath + "/lib/utility/underscore");
var util        = require("util");

class Logger
{
  constructor()
  {
    var transports = [];

    if (config.get("logger:transports:console:enabled")) {
      transports.push(
        new (winston.transports.Console)(
          config.get("logger:transports:console")
        )
      );
    }

    if (config.get("logger:transports:file:enabled")) {
      var fileConfig = config.get("logger:transports:file");
      fileConfig.filename = appRootPath + "/" + fileConfig.filename;
      transports.push(
        new (winston.transports.File)(
          fileConfig
        )
      );
    }

    if (config.get("logger:transports:logentries:enabled")) {
      // var Logentries = require("winston-logentries");
      transports.push(
        new (winston.transports.Logentries)(
          config.get("logger:transports:logentries")
        )
      );
    }

    this.logger = new winston.Logger({
      transports : transports
    });
  }

  _log(level, message, data)
  {
    // if error object
    if (_.isObject(message)) {
      message = "Message: " + message.message + "\n" +
                "Stack trace: " + message.stack + "\n" +
                "Additional data: " + util.inspect(message.data, {showHidden: true, depth: 5});
    }

    if (data && (!(data instanceof Error))) {
      message += "\n" + "Additional data: " + util.inspect(data, {showHidden: true, depth: 5});
    } else if (data && data instanceof Error) {
      message += "\n" + "Additional data: " + data.stack;
    }

    this.logger.log(level, message);
  }

  log(level, message, data)
  {
    this._log(level, message, data);
  }

  info(message, data)
  {
    this._log("info", message, data);
  }

  warn(message, data)
  {
    this._log("warn", message, data);
  }

  error(message, data)
  {
    this._log("error", message, data);
  }
}

module.exports = new Logger();
