
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var BaseEntity   = require(appRootPath + "/lib/entity/baseEntity");
var BaseGateway  = require("./baseGateway");
var Request      = require(appRootPath + "/lib/request/request");
var _            = require(appRootPath + "/lib/utility/underscore");
var GatewayError = require("./gatewayError");
var logger       = require(appRootPath + "/lib/logger/logger");
var inspector    = require("schema-inspector");

/**
 * RethinkdbGateway engine works in following way
 *
 * Selecting rows:
 *
 * 1. Filter by conditions connected to local table
 * 2. Join on each table with left side fully selected and right only
 *    partially mapped via fields[]
 * 3. Order by field(s)
 * 4. Limit the number of records
 * 5. Map(select) all fields without prefix and ones with prefix of
 *    current table alias
 *
 * ------------------------------------------------------
 *
 * Deleting rows is achieved by passing nested object:
 *
 * r.db("cerberus").table("Project")
 *   .get("460ad418-ac09-44bb-9941-a2a645032c01")
 *   .without({
 *     "boards" : {
 *       "10a98db6-fa2f-458f-ba88-4406bd389fe8":{
 *         "lanes": "972138be-7fd4-4ea6-aca0-7d03fc5e6242"
 *       }
 *     }
 *   }
 * )
 * Above will delete lane with id "972138be-7fd4-4ea6-aca0-7d03fc5e6242" from
 * board ("10a98db6-fa2f-458f-ba88-4406bd389fe8") from project
 * ("460ad418-ac09-44bb-9941-a2a645032c01")
 *
 */
class RethinkDbGateway extends BaseGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper"s gateway(i.e. dbConnection)
   */
  constructor(dataProvider)
  {
    if (!dataProvider) {
      dataProvider = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");
    }

    super(dataProvider);

    // Table name
    this.table = {
      "name": "Dummy",
      "alias": "d"
    };

    // Object describing comment"s relation
    this.relations = {};

    // Lists supported operators lists all valid operators that
    // are identical like MongoDb operators
    this.supportedOperators = {

      // logic operators
      "and": "AND",
      "or": "OR",

      // comparision operators
      "eq": "eq",            // Matches values that are exactly the same as the value specified in the query.
      "gt": "gt",            // Matches values that are greater than the value specified in the query.
      "gte": "ge",           // Matches values that are greater than or equal to the value specified in the query.
      "lt": "lt",            // Matches values that are less than the value specified in the query.
      "lte": "le",           // Matches values that are less than or equal to the value specified in the query.
      "ne": "ne",            // Matches all values that are not equal to the value specified in the query.
      "in": "IN",            // Matches values that exist in an array specified to the query.
      "nin": "NOT IN",       // Matches values that do not exist in an array specified to the query.
      "like": "LIKE",        // Matches values that are matching pattern passed in the value specified in the query.
      "contains": "contains" // Matches value that exists in array
    };
  }

  /**
   * Main method responsible for generating query from
   * request object
   *
   * @param  Request        request  Request object
   * @param  Array          entities List of connected entities
   * @param  RethinkDbQuery query    RethinkDb query
   * @return Array|Entity  entities Array of entities or entity
   */
  query(request, entities, query)
  {
    if (!(request instanceof Request)) {
      throw new GatewayError(
        "Invalid request provided. Request instance expected",
        request,
        500
      );
    }

    // if single entity change to array of one
    if (!_.isArray(entities)) {
      entities = [
        entities
      ];
    }

    this.request = request;
    this.entities = entities;
    this.joinedFields = {};
    this.fieldValidators = {};

    this.validateColumns();

    query = query ? query : this.dataProvider.table(this.table.name);

    // add conditions
    if (_.size(request.getConditions()) > 0) {
      var me = this;
      query = query.filter(function (doc) {
        return me.generateConditions(doc, request.getConditions());
      });
    }

    // add inner joins
    if (!_.isEmpty(request.getJoins())) {
      query = this.appendJoins(query);
    }

    // ----
    //
    //  if two steps below would be introduced after local map they could
    //  use aliased fields - here on the other hand they create
    //  REQUIREMENT to sort on actual names of fields before local aliases
    //
    // ----

    if (!_.isEmpty(request.getOrder())) {
      //var orderByRules = this.generateOrderByRules(queryData.order);
      //query = query.orderBy.apply(null, orderByRules);
      query = query.orderBy.apply(
        query,
        this.generateOrderByRules(request.getOrder())
      );
    }

    if (request.getLimit()) {
      query = query.limit(request.getLimit());
    }

    // select local fields
    if (!_.isEmpty(request.getFields())) {

      let queriedFields = request.getFields();

      if (!_.isArray(queriedFields) ||
        (_.isArray(queriedFields) && queriedFields.indexOf(this.table.alias + ".*") == -1)
      ) {
        var fieldsMap = this.generateFieldsMap(query);
        if (!_.isEmpty(fieldsMap)) {

          fieldsMap = _.extend(fieldsMap, this.joinedFields);

          var index, field;
          for (index in fieldsMap) {
            field = fieldsMap[index];
            fieldsMap[index] = this.dataProvider.row(field);
          }

          query = query.map(fieldsMap);
        }
      }

    }

    logger.info("RethinkDb query");
    logger.info(query.toString());

    return query.run();
  }

  /**
   * Method validates request against entity
   *
   * @param  Request request Query Builder populated by request
   * @param  Array|Entity  entities       An array of entities or entity
   * @todo Validate columns with connection to aliases
   */
  validateColumns(request, entities)
  {
    request = request ? request : this.request;
    entities = entities ? entities : this.entities;

    // if single entity change to array of one
    if (!_.isArray(entities)) {
      entities = [
        entities
      ];
    }

    // every element in array needs to be a BaseEntity
    var entity = null;
    var validColumns = ["*"]; // it"s always allowed to query by star
    var index = "";
    for (index in entities) {
      entity = entities[index];
      if (!(entity instanceof BaseEntity)) {
        throw new GatewayError(
          "Invalid entity(s) provided. Instance of BaseEntity expected",
          entity,
          501
        );
      }

      validColumns = _.union(validColumns, entity.getProperties());
      this.fieldValidators = _.extend(this.fieldValidators, entity.getFields());
    }

    var usedFields = request.getUsedFields();

    // validate fields
    var alias = null;
    var field = null;
    var tempField = null;
    var fieldParts = [];
    var tempIndex  = "";
    for (tempIndex in usedFields) {
      field = usedFields[tempIndex];
      tempField = field;

      // escape alias if exists
      if (tempField.indexOf(".") > -1) {
        fieldParts = tempField.split(".");
        alias = fieldParts.shift();
        if (!_.contains(this.getAllowedColumnAliases(), alias)) {
          throw new GatewayError(
            "Invalid alias for field provided " + alias,
            alias,
            1
          );
        }
        tempField = fieldParts.join("");
      }

      if (!_.contains(validColumns, tempField)) {
        throw new GatewayError(
          "Invalid field passed: " + field,
          {
            field: field,
            validColumns: validColumns
          },
          2
        );
      }
    }
  }

  /**
   * Generates map of fields using request and optionality
   * alias
   *
   * @param query   query   Rethikdb query
   * @param Request request Instance of Request
   * @param string  alias   If passed method generates map
   * only for fields prefixed with passed alias
   * @return object Fields map
   */
  generateFieldsMap(query, request, alias)
  {
    request = request ? request : this.request;

    var field = "";
    var index = "";
    var fields = {};

    var fieldsList = request.getFields();

    for (index in fieldsList) {

      field = fieldsList[index];

      if (isNaN(index) && field.indexOf(".") > -1) {
        throw new GatewayError(
          "Aliases are not allowed in aliases",
          field,
          502
        );
      }

      if (index == this.table.alias + ".*" || index == "*") {
        continue; // skip
      }

      // skip all non-aliased fields
      if (alias && index.indexOf(alias + ".") == -1) {
        continue; // skip
      }

      field = this.escapeColumnName(field);
      index = this.escapeColumnName(index);

      if (isNaN(index)) {
        fields[field] = index;
      } else {
        fields[field] = field;
      }
    }

    return fields;
  }

  getAllowedColumnAliases()
  {
    if (this.allowedColumnAliases && !_.isEmpty(this.allowedColumnAliases)) {
      return this.allowedColumnAliases;
    }

    // refering columns from main table with alias is valid
    this.allowedColumnAliases = [];
    this.allowedColumnAliases.push(this.table.alias);

    var relation = null;
    var relationName = null;
    for (relationName in this.relations) {

      relation = this.relations[relationName];

      if (!_.has(relation, "defaultAlias")) {
        throw new GatewayError(
          "Invalid relation definition for relation " + relationName,
          relationName,
          502
        );
      }

      this.allowedColumnAliases.push(relation.defaultAlias);
    }

    return this.allowedColumnAliases;
  }

  /**
   * Method generates conditions for RethinkDB query
   *
   * @param Object doc            Rethinkdb"s object representing row/document
   * @param Object conditionsData Data to create conditions
   */
  generateConditions(doc, conditionsData)
  {
    return this.generateConditionLevel(doc, conditionsData, 1);
  }

  /**
   * Method generates a single condition level or calls
   * recursively itself
   *
   * @param  object doc            Rethinkdb"s object representing row/document
   * @param  object conditionData Object describing condition"s level(s)
   * @param  int    level         Level of current recursive call(safety fuse)
   * @return object expr Expression object(part of squel package)
   */
  generateConditionLevel(doc, conditionData, level)
  {
    var index, value, nestedIndex, nestedValue, operator;
    var dbConditions = [], nestedDbConditions = [];

    for (index in conditionData) {

      value = conditionData[index];

      if (index === "or" || index === "and") {
        if (!Array.isArray(value)) {
          throw new GatewayError(
            "Invalid argument of logic operator passed. Array of conditions expected",
            value,
            3
          );
        }

        if (value.length < 2) {
          throw new GatewayError(
            "Logic operator expects array of at least two conditions",
            value,
            9
          );
        }

        if (level > 3) {
          throw new GatewayError(
            "Only 3 levels of nested conditions are supported",
            conditionData,
            4
          );
        }

        nestedValue = null;
        for (nestedIndex in value) {
          nestedValue = value[nestedIndex];
          level += 1;
          nestedDbConditions.push(
            this.generateConditionLevel(doc, nestedValue, level)
          );
        }

        dbConditions.push(this.dataProvider[index].apply(null, nestedDbConditions));

      } else {

        nestedValue = null;

        // simple conditions
        // {
        //   field: value
        // }
        if (_.isString(value) || _.isNumber(value) || _.isBoolean(value)) {
          if (value === "") {
            //expr.and(this.escapeColumnName(index) + " IS NULL");
            dbConditions.push(doc(this.escapeColumnName(index)).eq(null));
          } else {
            // expr.and(this.escapeColumnName(index) + " = ?");
            // expr.parameters.push(value);
            // console.log("SINGLE CONDITION: ", this.escapeColumnName(index), value);
            let result = inspector.sanitize(
              this.fieldValidators[this.escapeColumnName(index)],
              value
            );

            dbConditions.push(
              doc(this.escapeColumnName(index)).eq(
                result.data
              )
            );
          }
        } else if (_.isArray(value)) {

          // {
          //   field: [4, 5]
          // }
          dbConditions.push(
            this.dataProvider.expr(value).contains(doc(this.escapeColumnName(index)))
          );

          continue;

          // expr.and(this.escapeColumnName(index) + " IN ?");
          // expr.parameters.push(value);

        } else {

          // complex conditions
          // {
          //   field: {
          //     "gt" : 5
          //   }
          // }
          // or
          // {
          //   field: {
          //     "in" : [5, 4]
          //     // or
          //     "nin" : [5, 4]
          //   }
          // }
          for (operator in value) {

            nestedValue = value[operator];

            if (_.isArray(nestedValue)) {

              if (operator == "in") {
                dbConditions.push(
                  this.dataProvider.expr(nestedValue).contains(doc(this.escapeColumnName(index)))
                );
              } else if (operator == "nin") {
                dbConditions.push(
                  this.dataProvider.expr(nestedValue).contains(doc(this.escapeColumnName(index))).not()
                );
              // it's probably "contains"
              } else {
                dbConditions.push(
                  this.dataProvider.expr(nestedValue).setIntersection(doc(this.escapeColumnName(index))).isEmpty().not()
                );
              }

              continue;
            }

            // expr.and(this.escapeColumnName(index) + " " + this.decodeOperator(operator) + " ?");
            // expr.parameters.push(nestedValue);
            let result = inspector.sanitize(
              this.fieldValidators[this.escapeColumnName(index)],
              nestedValue
            );

            dbConditions.push(
              doc(this.escapeColumnName(index))[this.supportedOperators[operator]](result.data)
            );
          }
        }
      }
    }

    if (dbConditions.length > 1) {
      return this.dataProvider.and.apply(null, dbConditions);
    }

    return dbConditions.pop();
  }

  escapeColumnName(columnName)
  {
    if (columnName.indexOf(".") > -1) {
      var columnNameParts = columnName.split(".");
      columnName = columnNameParts[1];
    }
    return columnName;
  }

  /**
   * Method generates order part of query
   *
   * Expected data:
   * {
   *   "fieldA": "desc",
   *   "fieldB": "asc"
   * }
   *
   * @return string sql Generated part of valid RethinkDb query
   */
  generateOrderByRules(orderData)
  {
    let orderField, orderByRules = [];

    for (orderField in orderData) {
      if (orderData[orderField] === "desc") {
        orderByRules.push(this.dataProvider.desc(orderField));
        continue;
      }

      orderByRules.push(orderField);
    }

    return orderByRules;
  }

  /**
   * Appends joins to query object
   *
   * @param  object query        [description]
   * @param  object request [description]
   * @return {[type]}            [description]
   */
  appendJoins(query, request)
  {
    request = request ? request : this.request;

    var joins = request.getJoins();

    if (!(_.isArray(joins))) {
      throw new GatewayError(
        "Invalid joins list provided. Array expected",
        joins,
        503
      );
    }

    var joinedFields = {};
    var joinName = "";
    var index = "";
    var relation, fieldsMap, selectedFields;
    for (index in joins) {
      joinName = joins[index];

      if (!_.has(this.relations, joinName)) {
        throw new GatewayError(
          "Invalid join name provided",
          joinName,
          5
        );
      }

      relation = this.relations[joinName];

      // step 1 join and get left and right
      query = query.outerJoin(
        this.dataProvider.table(relation.table),
        function (baseTable, joinedTable) {
          return joinedTable(relation.targetColumn).eq(baseTable(relation.localColumn));
        }
      );

      // step 2 minimize right and leave left as it is
      fieldsMap = this.generateFieldsMap(
        query,
        request,
        relation.defaultAlias
      );

      if (_.isObject(fieldsMap) && !_.isEmpty(fieldsMap)) {

        // unset fieldsMap of local table
        selectedFields = request.getFields();
        var index, field;
        for (index in selectedFields) {
          field = selectedFields[index];
          if (index.indexOf(relation.defaultAlias + ".") > -1) {

            if (field.indexOf(this.table.alias) === 0) {
              continue;
            }

            delete selectedFields[index];
            joinedFields[field] = field;
          }

          if (isNaN(index) && field.indexOf(relation.defaultAlias + ".") > -1) {
            throw new GatewayError(
              "Aliases are not allowed in aliases",
              relation,
              6
            );
          }
        }

        // prepare for map
        for (index in fieldsMap) {
          field = fieldsMap[index];
          if (field == "*") {
            fieldsMap = this.dataProvider.row("right");
          } else {
            fieldsMap[index] = this.dataProvider.row("right")(field);
          }
        }

        query = query.map({
          left : this.dataProvider.row("left"),
          right : fieldsMap
        });
      } else if (typeof fieldsMap  === "boolean") {

        // merge whole thing!

      } else {
        // error
        throw new GatewayError(
          "No fields selected from joined collection",
          fieldsMap,
          7
        );
      }

      // step 3 zip both sets together
      query = query.zip();
    }

    this.joinedFields = joinedFields;

    return query;
  }

  /**
   * Generic insert method creates new object in table
   *
   * @param BaseEntity entity BaseEntity to be added
   * @return Promise promise Promise of new entity data after
   * being inserted
   */
  insert(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of BaseEntity expected",
        entity,
        504
      );
    }

    if (entity.has("createdAt")) {
      entity.set("createdAt", new Date());
    }

    if (entity.has("updatedAt")) {
      entity.set("updatedAt", null);
    }

    let entityData = entity.export();
    delete entityData.id;

    var me = this;
    var query = this.dataProvider.table(this.table.name)
      .insert(entityData);

    logger.info("RethinkDb query");
    logger.info(query.toString());

    return query.run()
      .then(function (data) {

        logger.info("RethinkDb response", data);

        if (!data.generated_keys ||
          !_.isArray(data.generated_keys) ||
          _.isEmpty(data.generated_keys)
        ) {
          throw new GatewayError(
            "Something gone wrong",
            data,
            503
          );
        }

        var id = data.generated_keys.pop();
        var request = new Request();
        request.setConditions({id: id});
        return me.fetchOne(request);
      });
  }

  /**
   * Generic update method updates object in table using
   * it"s id
   *
   * @param BaseEntity entity BaseEntity to be used to update old one
   * @return Promise promise Promise of new entity data after
   * update
   */
  update(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of BaseEntity expected",
        entity,
        504
      );
    }

    if (!entity.getId()) {
      throw new GatewayError(
        "Invalid logic. Insert should be called",
        entity,
        505
      );
    }

    var me = this;
    return this.dataProvider.table(this.table.name)
      .get(entity.getId())
      .then(function (entityFromDb) {
        if (!entityFromDb) {
          throw new GatewayError(
            "Resource does not exits",
            {},
            409
          );
        }

        if (entity.has("createdAt")) {
          entity.set("createdAt", entityFromDb.createdAt);
        }

        if (entity.has("updatedAt")) {
          entity.set("updatedAt", new Date());
        }

        return me.dataProvider.table(me.table.name)
          .get(entity.getId())
          .update(entity.export());

      }).then(function () {

        var request = new Request();
        request.setConditions({id: entity.getId()});
        return me.fetchOne(request);

      });
  }

  /**
   * Generic replace method replaces object in table using
   * it"s id
   *
   * @param BaseEntity entity BaseEntity to be used to replace old one
   * @return Promise promise Promise of new entity data after
   * replace
   */
  replace(entity)
  {
    if (!(entity instanceof BaseEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of BaseEntity expected"
      );
    }

    if (!entity.getId()) {
      throw new GatewayError(
        "Invalid logic. Insert should be called"
      );
    }

    var me = this;
    return this.dataProvider.table(this.table.name)
      .get(entity.getId())
      .then(function(currentEntityData) {

        if (!currentEntityData) {
          throw new GatewayError(
            "Resource does not exist",
            {},
            409
          );
        }

        var newEntityData = entity.export();
        newEntityData.createdAt = new Date(currentEntityData.createdAt);
        newEntityData.updatedAt = new Date();

        return me.dataProvider.table(me.table.name)
          .get(entity.getId())
          .replace(newEntityData)
          .then(function () {
            var request = new Request();
            request.setConditions({id: entity.getId()});
            return me.fetchOne(request);
          });
      });
  }

  /**
   * Generic delete method removes object from table by id
   *
   * @param string id BaseEntity"s identifier
   * @return Promise promise Promise of result of deleting
   * entity
   */
  delete(id)
  {
    var me = this;
    return this.dataProvider.table(this.table.name)
      .get(id)
      .then(function(entity) {

        if (!entity) {
          throw new GatewayError(
            "Resource does not exits",
            {},
            409
          );
        }

        return me.dataProvider.table(me.table.name)
          .get(id)
          .delete();
      });
  }

  /**
   * Method fetches single(first) record matching passed query builder"s
   * criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      results      Promise of query results
   */
  fetchOne(request) {

    if (!(request instanceof Request)) {
      throw new GatewayError(
        "Invalid request provided. Request instance expected",
        request,
        505
      );
    }

    request.setLimit(1);

    return this.fetchAll(request)
      .then(function (entities) {

        if (!_.isArray(entities)) {
          throw new GatewayError(
            "Invalid data returned from reThinkDb",
            entities,
            506
          );
        }

        if (_.isEmpty(entities)) {
          return null;
        }

        return entities.pop();
      });
  }

  /**
   * Method returns current UTC date object
   *
   * @return Date date Current UTC date object
   */
  getCurrentUtcDate()
  {
    let now = new Date();
    return new Date(
      now.getUTCFullYear(),
      now.getUTCMonth(),
      now.getUTCDate(),
      now.getUTCHours(),
      now.getUTCMinutes(),
      now.getUTCSeconds()
    );
  }
}

module.exports = RethinkDbGateway;
