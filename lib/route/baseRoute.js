
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var BaseService  = require(appRootPath + "/lib/service/baseService");
var logger       = require(appRootPath + "/lib/logger/logger");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");
var RouteError   = require(appRootPath + "/lib/route/routeError");

class BaseRoute
{
  /**
   * Constructor allows to inject service
   *
   * @param BaseService service Service
   */
  constructor(service)
  {
    if (!(service instanceof BaseService)) {
      throw new RouteError(
        "Invalid instance of service provided. Object extending BaseService expected",
        {
          service: service
        }
      );
    }

    this.service = service;
  }

  get(req, res)
  {
    logger.info(this.constructor.name + "::get - request received", {
      httpMethod: req.method,
      url: req.url,
      query: req.query.export()
    });

    var me = this;
    var nestedArguments = this.getNestedArguments(req);
    nestedArguments.push(req.query);

    try {
      this.service.fetchAll.apply(this.service, nestedArguments)
        .then(function (boards) {
          logger.info(me.constructor.name + "::get - 200 response sent back", {
            boards: boards
          });
          return res.status(200).json(boards);
        })
        .catch(function (error) {
          return errorHandler.resolve(error, req, res);
        });
    } catch (error) {
      return errorHandler.resolve(error, req, res);
    }
  };

  getById(req, res)
  {
    logger.info(this.constructor.name + "::getById - request received", {
      httpMethod: req.method,
      url: req.url,
      query: req.query.export()
    });

    var me = this;
    var nestedArguments = this.getNestedArguments(req);
    nestedArguments.push(req.params.id);

    try {
      this.service.fetchById.apply(this.service, nestedArguments)
        .then(function (board) {
          if (!board) {
            logger.info(me.constructor.name + "::getById - 404 response sent back");
            return res.status(404).json("Not Found");
          }
          logger.info(me.constructor.name + "::getById - 200 response sent back", {
            board: board
          });
          return res.status(200).json(board);
        })
        .catch(function (error) {
          return errorHandler.resolve(error, req, res);
        });
    } catch (error) {
      return errorHandler.resolve(error, req, res);
    }
  };

  create(req, res)
  {
    logger.info(this.constructor.name + "::create - request received", {
      httpMethod: req.method,
      url: req.url,
      body: req.body,
      params: req.params
    });

    var me = this;
    var nestedArguments = this.getNestedArguments(req);
    nestedArguments.push(req.body);

    try {
      this.service.create.apply(this.service, nestedArguments)
        .then(function (entity) {
          if (entity === null) {
            logger.info(me.constructor.name + "::create - 404 response sent back");
            return res.status(404).json("Not Found");
          }

          var locationHeader = me.generateUrl(req, entity);
          res.setHeader("Location", locationHeader);
          logger.info(me.constructor.name + "::create - 201 response sent back", {
            locationHeader: locationHeader,
            entity: entity
          });
          return res.status(201).json(entity.export());
        })
        .catch(function (error) {
          return errorHandler.resolve(error, req, res);
        });
    } catch (error) {
      return errorHandler.resolve(error, req, res);
    }
  };

  update(req, res)
  {
    logger.info(this.constructor.name + "::update - request received", {
      httpMethod: req.method,
      url: req.url,
      body: req.body,
      params: req.params
    });

    req.body.id = req.params.id;
    var me = this;
    var nestedArguments = this.getNestedArguments(req);
    nestedArguments.push(req.body);

    try {
      this.service.update.apply(this.service, nestedArguments)
        .then(function (board) {
          if (board === null) {
            logger.info(me.constructor.name + "::update - 409 response sent back");
            return res.status(409).json("Conflict");
          }
          logger.info(me.constructor.name + "::update - 200 response sent back", {
            board: board
          });
          return res.status(200).json(board.export());
        })
        .catch(function (error) {
          return errorHandler.resolve(error, req, res);
        });
    } catch (error) {
      return errorHandler.resolve(error, req, res);
    }
  };

  replace(req, res)
  {
    logger.info(this.constructor.name + "::replace - request received", {
      httpMethod: req.method,
      url: req.url,
      body: req.body,
      params: req.params
    });

    var me = this;
    var nestedArguments = this.getNestedArguments(req);
    nestedArguments.push(req.body);

    try {
      this.service.replace.apply(this.service, nestedArguments)
        .then(function (board) {
          if (board === null) {
            logger.info(me.constructor.name + "::replace - 409 response sent back");
            return res.status(409).json("Conflict");
          }
          logger.info(me.constructor.name + "::replace - 200 response sent back", {
            board: board
          });
          return res.status(200).json(board.export());
        })
        .catch(function (error) {
          return errorHandler.resolve(error, req, res);
        });
    } catch (error) {
      return errorHandler.resolve(error, req, res);
    }
  };

  delete(req, res)
  {
    logger.info(this.constructor.name + "::delete - request received", {
      httpMethod: req.method,
      url: req.url,
      body: req.body,
      params: req.params
    });

    var me = this;
    var nestedArguments = this.getNestedArguments(req);
    nestedArguments.push(req.params.id);

    try {
      this.service.delete.apply(this.service, nestedArguments)
        .then(function (numberOfDeleted) {
          if (numberOfDeleted === 0) {
            logger.info(me.constructor.name + "::delete - 404 response sent back");
            return res.status(404).json("Not Found");
          }
          logger.info(me.constructor.name + "::delete - 204 response sent back");
          return res.status(204).json("No Content");
        })
        .catch(function (error) {
          return errorHandler.resolve(error, req, res);
        });
    } catch (error) {
      return errorHandler.resolve(error, req, res);
    }
  };

  /**
   * Method returns list of nested parameters that should be passed to gateway
   * methods
   *
   * @return Array argumentsList List of arguments
   */
  getNestedArguments()
  {
    return [];
  }

  /**
   * Method generates URL based on current request and entity
   *
   * @param ExpressRequest request Request object
   * @param BaseEntity     entity  Entity object
   * @return string url Generated URL
   */
  generateUrl(request, entity)
  {
    return request.originalUrl + "/" + entity.getId();
  }
}

module.exports = BaseRoute;
