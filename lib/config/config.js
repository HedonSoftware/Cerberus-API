
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var nconf       = require("nconf");
var fs          = require("fs");
var ConfigError = require("./configError");

class Config
{
  constructor()
  {
    var environment = this.getEnvironment();

    if (!fs.existsSync(appRootPath + "/config/" + environment + ".json")) {
      throw new ConfigError(
        "Unable to find configuration file for specified environment( " + environment + " )"
      );
    }

    nconf.add("env-file", {type: "file", file: appRootPath + "/config/" + environment + ".env.json"});
    nconf.add("default-file", {type: "file", file: appRootPath + "/config/" + environment + ".json"});
  }

  getEnvironment()
  {
    nconf.argv().env("_");
    return nconf.get("NODE:ENV") || "app";
  }

  get(key)
  {
    return nconf.get(key);
  }
}

module.exports = new Config();
