
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var GatewayError = require(appRootPath + "/lib/gateway/gatewayError");
var RequestError = require(appRootPath + "/lib/request/requestError");
var ServiceError = require(appRootPath + "/lib/service/serviceError");
var BaseError    = require(appRootPath + "/lib/error/baseError");
var logger       = require(appRootPath + "/lib/logger/logger");
var _            = require(appRootPath + "/lib/utility/underscore");

class ErrorHandler
{
  constructor()
  {
    this.errorMapping = {
      "GatewayError": {
        "ERROR_DUPLICATE_INDEX": 409,
        "ERROR_INVALID_FIELD": 400,
      },
      "RequestError": {
        "ER_INVALID_LIMIT": 400,
        "ER_INVALID_ORDER": 400,
      }
    };
  }

  resolve(error, req, res)
  {
    logger.error(error);

    // by default
    var httpStatusCode = 500;
    var errorMessage = error;

    if (_.isObject(error)) {
      errorMessage = error.message;
    }

    if (error instanceof BaseError && error.errorCode) {
      httpStatusCode = error.errorCode;
    }

    if (error instanceof GatewayError && error.errorCode) {
      httpStatusCode = 400;
      if (error.errorCode > 499) {
        httpStatusCode = 500;
      } else if(error.errorCode === 409) {
        httpStatusCode = 409;
      } else if(error.errorCode === 404) {
        httpStatusCode = 404;
      }
      errorMessage = error.exportDetails();
    }

    if (error instanceof ServiceError && error.errorCode) {
      httpStatusCode = 400;
      if (error.errorCode > 499) {
        httpStatusCode = 500;
      } else if(error.errorCode === 409) {
        httpStatusCode = 409;
      } else if(error.errorCode === 404) {
        httpStatusCode = 404;
      }
      errorMessage = error.exportDetails();
    }

    if (error instanceof RequestError) {
      httpStatusCode = 400;
    }

    return res.status(httpStatusCode).json(errorMessage);
  }
}

module.exports = new ErrorHandler();
