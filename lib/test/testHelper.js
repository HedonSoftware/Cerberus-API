
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var _           = require(appRootPath + "/lib/utility/underscore");
var fs          = require("fs");
var BaseError   = require("../error/baseError");
var logger      = require("../logger/logger");

class TestHelper
{
  constructor(testDir)
  {
    this.testPath = null;
    this.testFixturesDirPath = null;
    this.testDataProvidersDirPath = null;

    this.setTestPath(testDir);
  }

  /**
   * Method sets test's path
   *
   * @param string testPath Tests path to be set
   * @return self this Fluent interface
   */
  setTestPath(testPath)
  {
    try {
      if (!_.isString(testPath) || !fs.realpathSync(testPath)) {
          // lovely interface of fs package...
      }
    } catch (e) {
      throw new BaseError("Invalid test path provided");
    }

    this.testPath = testPath;
    return this;
  }

  getTestPath()
  {
    return this.testPath;
  }

  /**
   * Sets path to test's fixtures directory
   *
   * @param String testFixturesDirPath Path to test's fixtures directory
   * @return Helper this Fluent interface
   */
  setTestFixturesDirPath(testFixturesDirPath)
  {
    try {
      if (!_.isString(testFixturesDirPath) || !fs.realpathSync(testFixturesDirPath)) {
        // lovely interface of fs package...
      }
    } catch (e) {
      throw new BaseError("Invalid test's fixtures path provided");
    }

    this.testFixturesDirPath = testFixturesDirPath;
    return this;
  }

  /**
   * Returns path to test's fixtures directory
   *
   * @return String testFixturesDirPath Path to test's fixtures directory
   */
  getTestFixturesDirPath()
  {
    if (!_.isString(this.testFixturesDirPath)) {
      var testPath = this.getTestPath();

      // deleting ".js"
      var tempFixturesDir = testPath.substr(0, testPath.length-3);

      // getting base by everything up to "/test/"
      var testDirPath = tempFixturesDir.substr(0, tempFixturesDir.indexOf("/test/"));

      // relPath is everything after "/test/"
      var relPath = tempFixturesDir.substr(testDirPath.length + 6);

      // adding fixture and testing does it exists
      if (!fs.realpathSync(testDirPath + "/test/fixtures/" + relPath)) {
        return null;
      }

      this.testFixturesDirPath = testDirPath + "/test/fixtures/" + relPath + "/";
    }

    return this.testFixturesDirPath;
  }

  /**
   * Sets path to test's data provider directory
   *
   * @param String testDataProvidersDirPath Path to test's data provider directory
   * @return Helper this Fluent interface
   */
  setTestDataProvidersDirPath(testDataProvidersDirPath)
  {
    try {
      if (!_.isString(testDataProvidersDirPath) || !fs.realpathSync(testDataProvidersDirPath)) {
        // lovely interface of fs package...
      }
    } catch (e) {
      throw new BaseError("Invalid test's data provider directory path provided");
    }

    this.testDataProvidersDirPath = testDataProvidersDirPath;
    return this;
  }

  /**
   * Returns path to test's data provider directory
   *
   * @return String testDataProvidersDirPath Path to test's data provider directory
   */
  getTestDataProvidersDirPath()
  {
    if (!_.isString(this.testDataProvidersDirPath)) {
      var testPath = this.getTestPath();

      // deleting ".js"
      var tempDataProviderDir = testPath.substr(0, testPath.length-3);

      // getting base by everything up to "/test/"
      var testDirPath = tempDataProviderDir.substr(0, tempDataProviderDir.indexOf("/test/"));

      // relPath is everything after "/test/"
      var relPath = tempDataProviderDir.substr(testDirPath.length + 6);

      // adding fixture and testing does it exists
      if (!fs.realpathSync(testDirPath + "/test/dataProviders/" + relPath)) {
        return null;
      }

      this.testDataProvidersDirPath = testDirPath + "/test/dataProviders/" + relPath + "/";
    }

    return this.testDataProvidersDirPath;
  }

  /**
   * Executes fixture script
   *
   * @param String   fileName Filename of JavaScript script (without .js)
   * @param Function callback Done callback
   * @return Helper this Fluent interface
   */
  executeFixtureScript(fileName, callback)
  {
    logger.info("Executing " + this.getTestFixturesDirPath() + fileName + ".js file");

    var promise = require(this.getTestFixturesDirPath() + fileName);

    // running execute in sync-way
    promise().then(function () {
      callback();
    });
  }

  /**
   * Returns required data provider's data
   *
   * @param  String fileName Filename of data provider (without .js)
   * @return Helper this Fluent interface
   */
  getDataProvider(fileName)
  {
    return require(this.getTestDataProvidersDirPath() + fileName + ".js");
  }
}

module.exports = TestHelper;
