
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

function Roles() {};

Roles.prototype.all = function (id, callback) {

};

Roles.prototype.get = function (id, callback) {

};

Roles.prototype.put = function (id, update, callback) {

};

Roles.prototype.post = function (name, data, callback) {

};

Roles.prototype.del = function (id, callback) {

};

module.exports = Roles;
