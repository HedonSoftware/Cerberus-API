
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var logger      = require(appRootPath + "/lib/logger/logger");

exports.get = function (req, res) {

};

exports.getById = function (req, res) {

};

exports.create = function (req, res) {

};

exports.update = function (req, res) {

};

exports.replace = function (req, res) {

};

exports.delete = function (req, res) {

};
