
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Acl service related files
 */

// export all routes
module.exports.Route = {
  Roles: require("./lib/route/roles")//,
  // Resources: require("./lib/routes/resources"),
  // Rules: require("./lib/routes/rules")
};

// export all services
// module.exports.Services = {
//   Roles: require("./lib/service/roles"),
//   Resources: require("./lib/service/resources"),
//   Rules: require("./lib/service/rules")
// };

// export all mappers
// module.exports.Mappers = {
//  Roles: require("./lib/mappers/roles"),
//  Resources: require("./lib/mappers/resources"),
//  Rules: require("./lib/mappers/rules")
// };

// export all entities
module.exports.entity = {
 Role: require("./lib/entity/aclRoleEntity"),
 Resource: require("./lib/entity/aclResourceEntity"),
 Rule: require("./lib/entity/aclRuleEntity")
};
