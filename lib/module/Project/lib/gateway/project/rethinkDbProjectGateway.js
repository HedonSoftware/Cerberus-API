
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath      = require("app-root-path");
var RethinkDbGateway = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var ProjectEntity    = require(appRootPath + "/lib/module/Project/lib/entity/projectEntity");
var GatewayError     = require(appRootPath + "/lib/gateway/gatewayError");

class RethinkDbProjectGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper"s gateway(i.e. dbConnection)
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "Project",
      "alias": "p"
    };

    /**
     * Object describing project"s relation
     *
     * @type Object
     */
    this.relations = {

    };
  }

  /**
   * Method fetches all records matching passed query builder"s criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(request)
  {
    var entities = [new ProjectEntity()];
    return this.query(request, entities);
  }

  /**
   * Method inserts new entity to table
   *
   * @param ProjectEntity project ProjectEntity entity entity
   * @return Promise project Promise of newly created project entity
   */
  insert(project)
  {
    if (!(project instanceof ProjectEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of ProjectEntity expected",
        project,
        502
      );
    }

    return super.insert(project)
      .then(function (projectData) {
        return new ProjectEntity(projectData);
      });
  }

  /**
   * Method updates entity in table
   *
   * @param ProjectEntity project ProjectEntity entity
   * @return Promise project Promise of newly created project entity
   */
  update(project)
  {
    if (!(project instanceof ProjectEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of ProjectEntity expected",
        project,
        504
      );
    }

    return super.update(project)
      .then(function (projectData) {
        return new ProjectEntity(projectData);
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param ProjectEntity project ProjectEntity entity
   * @return Promise project Promise of newly created project entity
   */
  replace(project)
  {
    if (!(project instanceof ProjectEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of ProjectEntity expected"
      );
    }

    return super.replace(project)
      .then(function (projectData) {
        return new ProjectEntity(projectData);
      });
  }
}

module.exports = RethinkDbProjectGateway;
