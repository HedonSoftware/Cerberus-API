
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Project service related files
 */

"use strict";

// export all routes
module.exports.Routes = {
  ProjectRoutes: require("./lib/route/projectRoutes")
};

// export all services
module.exports.Services = {
  ProjectsService: require("./lib/service/projectsService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbProjectGateway: require("./lib/gateway/project/rethinkDbProjectGateway")
};

// export all entities
module.exports.Entities = {
  ProjectEntity: require("./lib/entity/projectEntity")
};
