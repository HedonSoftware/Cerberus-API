
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath       = require("app-root-path");
var RethinkDbGateway  = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var TicketEntity      = require(appRootPath + "/lib/module/Ticket/lib/entity/ticketEntity");
var UserEntity        = require(appRootPath + "/lib/module/User/lib/entity/userEntity");
var GatewayError      = require(appRootPath + "/lib/gateway/gatewayError");
var _                 = require(appRootPath + "/lib/utility/underscore");

class RethinkDbTicketGateway extends RethinkDbGateway
{
  constructor(dbConnection)
  {
    super(dbConnection);

    this.table = {
      "name": "Ticket",
      "alias": "t"
    };

    this.relations = {
      "creator": {
        "table": "User",
        "localColumn": "creatorId",
        "targetColumn": "id",
        "defaultAlias": "uc",
        "condition": "`t`.`creatorId` = `uc`.`id`"
      },
      "assignee": {
        "table": "User",
        "localColumn": "assigneeId",
        "targetColumn": "id",
        "defaultAlias": "ua",
        "condition": "`t`.`assigneeId` = `ua`.`id`"
      }
    };
  }

  /**
   * Method fetches all records matching passed query builder"s criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(request)
  {
    var entities = [new TicketEntity()];
    var joins = request.getJoins();
    if (_.contains(joins, "creator") || _.contains(joins, "assignee")) {
      entities.push(new UserEntity());
    }
    return this.query(request, entities);
  }

  /**
   * Method inserts new entity to table
   *
   * @param TicketEntity ticket TicketEntity entity entity
   * @return Promise ticket Promise of newly created ticket entity
   */
  insert(ticket)
  {
    if (!(ticket instanceof TicketEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of TicketEntity expected",
        ticket,
        502
      );
    }

    return super.insert(ticket)
      .then(function (ticketData) {
        return new TicketEntity(ticketData);
      });
  }

  /**
   * Method updates entity in table
   *
   * @param TicketEntity ticket TicketEntity entity
   * @return Promise ticket Promise of newly created ticket entity
   */
  update(ticket)
  {
    if (!(ticket instanceof TicketEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of TicketEntity expected",
        ticket,
        504
      );
    }

    return super.update(ticket)
      .then(function (ticketData) {
        return new TicketEntity(ticketData);
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param TicketEntity ticket TicketEntity entity
   * @return Promise ticket Promise of newly created ticket entity
   */
  replace(ticket)
  {
    if (!(ticket instanceof TicketEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of TicketEntity expected"
      );
    }

    return super.replace(ticket)
      .then(function (ticketData) {
        return new TicketEntity(ticketData);
      });
  }
}

module.exports = RethinkDbTicketGateway;
