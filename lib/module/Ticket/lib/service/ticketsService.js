
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var TicketEntity = require("../entity/ticketEntity");
var Request      = require(appRootPath + "/lib/request/request");
var _            = require(appRootPath + "/lib/utility/underscore");
var ServiceError = require(appRootPath + "/lib/service/serviceError");
var BaseService  = require(appRootPath + "/lib/service/baseService");

class TicketsService extends BaseService
{
  constructor(ticketGateway)
  {
    super();

    if (!ticketGateway) {
      var TicketGateway = require("../gateway/ticket/rethinkDbTicketGateway");
      ticketGateway = new TicketGateway();
    }

    this.ticketGateway = ticketGateway;
  }

  /**
   * Method gets all tickets matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  fetchAll(query)
  {
    query = query || new Request();
    return this.ticketGateway.fetchAll(query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single ticket by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchById(id)
  {
    var query = new Request();
    query.setConditions({"id": id});

    return this.fetchAll(query)
      .then(function (tickets) {
        if (!_.isArray(tickets) || _.isEmpty(tickets)) {
          return null;
        }
        return tickets.shift();
      });
  }

  create(data)
  {
    var ticket = new TicketEntity(data);

    if (!_.isEmpty(ticket.comments)) {
      throw new ServiceError(
        "Unable to insert ticket with comments. " +
        "Use /tickets/:ticketId/comments to insert comment",
        data,
        409
      );
    }

    if (!_.isEmpty(ticket.workRecords)) {
      throw new ServiceError(
        "Unable to insert ticket with work records. " +
        "Use /tickets/:ticketId/work-records to insert work records",
        data,
        409
      );
    }

    return this.ticketGateway.insert(ticket);
  }

  update(data)
  {
    var ticket = new TicketEntity(data);

    if (!_.isEmpty(ticket.comments)) {
      throw new ServiceError(
        "Unable to update ticket with comments. " +
        "Use /tickets/:ticketId/comments to update comment",
        data,
        409
      );
    }

    if (!_.isEmpty(ticket.workRecords)) {
      throw new ServiceError(
        "Unable to update ticket with work records. " +
        "Use /tickets/:ticketId/work-records to update work records",
        data,
        409
      );
    }

    return this.ticketGateway.update(ticket);
  }

  replace(data)
  {
    var ticket = new TicketEntity(data);
    return this.ticketGateway.replace(ticket);
  }

  delete(id)
  {
    return this.ticketGateway.delete(id);
  }
}

module.exports = TicketsService;
