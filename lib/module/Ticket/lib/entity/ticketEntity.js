
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var uuid        = require("node-uuid");
var BaseEntity  = require(appRootPath + "/lib/entity/baseEntity");

class TicketEntity extends BaseEntity
{
  constructor(data)
  {
    super();

    this.setFields({
      id: {type: "string", optional: false, def: uuid.v4()},
      name: {type: "string"},
      description: {type: "string"},
      storyPoints: {type: "number"},
      creatorId: {type: "string"},
      assigneeId: {type: "string"},
      comments: {type: "object", optional: false, def: {}},
      workRecords: {type: "object", optional: false, def: {}},
      tags: {type: ["array", null], optional: false, def: null},
      status: {type: "string", optional: false, def: "active"},
      createdAt: {type: "date", optional: false, def: new Date()},
      updatedAt: {type: ["date", "null"], optional: false, def: null}
    });

    if (data) {
      this.inflate(data);
    }
  }

  getId()
  {
    return this.id;
  }

  setId(id)
  {
    this.id = id;
    return this;
  }
}

module.exports = TicketEntity;
