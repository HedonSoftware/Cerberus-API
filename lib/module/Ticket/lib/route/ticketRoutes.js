
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath    = require("app-root-path");
var TicketsService = require("../service/ticketsService");
var BaseService    = require(appRootPath + "/lib/service/baseService");
var BaseRoute      = require(appRootPath + "/lib/route/baseRoute");

class TicketRoutes extends BaseRoute
{
  /**
   * Constructor allows to assign base boards service
   *
   * @param BaseService service Service
   */
  constructor(service)
  {
    if (!(service instanceof BaseService)) {
      service = new TicketsService();
    }

    super(service);
  }
}

module.exports = TicketRoutes;
