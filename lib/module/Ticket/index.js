
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Ticket service related files
 */

// export all routes
module.exports.Routes = {
  TicketRoutes: require("./lib/route/ticketRoutes")
};

// export all services
module.exports.Services = {
  TicketsServices: require("./lib/service/ticketsService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbTicketGateway: require("./lib/gateway/ticket/rethinkDbTicketGateway")
};

// export all entities
module.exports.Entities = {
  TicketEntity: require("./lib/entity/ticketEntity")
};
