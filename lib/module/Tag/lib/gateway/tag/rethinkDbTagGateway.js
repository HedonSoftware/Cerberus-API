
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath      = require("app-root-path");
var RethinkDbGateway = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var TagEntity        = require(appRootPath + "/lib/module/Tag/lib/entity/tagEntity");
var GatewayError     = require(appRootPath + "/lib/gateway/gatewayError");

class RethinkDbTagGateway extends RethinkDbGateway
{
  constructor(dbConnection)
  {
    super(dbConnection);

    this.table = {
      "name": "Tag",
      "alias": "tag"
    };

    this.relations = {

    };
  }

  /**
   * Method fetches all records matching passed query builder"s criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(request)
  {
    var entities = [new TagEntity()];
    return this.query(request, entities);
  }

  /**
   * Method inserts new entity to table
   *
   * @param TagEntity tag TagEntity entity entity
   * @return Promise tag Promise of newly created tag entity
   */
  insert(tag)
  {
    if (!(tag instanceof TagEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of TagEntity expected",
        tag,
        502
      );
    }

    return super.insert(tag)
      .then(function (tagData) {
        return new TagEntity(tagData);
      });
  }

  /**
   * Method updates entity in table
   *
   * @param TagEntity tag TagEntity entity
   * @return Promise tag Promise of newly created tag entity
   */
  update(tag)
  {
    if (!(tag instanceof TagEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of TagEntity expected",
        tag,
        504
      );
    }

    return super.update(tag)
      .then(function (tagData) {
        return new TagEntity(tagData);
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param TagEntity tag TagEntity entity
   * @return Promise tag Promise of newly created tag entity
   */
  replace(tag)
  {
    if (!(tag instanceof TagEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of TagEntity expected"
      );
    }

    return super.replace(tag)
      .then(function (tagData) {
        return new TagEntity(tagData);
      });
  }
}

module.exports = RethinkDbTagGateway;
