
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all tag service related files
 */

// export all routes
module.exports.Routes = {
  TagRoutes: require("./lib/route/tagRoutes")
};

// export all services
module.exports.Services = {
  TagsServices: require("./lib/service/tagsService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbTagGateway: require("./lib/gateway/tag/rethinkDbTagGateway")
};

// export all entities
module.exports.Entities = {
  TagEntity: require("./lib/entity/tagEntity")
};
