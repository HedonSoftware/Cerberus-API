
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Board service related files
 */

// export all routes
module.exports.Routes = {
  BoardRoutes: require("./lib/route/boardRoutes")
};

// export all services
module.exports.Services = {
  BoardsService: require("./lib/service/boardsService")
};

// export all entities
module.exports.entities = {
  Board: require("./lib/entity/boardEntity")
};
