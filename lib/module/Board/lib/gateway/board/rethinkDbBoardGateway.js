
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath             = require("app-root-path");
var _                       = require("underscore");
var RethinkDbGateway        = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var BoardEntity             = require(appRootPath + "/lib/module/Board/lib/entity/boardEntity");
var ProjectEntity           = require(appRootPath + "/lib/module/Project/lib/entity/projectEntity");
var RethinkDbProjectGateway = require(appRootPath + "/lib/module/Project/lib/gateway/project/rethinkDbProjectGateway");
var GatewayError            = require(appRootPath + "/lib/gateway/gatewayError");
var Request                 = require(appRootPath + "/lib/request/request");

class RethinkDbBoardGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper"s gateway(i.e. dbConnection)
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "Project",
      "alias": "b"
    };

    this.rethinkDbProjectGateway = new RethinkDbProjectGateway(gateway);

    /**
     * Object describing board's relation
     *
     * @type Object
     */
    this.relations = {

    };
  }

  /**
   * Method fetches all records matching passed query builder"s criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(projectId, request)
  {
    var entities = [new BoardEntity()];
    var query = this.getRethinkDbSubQuery(projectId);
    return this.query(request, entities, query);
  }

  /**
   * Method inserts new board entity to table
   *
   * @param string projectId Project id that board will belong to
   * @param BoardEntity board BoardEntity entity entity
   * @return Promise board Promise of newly created board entity
   */
  insert(projectId, board)
  {
    if (!(board instanceof BoardEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of BoardEntity expected",
        board,
        502
      );
    }

    if (board.get("tags") === null) {
      board.set("tags", []);
    }

    board.set("createdAt", new Date());
    board.set("updatedAt", null);

    let projectEntity = new ProjectEntity();
    projectEntity.setId(projectId);
    projectEntity.set(
      "boards",
      {
        [board.getId()]: board.export()
      }
    );

    return this.rethinkDbProjectGateway.update(projectEntity)
      .then(function (projectData) {
        return new BoardEntity(
          projectData.boards[board.getId()]
        );
      });
  }

  /**
   * Method updates entity in table
   *
   * @param string projectId Project id that board will belong to
   * @param BoardEntity board BoardEntity entity
   * @return Promise board Promise of newly created board entity
   */
  update(projectId, board)
  {
    if (!(board instanceof BoardEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of BoardEntity expected",
        board,
        504
      );
    }

    if (board.get("tags") === null) {
      board.set("tags", undefined);
    }

    board.set("createdAt", undefined);
    board.set("updatedAt", new Date());

    let projectEntity = new ProjectEntity();
    projectEntity.setId(projectId);
    projectEntity.set(
      "boards",
      {
        [board.getId()]: board.export()
      }
    );

    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[board.getId()])
        ) {
          throw new GatewayError(
            "Specified board doesn\'t exist",
            board.getId(),
            409
          );
        }

        return me.rethinkDbProjectGateway.update(projectEntity)
          .then(function (projectData) {
            return new BoardEntity(
              projectData.boards[board.getId()]
            );
          });
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param string projectId Project id that board will belong to
   * @param BoardEntity board BoardEntity entity
   * @return Promise board Promise of newly created board entity
   */
  replace(projectId, board)
  {
    if (!(board instanceof BoardEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of BoardEntity expected"
      );
    }

    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[board.getId()])
        ) {
          throw new GatewayError(
            "Specified board doesn\'t exist"
          );
        }

        projectData.boards[board.getId()] = board.export();
        let projectEntity = new ProjectEntity(projectData);

        return me.rethinkDbProjectGateway.replace(projectEntity)
          .then(function (projectData) {
            return new BoardEntity(
              projectData.boards[board.getId()]
            );
          });
      });
  }

  /**
   * Method deletes board from Project row
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @return Promise promise Promise of result of deleting
   * entity
   */
  delete(projectId, boardId)
  {
    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[boardId])
        ) {
          throw new GatewayError(
            "Specified board doesn\'t exist",
            {
              projectId: projectId,
              boardId: boardId
            },
            404
          );
        }

        return me.dataProvider.table(me.rethinkDbProjectGateway.table.name)
          .get(projectId)
          .replace(
            me.dataProvider.table(me.table.name)
              .get(projectId)
              .without({
                "boards" : boardId
              }),
              {nonAtomic: true}
          )
      });
  }

  /**
   * Method returns RethinkDb"s subquery to start from "board" level
   *
   * @param string projectId ProjectId
   * @return RethinkDbQuery query RethinkDb query object
   */
  getRethinkDbSubQuery(projectId)
  {
    var query = this.dataProvider.table(this.table.name);
    return query.get(projectId)("boards")
      .coerceTo("array")
      .map({
        left: {},
        right: this.dataProvider.row(1)
      }).zip();
  }
}

module.exports = RethinkDbBoardGateway;
