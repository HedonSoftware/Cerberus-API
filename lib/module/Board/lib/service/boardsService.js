
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var BoardEntity  = require("../entity/boardEntity");
var Request      = require(appRootPath + "/lib/request/request");
var _            = require(appRootPath + "/lib/utility/underscore");
var ServiceError = require(appRootPath + "/lib/service/serviceError");
var BaseService  = require(appRootPath + "/lib/service/baseService");

class BoardsService extends BaseService
{
  constructor(boardGateway)
  {
    super();

    if (!boardGateway) {
      var BoardGateway = require("../gateway/board/rethinkDbBoardGateway");
      boardGateway = new BoardGateway();
    }

    this.boardGateway = boardGateway;
  }

  /**
   * Method gets all boards matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  fetchAll(projectId, query)
  {
    query = query || new Request();
    return this.boardGateway.fetchAll(projectId, query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single board by passed id
   * @param  projectId Project id
   * @return query     Query object
   */
  fetchById(projectId, boardId)
  {
    var query = new Request();
    query.setConditions({id: boardId});

    return this.fetchAll(projectId, query)
      .then(function (boards) {
        if (!_.isArray(boards) || _.isEmpty(boards)) {
          return null;
        }
        return boards.shift();
      });
  }

  create(projectId, data)
  {
    var board = new BoardEntity(data);

    if (!_.isEmpty(board.lanes)) {
      throw new ServiceError(
        "Unable to insert board with lanes. " +
        "Use /projects/:projectId/board/:boardId:/lanes to insert lane",
        board,
        409
      );
    }

    return this.boardGateway.insert(projectId, board);
  }

  update(projectId, data)
  {
    var board = new BoardEntity(data);

    if (!_.isEmpty(board.lanes)) {
      throw new ServiceError(
        "Unable to update board with lanes. " +
        "Use /projects/:projectId/board/:boardId:/lanes to update lane",
        board,
        409
      );
    }

    if (_.isEmpty(board.get("tags"))) {
      board.set("tags", undefined);
    }

    return this.boardGateway.update(projectId, board);
  }

  replace(projectId, data)
  {
    var board = new BoardEntity(data);
    return this.boardGateway.replace(projectId, board);
  }

  delete(projectId, boardId)
  {
    return this.boardGateway.delete(projectId, boardId);
  }
}

module.exports = BoardsService;
