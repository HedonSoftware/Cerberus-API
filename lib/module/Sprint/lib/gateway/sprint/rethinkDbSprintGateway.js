
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath             = require("app-root-path");
var RethinkDbGateway        = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var ProjectEntity           = require(appRootPath + "/lib/module/Project/lib/entity/projectEntity");
var SprintEntity            = require(appRootPath + "/lib/module/Sprint/lib/entity/sprintEntity");
var RethinkDbProjectGateway = require(appRootPath + "/lib/module/Project/lib/gateway/project/rethinkDbProjectGateway");
var GatewayError            = require(appRootPath + "/lib/gateway/gatewayError");
var Request                 = require(appRootPath + "/lib/request/request");
var _                       = require(appRootPath + "/lib/utility/underscore");

class RethinkDbSprintGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper's gateway(i.e. dbConnection)
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "Project",
      "alias": "s"
    };

    this.rethinkDbProjectGateway = new RethinkDbProjectGateway(gateway);

    /**
     * Object describing sprint's relation
     *
     * @type Object
     */
    this.relations = {
      tags: {

      }
    };
  }

  /**
   * Method fetches all records matching passed query builder's criteria
   *
   * @param string  projectId Project id
   * @param string  boardId   Board id
   * @param Request request   Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(projectId, boardId, request)
  {
    var entities = [new SprintEntity()];
    var query = this.getSubQuery(projectId, boardId);
    return this.query(request, entities, query);
  }

  /**
   * Method inserts new entity to table
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @param SprintEntity sprint SprintEntity entity entity
   * @return Promise sprint Promise of newly created sprint entity
   */
  insert(projectId, boardId, sprint)
  {
    if (!(sprint instanceof SprintEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of SprintEntity expected",
        sprint,
        502
      );
    }

    sprint.createdAt = this.getCurrentUtcDate();
    sprint.updatedAt = null;

    let projectEntity = new ProjectEntity();
    projectEntity.setId(projectId);
    projectEntity.set("boards", {
      [boardId]: {
        sprints: {
          [sprint.getId()]: sprint.export()
        }
      }
    });

    return this.rethinkDbProjectGateway.update(projectEntity)
      .then(function (projectData) {
        return new SprintEntity(
          projectData.boards[boardId].sprints[sprint.getId()]
        );
      });
  }

  /**
   * Method updates entity in table
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @param SprintEntity sprint SprintEntity entity
   * @return Promise sprint Promise of newly created sprint entity
   */
  update(projectId, boardId, sprint)
  {
    if (!(sprint instanceof SprintEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of SprintEntity expected",
        sprint,
        504
      );
    }

    sprint.createdAt = undefined;
    sprint.updatedAt = this.getCurrentUtcDate();

    let projectEntity = new ProjectEntity();
    projectEntity.setId(projectId);
    projectEntity.set(
      "boards",
      {
        [boardId]: {
          sprints: {
            [sprint.getId()] : sprint.export()
          }
        }
      }
    );

    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[boardId])
        ) {
          throw new GatewayError(
            "Specified board doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              sprintId: sprint.getId()
            },
            409
          );
        }

        if (_.isEmpty(projectData.boards[boardId].sprints) ||
          !_.isObject(projectData.boards[boardId].sprints[sprint.getId()])
        ) {
          throw new GatewayError(
            "Specified sprint doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              sprintId: sprint.getId()
            },
            409
          );
        }

        return me.rethinkDbProjectGateway.update(projectEntity)
          .then(function (projectData) {
            return new SprintEntity(
              projectData.boards[boardId].sprints[sprint.getId()]
            );
          });
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @param SprintEntity sprint SprintEntity entity
   * @return Promise sprint Promise of newly created sprint entity
   */
  replace(projectId, boardId, sprint)
  {
    if (!(sprint instanceof SprintEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of SprintEntity expected"
      );
    }

    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[boardId])
        ) {
          throw new GatewayError(
            "Specified board doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              sprintId: sprint.getId()
            },
            404
          );
        }

        if (_.isEmpty(projectData.boards[boardId].sprints) ||
          !_.isObject(projectData.boards[boardId].sprints[sprint.getId()])
        ) {
          throw new GatewayError(
            "Specified sprint doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              sprintId: sprint.getId()
            },
            404
          );
        }

        projectData.boards[boardId].sprints[sprint.getId()] = sprint.export();
        let projectEntity = new ProjectEntity(projectData);

        return me.rethinkDbProjectGateway.replace(projectEntity)
          .then(function (projectData) {
            return new SprintEntity(
              projectData.boards[boardId].sprints[sprint.getId()]
            );
          });
      });
  }

  /**
   * Method deletes board from Project row
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @param string sprintId  Sprint id
   * @return Promise promise Promise of result of deleting
   * entity
   */
  delete(projectId, boardId, sprintId)
  {
    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[boardId])
        ) {
          throw new GatewayError(
            "Specified board doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              sprintId: sprintId
            },
            404
          );
        }

        if (_.isEmpty(projectData.boards[boardId].sprints) ||
          !_.isObject(projectData.boards[boardId].sprints[sprintId])
        ) {
          throw new GatewayError(
            "Specified sprint doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              sprintId: sprintId
            },
            404
          );
        }

        return me.dataProvider.table(me.rethinkDbProjectGateway.table.name)
          .get(projectId)
          .replace(
            me.dataProvider.table(me.table.name)
              .get(projectId)
              .without({
                "boards" : {
                  [boardId] : {
                    sprints: sprintId
                  }
                }
              }),
              {nonAtomic: true}
          );
      });
  }

  /**
   * Method returns RethinkDb's subquery to start from "sprint" level
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @return RethinkDbQuery query RethinkDb query object
   */
  getSubQuery(projectId, boardId)
  {
    var query = this.dataProvider.table(this.table.name);
    return query.get(projectId)("boards")(boardId)("sprints")
      .coerceTo("array")
      .map({
        left: {},
        right: this.dataProvider.row(1)
      }).zip();
  }
}

module.exports = RethinkDbSprintGateway;
