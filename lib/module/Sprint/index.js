
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

// export all routes
module.exports.Routes = {
  SprintRoutes: require("./lib/route/sprintRoutes")
};

// export all services
module.exports.Services = {
  SprintsService: require("./lib/service/sprintsService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbSprintGateway: require("./lib/gateway/sprint/rethinkDbSprintGateway")
};

// export all entities
module.exports.Entities = {
  SprintEntity: require("./lib/entity/sprintEntity")
};
