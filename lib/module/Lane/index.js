
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Lane service related files
 */

// export all routes
module.exports.Routes = {
  LaneRoutes: require("./lib/route/laneRoutes")
};

// export all services
module.exports.Services = {
  LanesService: require("./lib/service/lanesService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbLaneGateway: require("./lib/gateway/lane/rethinkDbLaneGateway")
};

// export all entities
module.exports.Entities = {
  LaneEntity: require("./lib/entity/laneEntity")
};
