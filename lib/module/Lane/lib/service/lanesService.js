
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var LaneEntity  = require("../entity/laneEntity");
var Request     = require(appRootPath + "/lib/request/request");
var _           = require(appRootPath + "/lib/utility/underscore");
var BaseService = require(appRootPath + "/lib/service/baseService");

class LanesService extends BaseService
{
  constructor(laneGateway)
  {
    super();

    if (!laneGateway) {
      var LaneGateway = require("../gateway/lane/rethinkDbLaneGateway");
      laneGateway = new LaneGateway();
    }

    this.laneGateway = laneGateway;
  }

  /**
   * Method gets all lanes matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  fetchAll(projectId, boardId, query)
  {
    query = query || new Request();
    return this.laneGateway.fetchAll(projectId, boardId, query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single lane by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchById(projectId, boardId, laneId)
  {
    var query = new Request();
    query.setConditions({id: laneId});

    return this.fetchAll(projectId, boardId, query)
      .then(function (lanes) {
        if (!_.isArray(lanes) || _.isEmpty(lanes)) {
          return null;
        }
        return lanes.shift();
      });
  }

  create(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.insert(projectId, boardId, lane);
  }

  update(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.update(projectId, boardId, lane);
  }

  replace(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.replace(projectId, boardId, lane);
  }

  delete(projectId, boardId, laneId)
  {
    return this.laneGateway.delete(projectId, boardId, laneId);
  }
}

module.exports = LanesService;
