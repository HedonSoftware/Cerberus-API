
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath             = require("app-root-path");
var RethinkDbGateway        = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var ProjectEntity           = require(appRootPath + "/lib/module/Project/lib/entity/projectEntity");
var LaneEntity              = require(appRootPath + "/lib/module/Lane/lib/entity/laneEntity");
var TagEntity               = require(appRootPath + "/lib/module/Tag/lib/entity/tagEntity");
var RethinkDbProjectGateway = require(appRootPath + "/lib/module/Project/lib/gateway/project/rethinkDbProjectGateway");
var GatewayError            = require(appRootPath + "/lib/gateway/gatewayError");
var Request                 = require(appRootPath + "/lib/request/request");
var _                       = require(appRootPath + "/lib/utility/underscore");

class RethinkDbLaneGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper's gateway(i.e. dbConnection)
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "Project",
      "alias": "l"
    };

    this.rethinkDbProjectGateway = new RethinkDbProjectGateway(gateway);

    /**
     * Object describing lane's relation
     *
     * @type Object
     */
    this.relations = {
      tags: {

      }
    };
  }

  /**
   * Method fetches all records matching passed query builder's criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(projectId, boardId, request)
  {
    var entities = [new LaneEntity()];
    var query = this.getSubQuery(projectId, boardId);
    return this.query(request, entities, query);
  }

  /**
   * Method inserts new entity to table
   *
   * @param LaneEntity lane LaneEntity entity entity
   * @return Promise lane Promise of newly created lane entity
   */
  insert(projectId, boardId, lane)
  {
    if (!(lane instanceof LaneEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of LaneEntity expected",
        lane,
        502
      );
    }

    if (lane.get("tags") === null) {
      lane.set("tags", []);
    }

    lane.set("createdAt", new Date());
    lane.set("updatedAt", null);

    let projectEntity = new ProjectEntity();
    projectEntity.setId(projectId);
    projectEntity.set("boards", {
      [boardId]: {
        lanes: {
          [lane.getId()]: lane.export()
        }
      }
    });

    return this.rethinkDbProjectGateway.update(projectEntity)
      .then(function (projectData) {
        return new LaneEntity(
          projectData.boards[boardId].lanes[lane.getId()]
        );
      });
  }

  /**
   * Method updates entity in table
   *
   * @param LaneEntity lane LaneEntity entity
   * @return Promise lane Promise of newly created lane entity
   */
  update(projectId, boardId, lane)
  {
    if (!(lane instanceof LaneEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of LaneEntity expected",
        lane,
        504
      );
    }

    if (lane.get("tags") === null) {
      lane.set("tags", undefined);
    }

    lane.set("createdAt", undefined);
    lane.set("updatedAt", new Date());

    let projectEntity = new ProjectEntity();
    projectEntity.setId(projectId);
    projectEntity.set(
      "boards",
      {
        [boardId]: {
          lanes: {
            [lane.getId()] : lane.export()
          }
        }
      }
    );

    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[boardId])
        ) {
          throw new GatewayError(
            "Specified board doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              laneId: lane.getId()
            },
            409
          );
        }

        if (_.isEmpty(projectData.boards[boardId].lanes) ||
          !_.isObject(projectData.boards[boardId].lanes[lane.getId()])
        ) {
          throw new GatewayError(
            "Specified lane doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              laneId: lane.getId()
            },
            409
          );
        }

        return me.rethinkDbProjectGateway.update(projectEntity)
          .then(function (projectData) {
            return new LaneEntity(
              projectData.boards[boardId].lanes[lane.getId()]
            );
          });
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param LaneEntity lane LaneEntity entity
   * @return Promise lane Promise of newly created lane entity
   */
  replace(projectId, boardId, lane)
  {
    if (!(lane instanceof LaneEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of LaneEntity expected"
      );
    }

    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[board.getId()])
        ) {
          throw new GatewayError(
            "Specified board doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              laneId: laneId
            },
            404
          );
        }

        if (_.isEmpty(projectData.boards[boardId].lanes) ||
          !_.isObject(projectData.boards[boardId].lanes[laneId])
        ) {
          throw new GatewayError(
            "Specified lane doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              laneId: laneId
            },
            404
          );
        }

        projectData.boards[boardId].lanes[lane.getId()] = lane.export();
        let projectEntity = new ProjectEntity(projectData);

        return me.rethinkDbProjectGateway.replace(projectEntity)
          .then(function (projectData) {
            return new LaneEntity(
              projectData.boards[boardId].lanes[lane.getId()]
            );
          });
      });
  }

  /**
   * Method deletes board from Project row
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @return Promise promise Promise of result of deleting
   * entity
   */
  delete(projectId, boardId, laneId)
  {
    let request = new Request();
    request.setConditions({id: projectId});

    let me = this;
    return this.rethinkDbProjectGateway.fetchOne(request)
      .then(function(projectData) {

        if (!projectData || _.isEmpty(projectData.boards) ||
          !_.isObject(projectData.boards[boardId])
        ) {
          throw new GatewayError(
            "Specified board doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              laneId: laneId
            },
            404
          );
        }

        if (_.isEmpty(projectData.boards[boardId].lanes) ||
          !_.isObject(projectData.boards[boardId].lanes[laneId])
        ) {
          throw new GatewayError(
            "Specified lane doesn't exist",
            {
              projectId: projectId,
              boardId: boardId,
              laneId: laneId
            },
            404
          );
        }

        return me.dataProvider.table(me.rethinkDbProjectGateway.table.name)
          .get(projectId)
          .replace(
            me.dataProvider.table(me.table.name)
              .get(projectId)
              .without({
                "boards" : {
                  [boardId] : {
                    lanes: laneId
                  }
                }
              }),
              {nonAtomic: true}
          )
      });
  }

  /**
   * Method returns RethinkDb's subquery to start from "lane" level
   *
   * @param string projectId Project id
   * @param string boardId   Board id
   * @return RethinkDbQuery query RethinkDb query object
   */
  getSubQuery(projectId, boardId)
  {
    var query = this.dataProvider.table(this.table.name);
    return query.get(projectId)("boards")(boardId)("lanes")
      .coerceTo("array")
      .map({
        left: {},
        right: this.dataProvider.row(1)
      }).zip();
  }
}

module.exports = RethinkDbLaneGateway;
