
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var uuid        = require("node-uuid");
var BaseEntity  = require(appRootPath + "/lib/entity/baseEntity");

class LaneEntity extends BaseEntity
{
  constructor(data)
  {
    super();

    this.id = uuid.v4();
    this.name = undefined;
    this.description = undefined;
    this.sequenceNumber = undefined;
    this.status = undefined;
    this.tags = [];

    this.setFields({
      id: {type: "string", optional: false, def: uuid.v4()},
      name: {type: "string"},
      description: {type: "string"},
      sequenceNumber: {type: "number"},
      tags: {type: ["array", null], optional: false, def: null},
      status: {type: "string", optional: false, def: "active"},
      createdAt: {type: "date", optional: false, def: new Date()},
      updatedAt: {type: ["date", "null"], optional: false, def: null}
    });

    if (data) {
      this.inflate(data);
    }
  }

  getId()
  {
    return this.id;
  }

  setId(id)
  {
    this.id = id;
  }
}

module.exports = LaneEntity;
