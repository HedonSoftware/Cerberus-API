
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Comment service related files
 */

"use strict";

// export all routes
module.exports.Routes = {
  CommentRoutes: require("./lib/route/commentRoutes")
};

// export all services
module.exports.Services = {
  CommentsService: require("./lib/service/commentsService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbCommentGateway: require("./lib/gateway/comment/rethinkDbCommentGateway")
};

// export all entities
module.exports.Entities = {
  CommentEntity: require("./lib/entity/commentEntity")
};
