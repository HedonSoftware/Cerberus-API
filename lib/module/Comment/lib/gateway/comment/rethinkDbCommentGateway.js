
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath            = require("app-root-path");
var _                      = require(appRootPath + "/lib/utility/underscore");
var RethinkDbGateway       = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var CommentEntity          = require(appRootPath + "/lib/module/Comment/lib/entity/commentEntity");
var TicketEntity           = require(appRootPath + "/lib/module/Ticket/lib/entity/ticketEntity");
var UserEntity             = require(appRootPath + "/lib/module/User/lib/entity/userEntity");
var RethinkDbTicketGateway = require(appRootPath + "/lib/module/Ticket/lib/gateway/ticket/rethinkDbTicketGateway");
var GatewayError           = require(appRootPath + "/lib/gateway/gatewayError");
var Request                = require(appRootPath + "/lib/request/request");

class RethinkDbCommentGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper"s gateway(i.e. dbConnection)
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "Ticket",
      "alias": "c"
    };

    this.rethinkDbTicketGateway = new RethinkDbTicketGateway(gateway);

    /**
     * Object describing comment's relation
     *
     * @type Object
     */
    this.relations = {
      "user": {
        "table": "User",
        "localColumn": "userId",
        "targetColumn": "id",
        "defaultAlias": "u"
      }
    };
  }

  /**
   * Method fetches all records matching passed query builder's criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(ticketId, request)
  {
    var entities = [new CommentEntity()];

    var joins = request.getJoins();
    if (_.contains(joins, "user")) {
      entities.push(new UserEntity());
    }

    var query = this.getRethinkDbSubQuery(ticketId);
    return this.query(request, entities, query);
  }

  /**
   * Method inserts new comment entity to table
   *
   * @param string ticketId Ticket id that comment will belong to
   * @param CommentEntity comment CommentEntity entity entity
   * @return Promise comment Promise of newly created comment entity
   */
  insert(ticketId, comment)
  {
    if (!(comment instanceof CommentEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of CommentEntity expected",
        comment,
        502
      );
    }

    comment.createdAt = this.getCurrentUtcDate();
    comment.updatedAt = null;

    let ticketEntity = new TicketEntity();
    ticketEntity.setId(ticketId);
    ticketEntity.set(
      "comments",
      {
        [comment.getId()]: comment.export()
      }
    );

    return this.rethinkDbTicketGateway.update(ticketEntity)
      .then(function (ticketData) {
        return new CommentEntity(
          ticketData.comments[comment.getId()]
        );
      });
  }

  /**
   * Method updates entity in table
   *
   * @param string ticketId Ticket id that comment will belong to
   * @param CommentEntity comment CommentEntity entity
   * @return Promise comment Promise of newly created comment entity
   */
  update(ticketId, comment)
  {
    if (!(comment instanceof CommentEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of CommentEntity expected",
        comment,
        504
      );
    }

    comment.createdAt = undefined;
    comment.updatedAt = this.getCurrentUtcDate();

    let ticketEntity = new TicketEntity();
    ticketEntity.setId(ticketId);
    ticketEntity.set(
      "comments",
      {
        [comment.getId()]: comment.export()
      }
    );

    let request = new Request();
    request.setConditions({id: ticketId});

    let me = this;
    return this.rethinkDbTicketGateway.fetchOne(request)
      .then(function(ticketData) {

        if (!ticketData || _.isEmpty(ticketData.comments) ||
          !_.isObject(ticketData.comments[comment.getId()])
        ) {
          throw new GatewayError(
            "Specified comment doesn\'t exist",
            {
              id: comment.getId()
            },
            409
          );
        }

        return me.rethinkDbTicketGateway.update(ticketEntity)
          .then(function (ticketData) {
            return new CommentEntity(
              ticketData.comments[comment.getId()]
            );
          });
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param string ticketId Ticket id that comment will belong to
   * @param CommentEntity comment CommentEntity entity
   * @return Promise comment Promise of newly created comment entity
   */
  replace(ticketId, comment)
  {
    if (!(comment instanceof CommentEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of CommentEntity expected"
      );
    }

    let request = new Request();
    request.setConditions({id: ticketId});

    let me = this;
    return this.rethinkDbTicketGateway.fetchOne(request)
      .then(function(ticketData) {

        if (!ticketData || _.isEmpty(ticketData.comments) ||
          !_.isObject(ticketData.comments[comment.getId()])
        ) {
          throw new GatewayError(
            "Specified comment doesn\'t exist"
          );
        }

        ticketData.comments[comment.getId()] = comment.export();
        let ticketEntity = new TicketEntity(ticketData);

        return me.rethinkDbTicketGateway.replace(ticketEntity)
          .then(function (ticketData) {
            return new CommentEntity(
              ticketData.comments[comment.getId()]
            );
          });
      });
  }

  /**
   * Method deletes comment from Ticket row
   *
   * @param string ticketId Ticket id
   * @param string commentId   Comment id
   * @return Promise promise Promise of result of deleting
   * entity
   */
  delete(ticketId, commentId)
  {
    let request = new Request();
    request.setConditions({id: ticketId});

    let me = this;
    return this.rethinkDbTicketGateway.fetchOne(request)
      .then(function(ticketData) {

        if (!ticketData || _.isEmpty(ticketData.comments) ||
          !_.isObject(ticketData.comments[commentId])
        ) {
          throw new GatewayError(
            "Specified comment doesn\'t exist",
            {
              ticketId: ticketId,
              commentId: commentId
            },
            404
          );
        }

        return me.dataProvider.table(me.rethinkDbTicketGateway.table.name)
          .get(ticketId)
          .replace(
            me.dataProvider.table(me.table.name)
              .get(ticketId)
              .without({
                "comments" : commentId
              }),
              {nonAtomic: true}
          )
      });
  }

  /**
   * Method returns RethinkDb"s subquery to start from "comments" level
   *
   * @param string ticketId Ticket id
   * @return RethinkDbQuery query RethinkDb query object
   */
  getRethinkDbSubQuery(ticketId)
  {
    var query = this.dataProvider.table(this.table.name);
    return query.get(ticketId)("comments")
      .coerceTo("array")
      .map({
        left: {},
        right: this.dataProvider.row(1)
      }).zip();
  }
}

module.exports = RethinkDbCommentGateway;
