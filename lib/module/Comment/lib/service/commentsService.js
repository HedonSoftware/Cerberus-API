
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath   = require("app-root-path");
var CommentEntity = require("../entity/commentEntity");
var Request       = require(appRootPath + "/lib/request/request");
var _             = require(appRootPath + "/lib/utility/underscore");
var BaseService   = require(appRootPath + "/lib/service/baseService");

class CommentsService extends BaseService
{
  constructor(commentGateway)
  {
    super();

    if (!commentGateway) {
      var CommentGateway = require("../gateway/comment/rethinkDbCommentGateway");
      commentGateway = new CommentGateway();
    }

    this.commentGateway = commentGateway;
  }

  /**
   * Method fetches all comments matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  fetchAll(ticketId, query)
  {
    query = query || new Request();
    return this.commentGateway.fetchAll(ticketId, query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single comment by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchById(ticketId, commentId)
  {
    var query = new Request();
    query.setConditions({"id": commentId});

    return this.fetchAll(ticketId, query)
      .then(function (comments) {
        if (!_.isArray(comments) || _.isEmpty(comments)) {
          return null;
        }
        return comments.shift();
      });
  }

  create(ticketId, data)
  {
    var comment = new CommentEntity(data);
    return this.commentGateway.insert(ticketId, comment);
  }

  update(ticketId, data)
  {
    var comment = new CommentEntity(data);
    return this.commentGateway.update(ticketId, comment);
  }

  replace(ticketId, data)
  {
    var comment = new CommentEntity(data);
    return this.commentGateway.replace(ticketId, comment);
  }

  delete(ticketId, id)
  {
    return this.commentGateway.delete(ticketId, id);
  }
}

module.exports = CommentsService;
