
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath      = require("app-root-path");
var RethinkDbGateway = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var UserEntity       = require(appRootPath + "/lib/module/User/lib/entity/userEntity");
var AclRoleEntity    = require(appRootPath + "/lib/module/Acl/lib/entity/aclRoleEntity");
var _                = require(appRootPath + "/lib/utility/underscore");
var GatewayError     = require(appRootPath + "/lib/gateway/gatewayError");

class RethinkDbUserGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper"s gateway(i.e. dbConnection)
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "User",
      "alias": "u"
    };

    /**
     * Object describing user"s relation
     *
     * @type Object
     */
    this.relations = {
      "role": {
        "table": "AclRole",
        "localColumn": "roleId",
        "targetColumn": "id",
        "defaultAlias": "r",
        "condition": "`u`.`roleId` = `r`.`id`"
      },
    };
  }

  /**
   * Method fetches all records matching passed request"s criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise promise Promise of DB result
   */
  fetchAll(request)
  {
    var entities = [new UserEntity()];
    var joins = request.getJoins();
    if (_.contains(joins, "role")) {
      entities.push(new AclRoleEntity());
    }
    return this.query(request, entities);
  }

  /**
   * Method inserts new entity to table
   *
   * @param User user User entity
   * @return Promise user Promise of newly created user entity
   */
  insert(user)
  {
    if (!(user instanceof UserEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of User expected",
        user,
        500
      );
    }

    // var parent = super;
    return this.validateUsername(user)
      .then(super.insert.bind(this))
      .then(function (userData) {
        return new UserEntity(userData);
      });
  }

  /**
   * Method updates entity in table
   *
   * @param User user User entity
   * @return Promise user Promise of newly created user entity
   */
  update(user)
  {
    if (!(user instanceof UserEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of User expected",
        user,
        500
      );
    }

    // check username uniqueness before inserting
    return super.update(user)
      .then(function (userData) {
        return new UserEntity(userData);
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param User user User entity
   * @return Promise user Promise of newly created user entity
   */
  replace(user)
  {
    if (!(user instanceof UserEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of User expected"
      );
    }

    return super.replace(user)
      .then(function (userData) {
        return new UserEntity(userData);
      });
  }

  /**
   * Helper method validates username to be inserted
   *
   * @param User user User entity
   * @return Promise user Promise of exception if username exists
   * otherwise promise of passed object
   */
  validateUsername(user)
  {
    return this.dataProvider.table(this.table.name)
      .getAll(user.get("username"), {index: "username"})
      .then(function(users) {
        if (!_.isEmpty(users)) {
          throw new GatewayError(
            "Username already in use",
            {},
            409
          );
        }

        return user;
      });
  }
}

module.exports = RethinkDbUserGateway;
