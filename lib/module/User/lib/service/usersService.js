
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var UserEntity  = require("../entity/userEntity");
var Request     = require(appRootPath + "/lib/request/request");
var _           = require(appRootPath + "/lib/utility/underscore");
var BaseService = require(appRootPath + "/lib/service/baseService");

class UsersService extends BaseService
{
  constructor(userGateway)
  {
    super();

    if (!userGateway) {
      var UsersMapper = require("../gateway/user/rethinkDbUserGateway");
      userGateway = new UsersMapper();
    }

    this.userGateway = userGateway;
  }

  /**
   * Method gets all users matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  fetchAll(query)
  {
    query = query || new Request();
    return this.userGateway.fetchAll(query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single user by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchById(id)
  {
    var query = new Request();
    query.setConditions({"id": id});

    return this.fetchAll(query)
      .then(function (users) {
        if (!_.isArray(users) || _.isEmpty(users)) {
          return null;
        }
        return users.shift();
      });
  }

  create(data)
  {
    var user = new UserEntity(data);
    return this.userGateway.insert(user);
  }

  update(data)
  {
    var user = new UserEntity(data);
    return this.userGateway.update(user);
  }

  replace(data)
  {
    var user = new UserEntity(data);
    return this.userGateway.replace(user);
  }

  delete(id)
  {
    return this.userGateway.delete(id);
  }
}

module.exports = UsersService;
