
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath            = require("app-root-path");
var _                      = require(appRootPath + "/lib/utility/underscore");
var RethinkDbGateway       = require(appRootPath + "/lib/gateway/rethinkDbGateway");
var WorkRecordEntity       = require(appRootPath + "/lib/module/WorkRecord/lib/entity/workRecordEntity");
var TicketEntity           = require(appRootPath + "/lib/module/Ticket/lib/entity/ticketEntity");
var UserEntity             = require(appRootPath + "/lib/module/User/lib/entity/userEntity");
var RethinkDbTicketGateway = require(appRootPath + "/lib/module/Ticket/lib/gateway/ticket/rethinkDbTicketGateway");
var GatewayError           = require(appRootPath + "/lib/gateway/gatewayError");
var Request                = require(appRootPath + "/lib/request/request");

class RethinkDbWorkRecordGateway extends RethinkDbGateway
{
  /**
   * Cutom contractor allows to pass gateway instance
   *
   * @param object gateway Mapper"s gateway(i.e. dbConnection)
   */
  constructor(gateway)
  {
    super(gateway);

    this.table = {
      "name": "Ticket",
      "alias": "wr"
    };

    this.rethinkDbTicketGateway = new RethinkDbTicketGateway(gateway);

    /**
     * Object describing workRecord's relation
     *
     * @type Object
     */
    this.relations = {
      "user": {
        "table": "User",
        "localColumn": "userId",
        "targetColumn": "id",
        "defaultAlias": "u"
      }
    };
  }

  /**
   * Method fetches all records matching passed query builder's criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise      promise      Promise of DB result
   */
  fetchAll(ticketId, request)
  {
    var entities = [new WorkRecordEntity()];

    var joins = request.getJoins();
    if (_.contains(joins, "user")) {
      entities.push(new UserEntity());
    }

    var query = this.getRethinkDbSubQuery(ticketId);
    return this.query(request, entities, query);
  }

  /**
   * Method inserts new workRecord entity to table
   *
   * @param string ticketId Ticket id that workRecord will belong to
   * @param WorkRecordEntity workRecord WorkRecordEntity entity entity
   * @return Promise workRecord Promise of newly created workRecord entity
   */
  insert(ticketId, workRecord)
  {
    if (!(workRecord instanceof WorkRecordEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of WorkRecordEntity expected",
        workRecord,
        502
      );
    }

    workRecord.createdAt = this.getCurrentUtcDate();
    workRecord.updatedAt = null;

    let ticketEntity = new TicketEntity();
    ticketEntity.setId(ticketId);
    ticketEntity.set(
      "workRecords",
      {
        [workRecord.getId()]: workRecord.export()
      }
    );

    return this.rethinkDbTicketGateway.update(ticketEntity)
      .then(function (ticketData) {
        return new WorkRecordEntity(
          ticketData.workRecords[workRecord.getId()]
        );
      });
  }

  /**
   * Method updates entity in table
   *
   * @param string ticketId Ticket id that workRecord will belong to
   * @param WorkRecordEntity workRecord WorkRecordEntity entity
   * @return Promise workRecord Promise of newly created workRecord entity
   */
  update(ticketId, workRecord)
  {
    if (!(workRecord instanceof WorkRecordEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of WorkRecordEntity expected",
        workRecord,
        504
      );
    }

    workRecord.createdAt = undefined;
    workRecord.updatedAt = this.getCurrentUtcDate();

    let ticketEntity = new TicketEntity();
    ticketEntity.setId(ticketId);
    ticketEntity.set(
      "workRecords",
      {
        [workRecord.getId()]: workRecord.export()
      }
    );

    let request = new Request();
    request.setConditions({id: ticketId});

    let me = this;
    return this.rethinkDbTicketGateway.fetchOne(request)
      .then(function(ticketData) {

        if (!ticketData || _.isEmpty(ticketData.workRecords) ||
          !_.isObject(ticketData.workRecords[workRecord.getId()])
        ) {
          throw new GatewayError(
            "Specified workRecord doesn\'t exist",
            {
              id: workRecord.getId()
            },
            409
          );
        }

        return me.rethinkDbTicketGateway.update(ticketEntity)
          .then(function (ticketData) {
            return new WorkRecordEntity(
              ticketData.workRecords[workRecord.getId()]
            );
          });
      });
  }

  /**
   * Method replaces entity in table
   *
   * @param string ticketId Ticket id that workRecord will belong to
   * @param WorkRecordEntity workRecord WorkRecordEntity entity
   * @return Promise workRecord Promise of newly created workRecord entity
   */
  replace(ticketId, workRecord)
  {
    if (!(workRecord instanceof WorkRecordEntity)) {
      throw new GatewayError(
        "Invalid entity passed. Instance of WorkRecordEntity expected"
      );
    }

    let request = new Request();
    request.setConditions({id: ticketId});

    let me = this;
    return this.rethinkDbTicketGateway.fetchOne(request)
      .then(function(ticketData) {

        if (!ticketData || _.isEmpty(ticketData.workRecords) ||
          !_.isObject(ticketData.workRecords[workRecord.getId()])
        ) {
          throw new GatewayError(
            "Specified workRecord doesn\'t exist"
          );
        }

        ticketData.workRecords[workRecord.getId()] = workRecord.export();
        let ticketEntity = new TicketEntity(ticketData);

        return me.rethinkDbTicketGateway.replace(ticketEntity)
          .then(function (ticketData) {
            return new WorkRecordEntity(
              ticketData.workRecords[workRecord.getId()]
            );
          });
      });
  }

  /**
   * Method deletes workRecord from Ticket row
   *
   * @param string ticketId Ticket id
   * @param string workRecordId   WorkRecord id
   * @return Promise promise Promise of result of deleting
   * entity
   */
  delete(ticketId, workRecordId)
  {
    let request = new Request();
    request.setConditions({id: ticketId});

    let me = this;
    return this.rethinkDbTicketGateway.fetchOne(request)
      .then(function(ticketData) {

        if (!ticketData || _.isEmpty(ticketData.workRecords) ||
          !_.isObject(ticketData.workRecords[workRecordId])
        ) {
          throw new GatewayError(
            "Specified workRecord doesn\'t exist",
            {
              ticketId: ticketId,
              workRecordId: workRecordId
            },
            404
          );
        }

        return me.dataProvider.table(me.rethinkDbTicketGateway.table.name)
          .get(ticketId)
          .replace(
            me.dataProvider.table(me.table.name)
              .get(ticketId)
              .without({
                "workRecords" : workRecordId
              }),
              {nonAtomic: true}
          )
      });
  }

  /**
   * Method returns RethinkDb"s subquery to start from "workRecords" level
   *
   * @param string ticketId Ticket id
   * @return RethinkDbQuery query RethinkDb query object
   */
  getRethinkDbSubQuery(ticketId)
  {
    var query = this.dataProvider.table(this.table.name);
    return query.get(ticketId)("workRecords")
      .coerceTo("array")
      .map({
        left: {},
        right: this.dataProvider.row(1)
      }).zip();
  }
}

module.exports = RethinkDbWorkRecordGateway;
