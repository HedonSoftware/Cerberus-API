
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath      = require("app-root-path");
var WorkRecordEntity = require("../entity/workRecordEntity");
var Request          = require(appRootPath + "/lib/request/request");
var _                = require(appRootPath + "/lib/utility/underscore");
var BaseService      = require(appRootPath + "/lib/service/baseService");

class WorkRecordsService extends BaseService
{
  constructor(workRecordGateway)
  {
    super();

    if (!workRecordGateway) {
      var WorkRecordsMapper = require("../gateway/workRecord/rethinkDbWorkRecordGateway");
      workRecordGateway = new WorkRecordsMapper();
    }

    this.workRecordGateway = workRecordGateway;
  }

  /**
   * Method gets all workRecords matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  fetchAll(ticketId, query)
  {
    query = query || new Request();
    return this.workRecordGateway.fetchAll(ticketId, query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single workRecord by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchById(ticketId, workRecordId)
  {
    var query = new Request();
    query.setConditions({"id": workRecordId});

    return this.fetchAll(ticketId, query)
      .then(function (workRecords) {
        if (!_.isArray(workRecords) || _.isEmpty(workRecords)) {
          return null;
        }
        return workRecords.shift();
      });
  }

  create(ticketId, data)
  {
    var workRecord = new WorkRecordEntity(data);
    return this.workRecordGateway.insert(ticketId, workRecord);
  }

  update(ticketId, data)
  {
    var workRecord = new WorkRecordEntity(data);
    return this.workRecordGateway.update(ticketId, workRecord);
  }

  replace(ticketId, data)
  {
    var workRecord = new WorkRecordEntity(data);
    return this.workRecordGateway.replace(ticketId, workRecord);
  }

  delete(ticketId, id)
  {
    return this.workRecordGateway.delete(ticketId, id);
  }
}

module.exports = WorkRecordsService;
