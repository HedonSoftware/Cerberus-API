
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all WorkRecord service's related files
 */

// export all routes
module.exports.Routes = {
  WorkRecordRoutes: require("./lib/route/workRecordRoutes")
};

// export all services
module.exports.Services = {
  WorkRecordsService: require("./lib/service/workRecordsService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbWorkRecordGateway: require("./lib/gateway/workRecord/rethinkDbWorkRecordGateway")
};

// export all entities
module.exports.Entities = {
  WorkRecordEntity: require("./lib/entity/workRecordEntity")
};
