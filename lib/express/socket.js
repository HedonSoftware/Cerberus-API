
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var logger      = require(appRootPath + "/lib/logger/logger");

module.exports = function (server) {

  var io = require("socket.io").listen(server);

  io.on("connection", function (socket) {
    socket.broadcast.emit("user connected");

    socket.on("message", function (from, msg) {

      logger.info("recieved message from", from, "msg", JSON.stringify(msg));

      logger.info("broadcasting message");
      logger.info("payload is", msg);
      io.sockets.emit("broadcast", {
        payload: msg,
        source: from
      });
      logger.info("broadcast complete");
    });
  });
};
