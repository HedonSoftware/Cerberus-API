
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var express      = require("express");
var http         = require("http");
var config       = require("../config/config");
var middleware   = require("../middleware");
var morganLogger = require("morgan");
var logger       = require(appRootPath + "/lib/logger/logger");
var bodyParser   = require("body-parser");
var app          = express();
var router       = express.Router();

app.set("port", config.get("express:port"));

process.on("uncaughtException", function(error) {
  logger.error("Uncaught Exception", error);
});

// setting up logger
if (config.get("logger:console")) {
  app.use(
    morganLogger(
      config.get("logger:format"),
      {
        immediate: config.get("logger:immediate")
      }
    )
  );
}

app.use(bodyParser.json());
app.post("*", middleware.validatorMiddleware.requestData);
app.put("*", middleware.validatorMiddleware.requestData);
app.patch("*", middleware.validatorMiddleware.requestData);
app.all("*", middleware.requestMiddleware.validate);

// load our routes
require(appRootPath + "/app/routes.js")(router);

app.use("/", router);

// response with not found(404) when invalid link queried
// response with internal server error(500) and logger error
app.use(middleware.notFoundMiddleware);
app.use(middleware.errorHandlerMiddleware);

var server = http.createServer(app);

// adding Socket.io support
require("./socket")(server);

server.listen(app.get("port"));

module.exports = app;

logger.info("Cerberus API is now running on port " + app.get("port"));
