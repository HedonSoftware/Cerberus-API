
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var EntityError = require("./entityError");
var inspector   = require("schema-inspector");
var _           = require(appRootPath + "/lib/utility/underscore");
var logger      = require(appRootPath + "/lib/logger/logger");

class BaseEntity
{
  /**
   * Constructor needs to be overriden by extending class
   */
  constructor()
  {
    if (this.constructor === BaseEntity) {
      throw new EntityError(
        "BaseEntity used incorrectly. You need to extend default constructor!"
      );
    }

    this.sanitization = {
      type: 'object',
      strict: true
    };
  }

  /**
   * Method sets fields of entity
   *
   * @param object fields List of fields
   * @return self this Fluent interface
   */
  setFields(fields)
  {
    this.sanitization.properties = fields;
    return this;
  }

  /**
   * Method returns fields of entity
   *
   * @return object fields List of fields
   */
  getFields()
  {
    return this.sanitization.properties;
  }

  /**
   * Method inflates entity object
   *
   * @param object data Data to be used for inflation
   * @return self Fluent interface
   */
  inflate(data)
  {
    if (!_.isObject(data)) {
      throw new EntityError(
        "Invalid data provided. Object expected"
      );
    }

    var result = inspector.sanitize(this.sanitization, data);
    var sanitizedData = result.data;

    // reporting potentially contains error(s)
    if (result.reporting.length >= 1) {
      logger.info("BaseEntity: " + result.format());
    }

    var that = this;
    _(sanitizedData).forEach(function(value, key) {
      that.set(key, value);
    });

    return this;
  }

  /**
   * Method returns properties of entity
   *
   * @return Array properties Entity's properties
   */
  getProperties()
  {
    if (!this.properties) {

      var that = this;
      this.properties = [];

      _.forEach(this.getFields(), function(fieldDefinition, fieldName) {
        that.properties.push(fieldName);
      });

      if (_.isEmpty(this.properties)) {
        throw new EntityError(
          "Invalid entity definition. No properties defined"
        );
      }
    }

    return this.properties;
  }

  /**
   * Custom set method filters passed keys based on
   * entity"s properties
   *
   * @param string key   Property name
   * @param mixed  value Property value
   * @return self this Fluent interface
   */
  set(key, value)
  {
    let entityProperties = this.getProperties();

    if (entityProperties.indexOf(key) !== -1) {

      let setter = "set" + _(key).capitalize();
      if (typeof this[setter] === "function") {
        this[setter](value);
        return this;
      }

      this[key] = value;
    }

    return this;
  }

  /**
   * Returns value of passed entity's property
   * @param string propertyName
   */
  get(propertyName)
  {
    if (!_.isString(propertyName) || _.isEmpty(propertyName)) {
      throw new EntityError(
        "Invalid property name to be retrieved provided. Non-empty string expected",
        propertyName,
        500
      );
    }

    return this[propertyName];
  }

  /**
   * Method checks is passed property part of entity
   *
   * @param string property Property name
   * @return bool result Result of check is passed
   * property one of entity"s properties
   */
  has(property)
  {
    return this.getProperties().indexOf(property) > -1;
  }

  /**
   * Method exports data from entity
   *
   * @return object exportedData Data exported
   * from entity
   */
  export()
  {
    let exportedData = {};

    var that = this;
    _.forEach(this.getProperties(), function(fieldName) {
      if (typeof that[fieldName] != "undefined") {
        exportedData[fieldName] = that[fieldName];
      }
    });

    return exportedData;
  }
}

module.exports = BaseEntity;
