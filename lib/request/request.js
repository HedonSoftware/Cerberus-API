
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var _            = require(appRootPath + "/lib/utility/underscore");
var RequestError = require("./requestError");

/**
 * Request class provides deep validation of user"s requests
 */
class Request
{
  /**
   * Custom constructor sets supported logical, order and comparision
   * operators
   */
  constructor(requestData)
  {
    this.supportedLogicOperators = [
      "and",
      "or"
    ];

    this.supportedOrderOperators = [
      "asc",
      "desc"
    ];

    this.supportedComparisionOperators = [
      "eq",      // Matches values that are exactly the same as the value specified in the query.
      "gt",      // Matches values that are greater than the value specified in the query.
      "gte",     // Matches values that are greater than or equal to the value specified in the query.
      "lt",      // Matches values that are less than the value specified in the query.
      "lte",     // Matches values that are less than or equal to the value specified in the query.
      "ne",      // Matches all values that are not equal to the value specified in the query.
      "in",      // Matches values that do exist in an array specified to the query.
      "nin",     // Matches values that do not exist in an array specified to the query.
      "like",    // Matches values that are matching pattern passed in the value specified in the query.
      "contains" // Matches one of values in passed key which needs to be array
    ];

    this.supportedOperators = _.union(
      this.supportedOrderOperators,
      this.supportedComparisionOperators
    );

    this.defaultResultsLimit = 20;
    this.reset();

    if (requestData) {
      this.inflate(requestData);
    }
  }

  /**
   * Method inflates an Request object using passed data
   *
   * @param  object data Assoc array of conditions, fields, joins etc.
   * @return Request this Fluent interface
   */
  inflate(data)
  {
    if (_.has(data, "fields")) {
      this.setFields(data.fields);
    }

    if (_.has(data, "joins")) {
      this.setJoins(data.joins);
    }

    if (_.has(data, "conditions")) {
      this.setConditions(data.conditions);
    }

    if (_.has(data, "order")) {
      this.setOrder(data.order);
    }

    if (_.has(data, "limit")) {
      this.setLimit(data.limit);
    }

    if (_.has(data, "offset")) {
      this.setOffset(data.offset);
    }

    return this;
  }

  /**
   * Method sets list of fields to be retrieved
   * Converts 0: id to id: id
   *
   * @param array|object|string fields List of fields or single field
   * @return Request this Fluent interface
   */
  setFields(fields)
  {
    if (_.isString(fields)) {
      fields = [fields];
    }

    if ((!_.isArray(fields) && !_.isObject(fields)) || _.isEmpty(fields)) {
      throw new RequestError(
        "Invalid list of fields provided. String|Object|Array expected",
        fields,
        2
      );
    }

    let filtered = {};
    _(fields).forEach(function(field, index) {

      if (!_.isString(field) || _.isEmpty(field)) {

        let type = "field";
        if (isNaN(index)) {
          type = "field alias";
        }

        throw new RequestError(
          "Invalid " + type + " passed: '" + field + "'",
          {
            field : field
          },
          3
        );
      }

      if (_.isString(index) && _.isEmpty(index)) {
        throw new RequestError(
          "Invalid field passed: '" + index + "'",
          {
            index: index
          },
          4
        );
      }

      if (isNaN(index)) {
        filtered[index] = field;
      } else {
        let aliasedField = field;
        if (field.indexOf(".") > -1) {
          aliasedField = field.split(".").pop();
        }

        filtered[field] = aliasedField;
      }

    });

    this.query.fields = filtered;
    this.usedFields = _.union(this.usedFields, _.keys(filtered));

    return this;
  }

  /*
   * Method gets list of fields to be retrieved
   *
   * @param array fields List of fields
   * @return Request this Fluent interface
   */
  getFields()
  {
    return this.query.fields;
  }

  /**
   * Method sets resource"s relation(s) to be joined
   * Expected structure:
   * [
   *   "creator"
   * ]
   * @param array relations List of resource"s relation(s)
   * @return Request this Fluent interface
   */
  setJoins(relations)
  {
    if (_.isString(relations)) {
      relations = [relations];
    }

    if (!_.isArray(relations) ||
      _.isEmpty(relations)
    ) {
      throw new RequestError(
        "Invalid join relations list provided",
        relations,
        5
      );
    }

    for (var index in relations) {
      let relation = relations[index];

      if (!_.isString(relation) || relation.length === 0) {
        throw new RequestError(
          "Invalid join relation provided: '" + relation + "'",
          relation,
          6
        );
      }
    }

    this.query.joins = relations;
    return this;
  }

  /**
   * Method returns resources to be joined
   * Expected structure:
   * [
   *   owner
   * ]
   * @return array resources List of resources
   */
  getJoins()
  {
    return this.query.joins ? this.query.joins : [];
  }

  /**
   * Method sets query"s conditions
   *
   * @param object data Conditions details field => value|condition
   * @return Request this Fluent interface
   */
  setConditions(data)
  {
    this.validateCondtionsLevel(data, 1);

    // if nothing above thrown an exception we can safely assign the conditions
    this.query.conditions = data;

    return this;
  }

  /**
   * Method gets query"s conditions
   *
   * @param object data Conditions details field => value|condition
   * @return Request this Fluent interface
   */
  getConditions()
  {
    return this.query.conditions;
  }

  /**
   * Recursive method validates single condition level
   *
   * @param data  object  Validated condition level
   * @param level integer Level of recursion (how deep condition is)
   */
  validateCondtionsLevel(data, level)
  {
    if (!_.isObject(data) || _.size(data) === 0 || _.isArray(data)) {
      throw new RequestError(
        "Invalid data structure provided",
        {
          level: level,
          data: data
        },
        7
      );
    }

    var value = null;

    for (var index in data) {

      value = data[index];

      // if it"s logic it should point to array with more than 1 element
      if (index === "and" || index === "or") {

        if (!_.isArray(value)) {
          throw new RequestError(
            "Logic operator expects array of conditions",
            value,
            8
          );
        }

        if (value.length < 2) {
          throw new RequestError(
            "Logic operator expects array of at least two conditions",
            value,
            9
          );
        }

        if (level > 3) {
          throw new RequestError(
            "Only 3 levels of nested conditions are supported",
            data,
            4
          );
        }

        var singleCondtion = null;
        for (var tempIndex in value) {
          singleCondtion = value[tempIndex];
          level += 1;
          this.validateCondtionsLevel(singleCondtion, level);
        }

      } else {

        // now we have something like that
        // {
        //   field : "value",
        // }
        // or
        // {
        //   field2 : {
        //     "gt" : "value2"
        //   }
        // }
        // or
        // {
        //   field3 : ["abc", "def"]
        // }
        // or mix of 2 above
        // index is a field
        var field = index;

        this.usedFields = _.union(this.usedFields, [field]);

        // if type 1 it"s ok!
        if (_.isString(value) || _.isNumber(value)) {
          continue;
        }

        // if it"s type 2 validate that contains only strings etc
        if (_.isArray(value)) {
          var tempValue = null;
          for (var tempIndex in value) {
            tempValue = value[tempIndex];
            if (!_.isString(tempValue) && !_.isNumber(tempValue)) {
              throw new RequestError(
                "Invalid list of values for field: '" + field + "'",
                {
                  field : field,
                  value : tempValue
                },
                10
              );
            }
          }
          continue;
        }

        if (_.isObject(value)) {
          var tempValue = null;
          for (var tempIndex in value) {
            tempValue = value[tempIndex];

            if (_.isArray(tempValue)) {

              if (!_.contains(this.supportedComparisionOperators, tempIndex)) {
                throw new RequestError(
                  "Invalid operator provided for field: '" + field + "'",
                  {
                    field : field,
                    operator: tempIndex
                  },
                  12
                );
              }

              var i = null;
              for (var v in tempValue) {
                i = tempValue[v];
                // each value can be only string or integer
                if (!_.isString(i) && !_.isNumber(i)) {
                  throw new RequestError(
                    "Invalid list of values for field: '" + field + "' and operator: '" + tempIndex + "'",
                    {
                      field : field,
                      operator : tempIndex
                    },
                    11
                  );
                }
              }
              continue;
            }

            // each index should be valid operator
            if (!_.contains(this.supportedComparisionOperators, tempIndex)) {
              throw new RequestError(
                "Invalid operator provided for field: '" + field + "'",
                {
                  field : field
                },
                12
              );
            }

            // each value can be only string or integer
            if (!_.isString(tempValue) && !_.isNumber(tempValue)) {
              throw new RequestError(
                "Invalid list of values for field: " + field + " and operator: " + tempIndex,
                {
                  field : field,
                  opeator : tempIndex
                },
                13
              );
            }
          }

          continue;

        }

        throw new RequestError(
          "Invalid data structure provided for field: '" + field + "'",
          value,
          14
        );

      }
    }
  }

  /**
   * Sets query order
   *
   * Accepted object:
   * {
   *   "field1" : "desc"
   * }
   *
   * @param object order Order description
   * @return Request this Fluent interface
   */
  setOrder(order)
  {
    if (_.isString(order)) {
      order = {
        [order]: "asc"
      };
    }

    if (_.isArray(order)) {
      let newOrder = {};
      _(order).forEach(function(orderField) {
        if (!_.isString(orderField)) {
          throw new RequestError(
            "Invalid order field(s) passed",
            order,
            15
          );
        }
        newOrder[orderField] = "asc";
      });
      order = newOrder;
    }

    if (!_.isObject(order)) {
      throw new RequestError(
        "Invalid order passed",
        order,
        15
      );
    }

    var orderOperator = "";
    for (var field in order) {
      orderOperator = order[field];
      if (!_.isString(orderOperator) ||
        !_.contains(this.supportedOrderOperators, orderOperator)
      ) {
        throw new RequestError(
          "Invalid order operator passed",
          orderOperator,
          16
        );
      }
      this.usedFields = _.union(this.usedFields, [field]);
    }

    this.query.order = order;
  }

  /**
   * Getter for query order
   *
   * @return object Order description
   */
  getOrder()
  {
    return this.query.order;
  }

  /**
   * Method validates and sets limit for query
   *
   * @param integer limit Limit to be set in query
   * @return Request this Fluent interface
   * @throws RequestError When passed limit is invalid
   */
  setLimit(limit)
  {
    if ((!_.isNumber(limit) && !_.isString(limit)) ||
      parseInt(limit) != limit || limit < 1)
    {
      throw new RequestError(
        "Invalid limit passed. Positive integer value expected",
        limit,
        17
      );
    }

    this.query.limit = parseInt(limit);

    return this;
  }

  /**
   * @return number Query Limit
   */
  getLimit()
  {
    return this.query.limit;
  }

  /**
   * Method validates and sets offset for query
   *
   * @param integer offset Offset value
   * @return Request this Fluent interface
   * @throws RequestError When passed limit is invalid
   */
  setOffset(offset)
  {
    if ((!_.isNumber(offset) && !_.isString(offset)) ||
      parseInt(offset) != offset || offset < 1)
    {
      throw new RequestError(
        "Invalid offset passed. Positive integer value expected",
        offset,
        18
      );
    }

    this.query.offset = parseInt(offset);

    return this;
  }

  /**
   * @return number Offset value
   */
  getOffset()
  {
    return this.query.offset;
  }

  /**
   * Method returns fields used in query
   *
   * @return Array List of fields used in query
   */
  getUsedFields()
  {
    return this.usedFields;
  }

  /**
   * Method returns array representation of query
   * @return object query Object describing query
   */
  export()
  {
    return this.query;
  }

  /**
   * Method clears current state of Request entity
   *
   * @return Request this Fluent interface
   */
  reset()
  {
    this.query = {
      conditions: {},
      limit: this.defaultResultsLimit
    };

    this.usedFields = [];

    return this;
  }
}

module.exports = Request;

