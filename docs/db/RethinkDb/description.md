
1. Projects collection contains boards which contains lanes
2. Each lane contains list of tag references (type: lane)
3. Tags were promoted to their own collection to be able to easily list all of them what is required to create new boards
4. Tickets are linked to board's lanes via tags[type=lane] (this way multiple boards can show the same tickets)
5. Tickets are linked to boards via tags[type=sprint] (this way you can dynamically assign tickets to sprints)


There are 3 types of boards:
- action - it's a kanban/sprint active board
- overview - it's a read-only board (enables overviewing multiple boards in one time), allows to create sprints
- backlog - it's a backlog of project

