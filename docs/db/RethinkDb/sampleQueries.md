
Example 1
=========

Mysql Query
-----------

SELECT
  u.id,
  u.username,
  u.firstName,
  u.lastName,
  u.email,
  r.name as roleName
FROM
  User as u
LEFT JOIN
  AclRole as r
  ON u.roleId = r.id
WHERE
  u.roleId = '096b009d-015c-4f23-b3ca-a49faae377d8'
ORDER BY
  username DESC
LIMIT
  3

RethinkDb query
---------------

r.db("cerberus").table("User")
.filter(function (doc) {
    return doc("roleId").eq("096b009d-015c-4f23-b3ca-a49faae377d8");
  }
)
.eqJoin(
    "roleId",
    r.db("cerberus").table("AclRole"),
    {index: "id"}
).map({
  left : r.row("left"),
  right : {
    roleName: r.row("right")("name")
  }
})
.zip()
.orderBy(r.desc("username"))
.limit(3)
.map({
  id: r.row("id"),
  username: r.row("username"),
  firstName: r.row("firstName"),
  lastName: r.row("lastName"),
  email: r.row("email"),
  roleName: r.row("roleName")
});

Example 2
=========

Mysql Query
-----------

SELECT
  u.id,
  u.username,
  u.firstName,
  u.lastName,
  u.email,
  r.name as roleName
FROM
  User as u
LEFT JOIN
  AclRole as r
  ON u.roleId = r.id
WHERE
  u.roleId = '096b009d-015c-4f23-b3ca-a49faae377d8'
ORDER BY
  username DESC
LIMIT
  3

RethinkDb query
---------------

r.db("cerberus").table("User")
.filter(function (doc) {
    return doc("roleId").eq("096b009d-015c-4f23-b3ca-a49faae377d8");
  }
)
.eqJoin(
    "roleId",
    r.db("cerberus").table("AclRole"),
    {index: "id"}
).map({
  left : r.row("left"),
  right : {
    roleName: r.row("right")("name")
  }
})
.zip()
.orderBy(r.desc("username"))
.limit(3)
.map({
  id: r.row("id"),
  username: r.row("username"),
  firstName: r.row("firstName"),
  lastName: r.row("lastName"),
  email: r.row("email"),
  roleName: r.row("roleName")
});

Example 3
=========

Debugging in RethinkDb:

r.db("cerberus_test").table("Project").merge(
  function(project) {
    return r.error(project.coerceTo("STRING"));
  }
)

Example 4
=========

Right join

r.db("cerberus_test").table("Project").merge(
  function(project) {

    return { 'boards': r.db("cerberus_test").table("Board").filter(
      {
        projectId : project('id')
      }
   ).coerceTo('array') }

Example 5
=========

Getting really deep and then filtering

r.db("cerberus_test").table("Ticket")
  .get("61ad71eb-0ca2-49af-b371-c16599207bbc")("comments")
  .filter(
    {
      userId: "76039cc1-96eb-4721-81b3-211b6979d018"
    }
  );

Example 6
=========

Selecting nested board from project based on project id and unique board name

r.db("cerberus_test")
  .table("Project")
  .get("460ad418-ac09-44bb-9941-a2a645032c01")("boards")("backlog");

Example 7
=========

The same as example 6 just going deeper

Example 8
=========

Filter on nested entities(contained within array) level + join on that level to other table:

r.db("cerberus").table("Ticket")
  .get("61ad71eb-0ca2-49af-b371-c16599207bbc")("comments")
  .filter({text: "Full regression tests are required ..."})
  .outerJoin(r.db("cerberus").table("User"), function(var_1, var_2) {
    return var_2("id").eq(var_1("userId"))
}).map({
    left: r.row("left"),
    right: {
        username: r.row("right")("username")
    }
}).zip()

Example 9
=========

Get tags for board's lanes and selects all tickets:

Step 1:

r.db('cerberus')
  .table('Tag')
  .filter({
    boardId: "0a193918-ebe1-4ca1-8450-7924c0321868",
    type: "lane"
  });

Step 2:
r.db('cerberus')
  .table('Ticket')
  .filter(function(ticket) {
    return r.or(
      ticket('tags').contains("e12ba8d6-b125-4692-94bb-616f7fbc8ab5"),
      ticket('tags').contains("261cfeb0-8c47-48ac-acfa-6a59a140235e"),
      ticket('tags').contains("da2f64bd-1d70-4415-a232-95b2f35366ba")
    )
  });

Example 10
==========

Get only tickets in specific sprint:

Version 1:

r.db('cerberus').table('Ticket').filter(function(hero) {
    return r.or(
      r.and(
        hero('tags').contains("e12ba8d6-b125-4692-94bb-616f7fbc8ab5"),
        hero('tags').contains("3f2213f2-0c04-44bf-907a-9f167404adff")
      ),
      r.and(
        hero('tags').contains("261cfeb0-8c47-48ac-acfa-6a59a140235e"),
        hero('tags').contains("3f2213f2-0c04-44bf-907a-9f167404adff")
      ),
      r.and(
        hero('tags').contains("da2f64bd-1d70-4415-a232-95b2f35366ba"),
        hero('tags').contains("3f2213f2-0c04-44bf-907a-9f167404adff")
      )
    )
});

Version 2:

r.db('cerberus').table('Ticket').filter(function(hero) {
    return r.and(
      hero('tags').contains("3f2213f2-0c04-44bf-907a-9f167404adff"),
      r.or(
        hero('tags').contains("e12ba8d6-b125-4692-94bb-616f7fbc8ab5"),
        hero('tags').contains("261cfeb0-8c47-48ac-acfa-6a59a140235e"),
        hero('tags').contains("da2f64bd-1d70-4415-a232-95b2f35366ba")
      )
   );
});