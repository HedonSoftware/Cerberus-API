
/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var middleware  = require(appRootPath + "/lib/middleware");

var BoardRoutes = require(appRootPath + "/lib/module/Board").Routes.BoardRoutes;
var boardRoutes = new BoardRoutes();

var CommentRoutes = require(appRootPath + "/lib/module/Comment").Routes.CommentRoutes;
var commentRoutes = new CommentRoutes();

var LaneRoutes = require(appRootPath + "/lib/module/Lane").Routes.LaneRoutes;
var laneRoutes = new LaneRoutes();

var ProjectRoutes = require(appRootPath + "/lib/module/Project").Routes.ProjectRoutes;
var projectRoutes = new ProjectRoutes();

var SprintRoutes = require(appRootPath + "/lib/module/Sprint").Routes.SprintRoutes;
var sprintRoutes = new SprintRoutes();

var TagRoutes = require(appRootPath + "/lib/module/Tag").Routes.TagRoutes;
var tagRoutes = new TagRoutes();

var TicketRoutes  = require(appRootPath + "/lib/module/Ticket").Routes.TicketRoutes;
var ticketRoutes = new TicketRoutes();

var UserRoutes = require(appRootPath + "/lib/module/User").Routes.UserRoutes;
var userRoutes = new UserRoutes();

var WorkRecordRoutes = require(appRootPath + "/lib/module/WorkRecord").Routes.WorkRecordRoutes;
var workRecordRoutes = new WorkRecordRoutes();

module.exports = function(router) {

  // user resources
  router.param("id", middleware.validatorMiddleware.id);
  router.get("/users", userRoutes.get.bind(userRoutes));
  router.get("/users/:id", userRoutes.getById.bind(userRoutes));
  router.post("/users", userRoutes.create.bind(userRoutes));
  router.patch("/users/:id", userRoutes.update.bind(userRoutes));
  // router.put("/users/:id", userRoutes.replace.bind(userRoutes));
  router.delete("/users/:id", userRoutes.delete.bind(userRoutes));

  // tag resources
  router.param("id", middleware.validatorMiddleware.id);
  router.get("/tags", tagRoutes.get.bind(tagRoutes));
  router.get("/tags/:id", tagRoutes.getById.bind(tagRoutes));
  router.post("/tags", tagRoutes.create.bind(tagRoutes));
  router.patch("/tags/:id", tagRoutes.update.bind(tagRoutes));
  // router.put("/tags/:id", tagRoutes.replace.bind(tagRoutes));
  router.delete("/tags/:id", tagRoutes.delete.bind(tagRoutes));

  // project resources
  router.param("id", middleware.validatorMiddleware.id);
  router.get("/projects", projectRoutes.get.bind(projectRoutes));
  router.get("/projects/:id", projectRoutes.getById.bind(projectRoutes));
  router.post("/projects", projectRoutes.create.bind(projectRoutes));
  router.patch("/projects/:id", projectRoutes.update.bind(projectRoutes));
  // router.put("/projects/:id", projectRoutes.replace.bind(projectRoutes));
  router.delete("/projects/:id", projectRoutes.delete.bind(projectRoutes));

  // project -> board resources
  router.param("projectId", middleware.validatorMiddleware.id);
  router.get("/projects/:projectId/boards", boardRoutes.get.bind(boardRoutes));
  router.get("/projects/:projectId/boards/:id", boardRoutes.getById.bind(boardRoutes));
  router.post("/projects/:projectId/boards", boardRoutes.create.bind(boardRoutes));
  router.patch("/projects/:projectId/boards/:id", boardRoutes.update.bind(boardRoutes));
  // router.put("/projects/:projectId/boards/:id", boardRoutes.replace.bind(boardRoutes));
  router.delete("/projects/:projectId/boards/:id", boardRoutes.delete.bind(boardRoutes));

  // project -> board -> lane resources
  router.param("projectId", middleware.validatorMiddleware.id);
  router.param("boardId", middleware.validatorMiddleware.id);
  router.param("laneId", middleware.validatorMiddleware.id);
  router.get("/projects/:projectId/boards/:boardId/lanes", laneRoutes.get.bind(laneRoutes));
  router.get("/projects/:projectId/boards/:boardId/lanes/:id", laneRoutes.getById.bind(laneRoutes));
  router.post("/projects/:projectId/boards/:boardId/lanes", laneRoutes.create.bind(laneRoutes));
  router.patch("/projects/:projectId/boards/:boardId/lanes/:id", laneRoutes.update.bind(laneRoutes));
  // router.put("/projects/:projectId/boards/:boardId/lanes/:id", laneRoutes.replace.bind(laneRoutes));
  router.delete("/projects/:projectId/boards/:boardId/lanes/:id", laneRoutes.delete.bind(laneRoutes));

  // project -> board -> sprint resources
  router.param("sprintId", middleware.validatorMiddleware.id);
  router.get("/projects/:projectId/boards/:boardId/sprints", sprintRoutes.get.bind(sprintRoutes));
  router.get("/projects/:projectId/boards/:boardId/sprints/:id", sprintRoutes.getById.bind(sprintRoutes));
  router.post("/projects/:projectId/boards/:boardId/sprints", sprintRoutes.create.bind(sprintRoutes));
  router.patch("/projects/:projectId/boards/:boardId/sprints/:id", sprintRoutes.update.bind(sprintRoutes));
  // router.put("/projects/:projectId/boards/:boardId/sprints/:id", sprintRoutes.replace.bind(sprintRoutes));
  router.delete("/projects/:projectId/boards/:boardId/sprints/:id", sprintRoutes.delete.bind(sprintRoutes));

  // ticket resources
  router.param("id", middleware.validatorMiddleware.id);
  router.get("/tickets", ticketRoutes.get.bind(ticketRoutes));
  router.get("/tickets/:id", ticketRoutes.getById.bind(ticketRoutes));
  router.post("/tickets", ticketRoutes.create.bind(ticketRoutes));
  router.patch("/tickets/:id", ticketRoutes.update.bind(ticketRoutes));
  // router.put("/tickets/:id", ticketRoutes.replace.bind(ticketRoutes));
  router.delete("/tickets/:id", ticketRoutes.delete.bind(ticketRoutes));

  // ticket -> comment resources
  router.get("/tickets/:ticketId/comments", commentRoutes.get.bind(commentRoutes));
  router.get("/tickets/:ticketId/comments/:id", commentRoutes.getById.bind(commentRoutes));
  router.post("/tickets/:ticketId/comments", commentRoutes.create.bind(commentRoutes));
  router.patch("/tickets/:ticketId/comments/:id", commentRoutes.update.bind(commentRoutes));
  // router.put("/tickets/comments/:id", commentRoutes.replace.bind(commentRoutes));
  router.delete("/tickets/:ticketId/comments/:id", commentRoutes.delete.bind(commentRoutes));

  // ticket -> work record resources
  router.param("id", middleware.validatorMiddleware.id);
  router.get("/tickets/:ticketId/work-records", workRecordRoutes.get.bind(workRecordRoutes));
  router.get("/tickets/:ticketId/work-records/:id", workRecordRoutes.getById.bind(workRecordRoutes));
  router.post("/tickets/:ticketId/work-records", workRecordRoutes.create.bind(workRecordRoutes));
  router.patch("/tickets/:ticketId/work-records/:id", workRecordRoutes.update.bind(workRecordRoutes));
  // router.put("/tickets/:ticketId/work-records/:id", workRecordRoutes.replace.bind(workRecordRoutes));
  router.delete("/tickets/:ticketId/work-records/:id", workRecordRoutes.delete.bind(workRecordRoutes));
};
