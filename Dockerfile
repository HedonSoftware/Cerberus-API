
# --------------------------------------------------------
# ----------------- Building docker file -----------------
# --------------------------------------------------------
#
# ---
# To build it:
#
# sudo docker build --no-cache -t "hedonsoftware/cerberus-api:2.0.0" .
#
# ---
#
# To check it after build:
#
# * create docker network bridge and run rethinkDb:
# sudo docker network create kogonet
#
# sudo docker run -itP \
#   --net kogonet \
#   --volume "/var/data/cerberus-api/rethinkdb:/data" \
#   --detach \
#   rethinkdb
#
# * load database
# sudo docker run -it \
#   --net kogonet \
#   hedonsoftware/cerberus-api:2.0.0 \
#   node /opt/cerberus-api/build/RethinkDb/init.js
#
# * then start newly build container with link to rethinkDb container:
# sudo docker run -itP \
#   --net kogonet \
#   --volume /var/logs/cerberus-api:/opt/cerberus-api/logs \
#   --name hedonsoftware-cerberus-api \
#   --publish 3001:3001 \
#   --detach \
#   --restart=always \
#   hedonsoftware/cerberus-api:2.0.0
#
# ---
# To check IP addresses of newly created boxes
# ---
#
# sudo docker network inspect kogonet
#
# ---
# Publishing new package
# ---
#
# sudo docker push hedonsoftware/cerberus-api:2.0.0
#
# --------------------------------------------------------

FROM    iojs:latest

# Getting newest version of node
RUN     npm install -g n
RUN     n latest

# Creating directory to store project files
RUN    mkdir -p /opt/cerberus-api

# Copy files across to container
COPY   . /opt/cerberus-api

# Install app dependencies
RUN    cd /opt/cerberus-api; npm install --production

EXPOSE  3001

CMD ["node", "/opt/cerberus-api/app.js"]
