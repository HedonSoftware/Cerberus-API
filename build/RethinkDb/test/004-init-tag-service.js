
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("Tag").then(function () {

  return r.table("Tag").indexCreate("name").run();

}).then(function () {

  var resources = [
    {
      id: "322b84e5-4122-4a6d-891d-b7d83fb182a0",
      name: "tag",
      description: "Tag",
      status: "active",
      createdAt: new Date("2015-08-22T18:34:53.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

}).then(function () {

  var tags = [

    // -----------------------------------------------
    // ------------------ UNIVERSE -------------------
    // -----------------------------------------------

    // Universe -> Frontend board tag

    {
      id: "14fbcf19-de75-4cbc-8111-2b26c220a2d8",
      name: "Universe Frontend",
      description: "Frontend board",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      type: "board",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Universe -> Frontend's lane tags

    {
      id: "da2f64bd-1d70-4415-a232-95b2f35366ba",
      name: "To do",
      description: "Tickets to be started",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "0a193918-ebe1-4ca1-8450-7924c0321868",   // Frontend
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "e12ba8d6-b125-4692-94bb-616f7fbc8ab5",
      name: "In progress",
      description: "Tickets that are in progress",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "0a193918-ebe1-4ca1-8450-7924c0321868",   // Frontend
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T17:23:54.000Z"),
      updatedAt: null
    },
    {
      id: "261cfeb0-8c47-48ac-acfa-6a59a140235e",
      name: "Done",
      description: "Tickets that are finished",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "0a193918-ebe1-4ca1-8450-7924c0321868",   // Frontend
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T04:00:21.000Z"),
      updatedAt: null
    },

    // Universe -> Frontend's sprint tags

    {
      id: "6fdbebe4-4f1b-4304-86a5-e67fc6333c49",
      name: "Sprint 0.0.1",
      description: "Sprint 0.0.1 tag",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "0a193918-ebe1-4ca1-8450-7924c0321868",   // Frontend
      type: "sprint",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    {
      id: "e53be40b-b712-4a5a-8b99-c5b400b9773e",
      name: "Sprint 0.0.2",
      description: "Sprint 0.0.2 tag",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "0a193918-ebe1-4ca1-8450-7924c0321868",   // Frontend
      type: "sprint",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Universe -> Api's board tags

    {
      id: "4b2b9a53-a5db-4490-b005-3e68cef3ba98",
      name: "Universe API",
      description: "Universe API",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "43469ba8-b072-4bce-83d2-661ef5164972",   // Api
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Universe -> Api's lane tags

    {
      id: "705b5f6a-2448-4309-98ed-47acc0806886",
      name: "To do",
      description: "Tickets to be started",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "43469ba8-b072-4bce-83d2-661ef5164972",   // Api
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "d91a31c9-aa3b-458d-9143-9fdfaf088181",
      name: "Development",
      description: "Tickets under development",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "43469ba8-b072-4bce-83d2-661ef5164972",   // Api
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "cc1ed2c3-a2f9-4d93-9e93-859140f0e6e7",
      name: "Review",
      description: "Tickets under review",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "43469ba8-b072-4bce-83d2-661ef5164972",   // Api
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8",
      name: "Done",
      description: "Finished tickets",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "43469ba8-b072-4bce-83d2-661ef5164972",   // Api
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Universe -> Api's sprint tags

    {
      id: "8e4fe830-283b-4fea-994c-b421250bfa00",
      name: "Sprint 1.0.0",
      description: "Sprint 1.0.0 tag",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "43469ba8-b072-4bce-83d2-661ef5164972",   // Api
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "71f1b0f1-cc82-4056-b349-c3422ed814ea",
      name: "Sprint 1.0.1",
      description: "Sprint 1.0.1 tag",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "43469ba8-b072-4bce-83d2-661ef5164972",   // Api
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Universe Backlog's board tag

    {
      id: "de2de2b3-d5f5-4668-aab4-e05acaeae8a4",
      name: "Universe Backlog",
      description: "Universe backlog",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      type: "board",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Universe -> Backlog's lane tags

    {
      id: "076ef7ce-845d-4237-b3ad-1ce0bb5e37f1",
      name: "Must",
      description: "Tickets that must be added to next sprint",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "408774d5-207b-4523-aa35-74b4a72be6d2",   // Backlog
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "734f9494-d16f-49e1-9535-fab7a124bb2d",
      name: "Should",
      description: "Tickets that should be added to next sprint",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "408774d5-207b-4523-aa35-74b4a72be6d2",   // Backlog
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71",
      name: "Could",
      description: "Tickets that could be added to next sprint",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "408774d5-207b-4523-aa35-74b4a72be6d2",   // Backlog
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "84c413e4-7031-4569-af59-0763b714cad5",
      name: "Won't",
      description: "Tickets that won't be added to next sprint",
      projectId: "5de514c2-49f6-429e-a478-36bdc206cab4", // Universe
      boardId: "408774d5-207b-4523-aa35-74b4a72be6d2",   // Backlog
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // -----------------------------------------------
    // ------------------- GALAXY --------------------
    // -----------------------------------------------

    // Galaxy -> Design's board tag

    {
      id: "e7981571-f6f9-469f-bf97-7dbbad19b439",
      name: "Galaxy design",
      description: "Galaxy design board",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      type: "board",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Galaxy -> Design's lane tags

    {
      id: "05165da4-3950-48d9-bffb-5aff67edb47b",
      name: "To do",
      description: "Tickets in sprint waiting to be picked up",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "b6f65cd5-8deb-4c86-9c32-58fbd0f2b1fa",   // Design
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "87d87f31-ce2b-42a9-8155-b923c548f922",
      name: "In progress",
      description: "In progress - designing",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "b6f65cd5-8deb-4c86-9c32-58fbd0f2b1fa",   // Design
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "06580431-9b48-4c79-baf6-07259f3975a7",
      name: "Done",
      description: "Finished",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "b6f65cd5-8deb-4c86-9c32-58fbd0f2b1fa",   // Design
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Galaxy -> Development's board tag

    {
      id: "4b5dcd90-b07b-4044-8572-0679a937cc06",
      name: "Galaxy Development",
      description: "Galaxy development board",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      type: "board",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Galaxy -> Development's lane tags

    {
      id: "6552011d-ae06-482d-98fc-abf49f220012",
      name: "To do",
      description: "Tickets in sprint waiting to be picked up",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "1720a990-f63a-4ba3-93f8-a1a0205be9d6",   // Development
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "631868e9-2cdb-4676-85ec-e8c26a770b47",
      name: "In progress",
      description: "In progress - developing",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "1720a990-f63a-4ba3-93f8-a1a0205be9d6",   // Development
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "aa05b7f8-3117-4611-a73c-c3f62a4950d3",
      name: "Code review",
      description: "Under code review",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "1720a990-f63a-4ba3-93f8-a1a0205be9d6",   // Development
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "05caba71-c8dd-4c57-9883-d5ae5632198c",
      name: "Acceptance testing",
      description: "Acceptance testing",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "1720a990-f63a-4ba3-93f8-a1a0205be9d6",   // Development
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },
    {
      id: "17dde46d-819c-40f5-856e-b94de6d9c40c",
      name: "Done",
      description: "Finished",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "1720a990-f63a-4ba3-93f8-a1a0205be9d6",   // Development
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Galaxy -> Overview's board tag

    {
      id: "7eb5f80b-5ae3-4532-8d12-109f5072afda",
      name: "Galaxy Overview",
      description: "Galaxy overview board",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      type: "board",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    // Galaxy -> Overview's lane tags
    // no tags required as board pointing to different board"s tags

    // Galaxy -> Backlog's lane tags

    {
      id: "75fe8bc5-4633-4450-a405-f9e318ec9615",
      name: "Galaxy Backlog",
      description: "Galaxy backlog board",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      type: "board",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    },

    {
      id: "bdc24220-4bb1-4a9c-bdfa-9752992227e7",
      name: "Waiting",
      description: "Waiting to be added to sprint",
      projectId: "460ad418-ac09-44bb-9941-a2a645032c01", // Galaxy
      boardId: "2f1e4feb-dc31-40f9-9224-a189a23ab0a0",   // Backlog
      type: "lane",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    }
  ];

  return r.table("Tag").insert(tags).run();
});
