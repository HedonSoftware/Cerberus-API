
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("ApiAccount").then(function () {

  var resources = [
    {
      id: "65912af6-36a6-4921-8147-2ce5482b42d0",
      name: "apiAccount",
      description: "ApiAccount",
      status: "active",
      createdAt: new Date("2015-08-22T16:31:54.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

}).then(function () {

  var apiAccounts = [
    {
      id: "83e5787c-dbe5-4bc5-9842-ad0035385fde",
      authKey: "ijsoifjdsoisjfoisdjfijsoifjdsoisjfoisdjf",
      secret:  "ajifsojifsiojfssdaf3ajifsojifsiojfssdaf3",
      username: "KogoWebsite",
      status: "active",
      roleId: "53c278d9-e3e7-4a20-b717-9e86efbe123a",
      createdAt: new Date("2015-08-22T21:36:04.000Z"),
      updatedAt: null
    },
    {
      id: "12297e34-65c6-45ac-b1b3-16d5372c8cd5",
      authKey: "kjoisdfjdosijfsoij34kjoisdfjdosijfsoij34",
      secret:  "fsdaf34oijsdjifosdisfsdaf34oijsdjifosdis",
      username: "KogoAdminWebsite",
      status: "active",
      roleId: "91bf4f95-60a6-4d58-aa72-b49bcb8c5e0b",
      createdAt: new Date("2015-08-22T05:43:08.000Z"),
      updatedAt: null
    }
  ];

  return r.table("ApiAccount").insert(apiAccounts).run();

});
