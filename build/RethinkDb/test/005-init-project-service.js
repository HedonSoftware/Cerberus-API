
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("Project").then(function () {

  var resources = [
    {
      id: "cf5061be-b52c-445c-a45b-8f83e2183600",
      name: "project",
      description: "Project",
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

}).then(function () {

  var projects = [
    {
      id: "5de514c2-49f6-429e-a478-36bdc206cab4",
      name: "Universe",
      code: "UNI",
      description: "Universe is project separated from Galaxy ...",
      boards: {
        "0a193918-ebe1-4ca1-8450-7924c0321868": {
          id: "0a193918-ebe1-4ca1-8450-7924c0321868",
          name: "Frontend",
          description: "Frontend team's board",
          type: "action",
          tags: [
            "14fbcf19-de75-4cbc-8111-2b26c220a2d8"
          ],
          sprints: {
            "fbb6f47e-261c-4b55-95d2-92374595007f": {
              id: "fbb6f47e-261c-4b55-95d2-92374595007f",
              name: "Sprint 0.0.1",
              description: "Sprint 0.0.1",
              startDate: new Date("2014-10-02T00:00:00.000Z"),
              endDate: new Date("2014-10-16T00:00:00.000Z"),
              tags: [
                "6fdbebe4-4f1b-4304-86a5-e67fc6333c49"
              ],
              status: "active",
              createdAt: new Date("2015-08-23T16:13:51.000Z"),
              updatedAt: null
            },
            "3f2213f2-0c04-44bf-907a-9f167404adff": {
              id: "3f2213f2-0c04-44bf-907a-9f167404adff",
              name: "Sprint 0.0.2",
              description: "Sprint 0.0.2",
              startDate: new Date("2014-10-17T00:00:00.000Z"),
              endDate: new Date("2014-10-31T00:00:00.000Z"),
              tags: [
                "e53be40b-b712-4a5a-8b99-c5b400b9773e"
              ],
              status: "active",
              createdAt: new Date("2015-08-23T16:13:51.000Z"),
              updatedAt: null
            }
          },
          lanes: {
            "9fffc983-16fd-4e73-aac9-80f493a4e169": {
              id: "9fffc983-16fd-4e73-aac9-80f493a4e169",
              name: "To do",
              description: "To be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "da2f64bd-1d70-4415-a232-95b2f35366ba"
              ]
            },
            "1fa796a6-050b-4f45-9cec-ab230011c1b5": {
              id: "1fa796a6-050b-4f45-9cec-ab230011c1b5",
              name: "In progress",
              description: "Started",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "e12ba8d6-b125-4692-94bb-616f7fbc8ab5"
              ]
            },
            "3cb4f481-33bd-4aeb-affc-b13d7244e0af": {
              id: "3cb4f481-33bd-4aeb-affc-b13d7244e0af",
              name: "Done",
              description: "Ended",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "261cfeb0-8c47-48ac-acfa-6a59a140235e"
              ]
            }
          },
          status: "active"
        },
        "43469ba8-b072-4bce-83d2-661ef5164972": {
          id: "43469ba8-b072-4bce-83d2-661ef5164972",
          name: "API",
          description: "API team's board",
          type: "action",
          tags: [
            "4b2b9a53-a5db-4490-b005-3e68cef3ba98"
          ],
          sprints: {
            "a239fb8d-a632-4f6e-91b1-16cbaa519800": {
              id: "a239fb8d-a632-4f6e-91b1-16cbaa519800",
              name: "Sprint 1.0.0",
              description: "First post-release sprint",
              startDate: new Date("2014-10-05T00:00:00.000Z"),
              endDate: new Date("2014-10-19T00:00:00.000Z"),
              tags: [
                "8e4fe830-283b-4fea-994c-b421250bfa00"
              ],
              status: "active",
              createdAt: new Date("2015-08-23T16:13:51.000Z"),
              updatedAt: null
            },
            "72db1cef-4e21-4946-a16e-1e0690d69dc7": {
              id: "72db1cef-4e21-4946-a16e-1e0690d69dc7",
              name: "Sprint 1.0.1",
              description: "Second post-release sprint",
              startDate: new Date("2014-10-20T00:00:00.000Z"),
              endDate: new Date("2014-11-03T00:00:00.000Z"),
              tags: [
                "71f1b0f1-cc82-4056-b349-c3422ed814ea"
              ],
              status: "active",
              createdAt: new Date("2015-08-23T16:13:51.000Z"),
              updatedAt: null
            }
          },
          lanes: {
            "d088bd68-d010-4253-b651-cec707bce0ee": {
              id: "d088bd68-d010-4253-b651-cec707bce0ee",
              name: "To do",
              description: "To be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "705b5f6a-2448-4309-98ed-47acc0806886"
              ]
            },
            "3901bcd1-a614-45ca-b2fb-b17431fc4222": {
              id: "3901bcd1-a614-45ca-b2fb-b17431fc4222",
              name: "Development",
              description: "Developing right now",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "d91a31c9-aa3b-458d-9143-9fdfaf088181"
              ]
            },
            "8063841b-b875-4afd-a8c5-589d0bd278cb": {
              id: "8063841b-b875-4afd-a8c5-589d0bd278cb",
              name: "Review",
              description: "Code review",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "cc1ed2c3-a2f9-4d93-9e93-859140f0e6e7"
              ]
            },
            "c00f7943-2a3b-4eb3-9408-4af7d4274914": {
              id: "c00f7943-2a3b-4eb3-9408-4af7d4274914",
              name: "Done",
              description: "Ended",
              sequenceNumber: 4,
              status: "active",
              tags: [
                "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8"
              ]
            }
          },
          status: "active"
        },
        "408774d5-207b-4523-aa35-74b4a72be6d2": {
          id: "408774d5-207b-4523-aa35-74b4a72be6d2",
          name: "Backlog",
          description: "Tickets incubator",
          type: "backlog",
          tags: [
            "de2de2b3-d5f5-4668-aab4-e05acaeae8a4"
          ],
          lanes: {
            "df86b40c-f31b-4c23-9da7-f73506141510": {
              id: "df86b40c-f31b-4c23-9da7-f73506141510",
              name: "Must",
              description: "Must be done in next sprint",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "bdc24220-4bb1-4a9c-bdfa-9752992227e7"
              ]
            },
            "59a1c5e1-0513-4ba0-b480-1135c3ef00af": {
              id: "59a1c5e1-0513-4ba0-b480-1135c3ef00af",
              name: "Should",
              description: "Should be done in next sprint",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "db4958e1-ef81-4213-a68a-97f2ba77d7cc"
              ]
            },
            "c34efa75-b9ab-4bfa-ac0d-3514886057f7": {
              id: "c34efa75-b9ab-4bfa-ac0d-3514886057f7",
              name: "Could",
              description: "Could be done in next sprint",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "0397c137-94ac-4a1e-8b98-83b603f15a29"
              ]
            },
            "f9789083-742e-4211-aaa6-54c0c2183ac2": {
              id: "f9789083-742e-4211-aaa6-54c0c2183ac2",
              name: "Won't",
              description: "Won't be done in next sprint",
              sequenceNumber: 4,
              status: "active",
              tags: [
                "9917c08c-ef33-4ab1-9a2a-8d7a4ba69a33"
              ]
            }
          },
          status: "active"
        }
      },
      status: "active",
      createdAt: new Date("2015-08-22T15:41:10.000Z"),
      updatedAt: null
    },
    {
      id: "460ad418-ac09-44bb-9941-a2a645032c01",
      name: "Galaxy",
      code: "GAL",
      description: "Galaxy is a massive project ...",
      boards: {
        "b6f65cd5-8deb-4c86-9c32-58fbd0f2b1fa": {
          id: "b6f65cd5-8deb-4c86-9c32-58fbd0f2b1fa",
          name: "Design",
          description: "Graphic design team's board",
          type: "action",
          tags: [
            "e7981571-f6f9-469f-bf97-7dbbad19b439"
          ],
          lanes: {
            "d4b39f85-f646-4963-8fb1-1a29e0af1fa0": {
              id: "d4b39f85-f646-4963-8fb1-1a29e0af1fa0",
              name: "To do",
              description: "To be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "05165da4-3950-48d9-bffb-5aff67edb47b"
              ]
            },
            "d27b4311-f172-49df-a567-3934a12357e3": {
              id: "d27b4311-f172-49df-a567-3934a12357e3",
              name: "In progress",
              description: "Designing right now",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "87d87f31-ce2b-42a9-8155-b923c548f922"
              ]
            },
            "32e9b31c-a6f4-4e52-a1c1-cc5d1af5fec7": {
              id: "32e9b31c-a6f4-4e52-a1c1-cc5d1af5fec7",
              name: "Done",
              description: "Ended",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "06580431-9b48-4c79-baf6-07259f3975a7"
              ]
            }
          },
          status: "active"
        },
        "1720a990-f63a-4ba3-93f8-a1a0205be9d6": {
          id: "1720a990-f63a-4ba3-93f8-a1a0205be9d6",
          name: "Development",
          description: "Development team's board",
          type: "action",
          tags: [
            "4b5dcd90-b07b-4044-8572-0679a937cc06"
          ],
          lanes: {
            "392f6bac-a590-49fd-8eb0-ab1c621690dc": {
              id: "392f6bac-a590-49fd-8eb0-ab1c621690dc",
              name: "To do",
              description: "To be developed",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "6552011d-ae06-482d-98fc-abf49f220012"
              ]
            },
            "c6579da1-8157-4d9f-a18e-d2b073471b37": {
              id: "c6579da1-8157-4d9f-a18e-d2b073471b37",
              name: "In progress",
              description: "Developing..",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "631868e9-2cdb-4676-85ec-e8c26a770b47"
              ]
            },
            "2f832805-7b63-49c7-aca7-57ffec01b77a": {
              id: "2f832805-7b63-49c7-aca7-57ffec01b77a",
              name: "Code review",
              description: "Peer review",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "aa05b7f8-3117-4611-a73c-c3f62a4950d3"
              ]
            },
            "25096f7f-ca27-4d52-bf4a-b32aecd92f6f": {
              id: "25096f7f-ca27-4d52-bf4a-b32aecd92f6f",
              name: "Acceptance testing",
              description: "User acceptance testing",
              sequenceNumber: 4,
              status: "active",
              tags: [
                "05caba71-c8dd-4c57-9883-d5ae5632198c"
              ]
            },
            "d79ef673-65eb-4678-8b40-c2cf91b1f34c": {
              id: "d79ef673-65eb-4678-8b40-c2cf91b1f34c",
              name: "Done",
              description: "Finished/Approved",
              sequenceNumber: 5,
              status: "active",
              tags: [
                "17dde46d-819c-40f5-856e-b94de6d9c40c"
              ]
            }
          },
          status: "active"
        },
        "156201b4-5ae6-428d-8baf-77ab4729dedd": {
          id: "156201b4-5ae6-428d-8baf-77ab4729dedd",
          name: "Overview",
          description: "Management team's overview board",
          type: "overview",
          tags: [
            "7eb5f80b-5ae3-4532-8d12-109f5072afda"
          ],
          lanes: {
            "cd1eb25c-33b5-4fc1-a2e9-19e4ff7602a4": {
              id: "cd1eb25c-33b5-4fc1-a2e9-19e4ff7602a4",
              name: "Waiting",
              description: "Waiting to be started",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "05165da4-3950-48d9-bffb-5aff67edb47b", // design - to do
                "6552011d-ae06-482d-98fc-abf49f220012"  // development - to do
              ]
            },
            "a5511ccd-376c-4720-8ace-d167138befd8": {
              id: "a5511ccd-376c-4720-8ace-d167138befd8",
              name: "Progressing",
              description: "Progressing",
              sequenceNumber: 2,
              status: "active",
              tags: [
                "87d87f31-ce2b-42a9-8155-b923c548f922", // design -> in progress
                "631868e9-2cdb-4676-85ec-e8c26a770b47", // development -> in progress
                "aa05b7f8-3117-4611-a73c-c3f62a4950d3", // development -> code review
                "05caba71-c8dd-4c57-9883-d5ae5632198c"  // development -> acceptance testing
              ]
            },
            "1377ba46-5f7e-47c0-830e-e8211759b2ef": {
              id: "1377ba46-5f7e-47c0-830e-e8211759b2ef",
              name: "Finished",
              description: "Finished",
              sequenceNumber: 3,
              status: "active",
              tags: [
                "06580431-9b48-4c79-baf6-07259f3975a7", // design - done
                "17dde46d-819c-40f5-856e-b94de6d9c40c"  // development - done
              ]
            }
          },
          status: "active"
        },
        "2f1e4feb-dc31-40f9-9224-a189a23ab0a0": {
          id: "2f1e4feb-dc31-40f9-9224-a189a23ab0a0",
          name: "Backlog",
          description: "Ideas incubator",
          type: "backlog",
          tags: [
            "75fe8bc5-4633-4450-a405-f9e318ec9615"
          ],
          lanes: {
            "5664df98-c298-4b20-a355-a1618be91335": {
              id: "5664df98-c298-4b20-a355-a1618be91335",
              name: "Waiting",
              description: "Waiting to be added to sprint",
              sequenceNumber: 1,
              status: "active",
              tags: [
                "bdc24220-4bb1-4a9c-bdfa-9752992227e7"
              ]
            }
          },
          status: "active"
        }
      },
      status: "active",
      createdAt: new Date("2015-08-22T05:29:22.000Z"),
      updatedAt: null
    },
    {
      id: "7ab74e35-d06d-48cf-a320-fe1efb5b4275",
      name: "Atom",
      code: "ATM",
      description: "Atom is a chemical project ...",
      boards: {},
      status: "active",
      createdAt: new Date("2015-08-22T09:42:43.000Z"),
      updatedAt: null
    }
  ];

  return r.table("Project").insert(projects).run();

});
