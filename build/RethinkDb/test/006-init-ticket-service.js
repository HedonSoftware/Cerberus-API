
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("Ticket").then(function () {

  var resources = [
    {
      id: "8073805a-3feb-4877-868a-73e94e5fa68a",
      name: "ticket",
      description: "Ticket",
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

}).then(function () {

  var tickets = [
    {
      id: "f65a5fe6-371a-4216-884a-ab61047ab3db",
      name: "Install Grunt Watch",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-watch --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-watch');</pre><p></p>",
      storyPoints: 8,
      creatorId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      comments: {
        "ff0931ae-5e8d-4513-a43e-88ccdd0c88ec": {
          id: "ff0931ae-5e8d-4513-a43e-88ccdd0c88ec",
          text: "Dependency on ...",
          userId: "76039cc1-96eb-4721-81b3-211b6979d018",
          status: "active",
          createdAt: new Date("2015-08-22T21:34:32.000Z"),
          updatedAt: null
        }
      },
      tags: [
        "da2f64bd-1d70-4415-a232-95b2f35366ba", // Universe -> Frontend -> To do
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "c9a67fa1-9903-4bb7-a584-31ea0abdcdb6",
      name: "Validate files with JSHint",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-jshint --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-jshint');</pre>",
      storyPoints: 13,
      creatorId: "4c374646-ff38-4a8b-adf6-0bf0e3723c75",
      assigneeId: "ede8e906-03f4-40e8-9b8e-46c1aad72de6",
      comments: {
        "c651d7c8-c48e-4025-88a6-7365e3ab958e": {
          text: "Already fixed by ...",
          userId: "9f894edc-23cb-4704-aa65-39e90d3a078a",
          status: "active",
          createdAt: new Date("2015-08-22T21:34:32.000Z"),
          updatedAt: null
        }
      },
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "382f0878-29fd-4161-8231-fc931741e49f",
      name: "Minify files with UglifyJS",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-uglify --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-uglify');</pre>",
      storyPoints: 5,
      creatorId: "a5390c1c-eb83-4577-9446-cc1653102ee7",
      assigneeId: "761a6b5c-d5e9-484f-923f-079e6780bf65",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "647c3c3c-309d-476a-8891-c2d569ba14e1",
      name: "Clean files and folders",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-clean --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-clean');</pre>",
      storyPoints: 30,
      creatorId: "5510260a-70d0-4ff6-8ba6-b1fc0f019634",
      assigneeId: "b877bb53-86b9-40a3-849c-2b624ebb5dce",
      comments: {
        "d83fc8d2-076b-4b16-a4f3-632c85418c1b": {
          id: "d83fc8d2-076b-4b16-a4f3-632c85418c1b",
          text: "Sorry wont do it...",
          userId: "9f894edc-23cb-4704-aa65-39e90d3a078a",
          status: "active",
          createdAt: new Date("2015-08-22T21:34:32.000Z"),
          updatedAt: null
        }
      },
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "61ad71eb-0ca2-49af-b371-c16599207bbc",
      name: "Concatenate files",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-concat --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-concat');</pre>",
      storyPoints: 3,
      creatorId: "d7885ec6-ece3-427a-b15c-c114d27d2f82",
      assigneeId: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
      comments: {
        "7001f385-bcc7-412b-9629-648fa977b22a": {
          id: "7001f385-bcc7-412b-9629-648fa977b22a",
          text: "Should be marked as duplicate ...",
          userId: "b808972a-ec28-4d69-a194-d1e05c5ff0e8",
          status: "active",
          createdAt: new Date("2015-08-22T21:34:32.000Z"),
          updatedAt: null
        },
        "da517538-c7ae-4bd5-9242-71b2a9b98828": {
          id: "da517538-c7ae-4bd5-9242-71b2a9b98828",
          text: "Full regression tests are required ...",
          userId: "76039cc1-96eb-4721-81b3-211b6979d018",
          status: "active",
          createdAt: new Date("2015-08-22T21:34:32.000Z"),
          updatedAt: null
        }
      },
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "fbf750db-8d63-446e-ba8b-603f2d40c35d",
      name: "Compress CSS files",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-cssmin --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-cssmin');</pre>",
      storyPoints: 13,
      creatorId: "4c374646-ff38-4a8b-adf6-0bf0e3723c75",
      assigneeId: "1d58b82e-eeec-4065-9386-5309a6aede48",
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "263e40f6-56a4-484b-b28a-ad87a6ebd393",
      name: "Compile LESS files to CSS",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-less --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-less');</pre>",
      storyPoints: 13,
      creatorId: "e4aeff0c-eddb-4ba0-b518-6ae7061b090e",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "da2f64bd-1d70-4415-a232-95b2f35366ba", // Universe -> Frontend -> To do
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "8b04dc1d-f2b0-4b3c-b884-7b5ecd361a35",
      name: "Allow to run grunt tasks concurrently",
      description: "<p>Running slow tasks like Coffee and Sass concurrently can potentially improve your build time significantly. This task is also useful if you need to run multiple blocking tasks like nodemon and watch at once.</p><p><br/></p><h4><b>Install</b></h4><pre>npm install --save-dev grunt-concurrent</pre><pre>require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks&#10;&#10;grunt.initConfig({&#10;    concurrent: {&#10;        target1: ['coffee', 'sass'],&#10;        target2: ['jshint', 'mocha']&#10;    }&#10;});&#10;&#10;grunt.registerTask('default', ['concurrent:target1', 'concurrent:target2']);</pre>",
      storyPoints: 8,
      creatorId: "2b8918c0-e281-4d90-a165-ff40f96b5de9",
      assigneeId: "32a00583-3ae2-40ef-88c4-590a2617c7e2",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date(),
      updatedAt: null
    },
    {
      id: "4b9c8c26-9962-4351-b7f8-2e6e333c9ff5",
      name: "Minify images",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-imagemin --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-imagemin');</pre>",
      storyPoints: 20,
      creatorId: "dbfdc23a-d442-4970-a214-ff14f0d1875c",
      assigneeId: "7b13132f-6bc2-4841-b223-e726c73a5013",
      workRecords: {
        "52cf7be4-bfde-4f4e-96f3-4b9cf83e654f": {
          id: "52cf7be4-bfde-4f4e-96f3-4b9cf83e654f",
          name: "development",
          description: "Adding minify images to build script",
          startDate: new Date("2015-07-15T00:00:00.000Z"),
          time: 120,
          status: "active",
          userId: "8e6002c6-e5ef-4552-8bfe-f785a810767a"
        }
      },
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "de1b30ca-c0aa-4a0a-8b66-22f38706deb1",
      name: "Auto-replace assets from HTML",
      description: "<p>Replace references from non-optimized scripts, stylesheets and other assets to their optimized version within a set of HTML files (or any templates/views).</p><h4><b><br/></b></h4><h4><b>Getting Started</b></h4><p><br/></p><p>If you haven't used grunt before, be sure to check out the Getting Started guide, as it explains how to create a gruntfile as well as install and use grunt plugins. Once you're familiar with that process, install this plugin with this command:</p><pre>npm install grunt-usemin --save-dev</pre><h4><b><br/></b></h4><h4><b>Tasks</b></h4><p><br/></p><p>usemin replaces the references of scripts, stylesheets and other assets within HTML files dynamically with optimized versions of them. To do this usemin exports 2 built-in tasks called useminPrepare and usemin and utilizes a couple of other Grunt plugins for the optimization process. usemin does this by generating the subtasks for these Grunt plugins dynamically.</p><p>The built-in tasks of usemin:</p><ul><li>useminPrepare prepares the configuration to transform specific blocks in the scrutinized file into a single line, targeting an optimized version of the files. This is done by generating subtasks called generated for every optimization steps handled by the Grunt plugins listed below.</li></ul><ul><li>usemin replaces the blocks by the file they reference, and replaces all references to assets by their revisioned version if it is found on the disk. This target modifies the files it is working on.</li></ul><p>Grunt plugins which usemin can use to optimize files:</p><ul><li>concat concatenates files (usually JS or CSS).</li></ul><ul><li>uglify minifies JS files.</li></ul><ul><li>cssmin minifies CSS files.</li></ul><ul><li>filerev revisions static assets through a file content hash.</li></ul><p>Important: You still need to manually install and load these dependencies.</p>",
      storyPoints: 20,
      creatorId: "e177d163-b3f3-4d0f-af53-1954e082bb26",
      assigneeId: "612d2124-d3d0-403a-adaa-f0d9b7df0409",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "a6dfa3aa-1fc6-44db-a6e3-23f2479ac2a4",
      name: "Add vendor-prefixed CSS properties",
      description: "<p>Autoprefixer parses CSS and adds vendor-prefixed CSS properties using the Can I Use database</p><p><br/></p><h4><b>Getting Started</b></h4><h4><br/></h4><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-autoprefixer --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-autoprefixer');</pre>",
      storyPoints: 5,
      creatorId: "3770ec25-7919-474a-b517-594cde46a506",
      assigneeId: "562b98b3-79dc-40d6-91c3-908b908bd5be",
      tags: [
        "e12ba8d6-b125-4692-94bb-616f7fbc8ab5", // Universe -> Frontend -> In progress
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "299d4254-da73-4eb6-a5ff-78cf082e811b",
      name: "Minify HTML",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-htmlmin --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-htmlmin');</pre>",
      storyPoints: 21,
      creatorId: "ed2243d8-faf8-45e4-a80a-d39ff17803de",
      assigneeId: "ede8e906-03f4-40e8-9b8e-46c1aad72de6",
      tags: [
        "cc1ed2c3-a2f9-4d93-9e93-859140f0e6e7", // Universe -> Api -> Review
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "c16c32d0-3a2a-4428-b5b1-2df00ad5e39c",
      name: "Compile Sass to CSS using Compass",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt &gt;=0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-compass --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-compass');</pre>",
      storyPoints: 5,
      creatorId: "9f894edc-23cb-4704-aa65-39e90d3a078a",
      assigneeId: "ab366f1e-d89b-496a-a80e-f6960552f04c",
      tags: [
        "e12ba8d6-b125-4692-94bb-616f7fbc8ab5", // Universe -> Frontend -> In progress
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "aa9b0519-0161-46d4-adb3-69964db6c34c",
      name: "Compile CoffeeScript files to JavaScript",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-coffee --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-coffee');</pre>",
      storyPoints: 3,
      creatorId: "5510260a-70d0-4ff6-8ba6-b1fc0f019634",
      assigneeId: "f235b9a0-0db9-4fca-a964-97d5795a805a",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "622881f8-fe02-4a57-9713-8a11696c5065",
      name: "Minify SVG",
      description: "<h5>Minify SVG using SVGO</h5><h4><b><br/></b></h4><h4><b>Install</b></h4><p><br/></p><pre>$ npm install --save-dev grunt-svgmin</pre>",
      storyPoints: 5,
      creatorId: "6391f83f-06b4-4f64-89d2-e80aefeefe1d",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      workRecords: {
        "104a06d1-e0f0-4d03-b440-b1e36bae5c29": {
          id: "104a06d1-e0f0-4d03-b440-b1e36bae5c29",
          name: "development",
          description: "Adding minify SVG to build script",
          startDate: new Date("2015-07-22T00:00:00.000Z"),
          time: 120,
          status: "active",
          userId: "8e6002c6-e5ef-4552-8bfe-f785a810767a"
        }
      },
      tags: [
        "da2f64bd-1d70-4415-a232-95b2f35366ba", // Universe -> Frontend -> To do
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "20be3792-ff5c-4e29-ae0d-69e6056b9e99",
      name: "Daily running server side mocha tests",
      description: "<h4><b>Usage</b></h4><p><br/></p><p>Install next to your project's Gruntfile.js with:</p><pre>npm install grunt-mocha-test --save-dev</pre><p>Note that due to some dependencies using newer features of npm it is necessary to update npm if still using the default version that ships with node 0.8. This can be done as follows:</p><pre>npm update npm -g</pre><p>On some systems it may be necessary to run this with sudo</p>",
      storyPoints: 13,
      creatorId: "76039cc1-96eb-4721-81b3-211b6979d018",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "705b5f6a-2448-4309-98ed-47acc0806886", // Universe -> Api -> To do
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "7228a93c-a6f3-4d35-9e6c-ed8c2c276cff",
      name: "Optimize RequireJS",
      description: "<p>Optimize RequireJS projects using r.js.</p><p><br/></p><h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-requirejs --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-requirejs');</pre>",
      storyPoints: 8,
      creatorId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      assigneeId: "a533213f-214f-47da-b275-f47220096ce9",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "9003a07c-5d89-4deb-b57c-a35042f22eb7",
      name: "Lint CSS files",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><span><br/></span></p><p><span>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</span></p><pre>npm install grunt-contrib-csslint --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-csslint');</pre>",
      storyPoints: 25,
      creatorId: "23ea9aa9-cdb1-4001-b1c6-50ae31643b2b",
      assigneeId: "bb200266-2005-4831-af29-061c06bed4c4",
      workRecords: {
        "1a78127b-eb06-482f-a858-9c02fc2ae6b1": {
          id: "1a78127b-eb06-482f-a858-9c02fc2ae6b1",
          name: "development",
          description: "Adding linting CSS to build script",
          startDate: new Date("2015-07-10T00:00:00.000Z"),
          time: 10,
          status: "active",
          userId: "3aaeeb32-8c7c-47f1-90c3-b4476f19e552"
        }
      },
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "f93653eb-45e8-41ba-bde4-3691e483d0a3",
      name: "Produce responsive images",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0.</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-responsive-images --save-dev</pre><p>You also need to install either GraphicsMagick or Imagemagick CLI tools.</p><p><br/></p><p><b>Installing GraphicsMagick (Recommended)</b></p><p><br/></p><p>If you're a Mac user and have Homebrew installed, simply type:</p><pre>brew install GraphicsMagick</pre>",
      storyPoints: 13,
      creatorId: "982b4a36-2eba-4560-8a15-e6fdeed34c1f",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "705b5f6a-2448-4309-98ed-47acc0806886", // Universe -> Api -> To do
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "38808cbf-7152-4760-8704-9e0eb0bb8abc",
      name: "Run daily builds with Nodeunit unit tests.",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-nodeunit --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-nodeunit');</pre>",
      storyPoints: 13,
      creatorId: "0fcc7e7e-3c26-48b4-97ca-962bd469f5cf",
      assigneeId: "2e32a613-c8e5-43b7-97b5-292f63791ec8",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "e61c39cd-93ea-40e9-8a1e-dfeaeb375be3",
      name: "Convert AngularJS templates to JavaScript",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre><span>npm install grunt-html2js --save-dev</span><br/></pre><p>One the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-html2js');</pre>",
      storyPoints: 20,
      creatorId: "d5215bd4-6a2a-4cd2-b6d8-d5c2c933e5df",
      assigneeId: "ac9180c4-3e17-4094-9548-d75dab0f7f35",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "e2126d2e-10de-4515-a9c3-fec5cab7fbcf",
      name: "Automate bumping package version",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt.</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-bump --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-bump');</pre>",
      storyPoints: 20,
      creatorId: "4baf7c31-84a9-4fd5-9584-c401b0030f5b",
      assigneeId: "e312afc1-6073-4198-aa3e-c9aef12cb562",
      tags: [
        "e12ba8d6-b125-4692-94bb-616f7fbc8ab5", // Universe -> Frontend -> In progress
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "34674904-4377-4136-8896-19d3e6e22859",
      name: "Run Protractor tests daily",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.1 and Protractor 1.x.x</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-protractor-runner --save-dev</pre><p>This plugin will install protractor module locally as a normal dependency. Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-protractor-runner');</pre><p>If you want to use standalone selenium server, run ./node_modules/protractor/bin/webdriver-manager update to install/update the selenium server for local installed protractor.</p>",
      storyPoints: 13,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "705b5f6a-2448-4309-98ed-47acc0806886", // Universe -> Api -> To do
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "922b3a7f-ff40-4aea-a07a-2c618cbe4095",
      name: "Run QUnit unit tests in a headless PhantomJS instance",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-qunit --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-qunit');</pre>",
      storyPoints: 10,
      creatorId: "7d39bdac-8753-4036-bc64-16dab0384c06",
      assigneeId: "6788f7ed-0296-432c-ba98-688e093afdc6",
      tags: [
        "e12ba8d6-b125-4692-94bb-616f7fbc8ab5", // Universe -> Frontend -> In progress
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "d84b409c-35a6-4063-9879-b4209d05cfea",
      name: "Precompile Underscore templates to JST file",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-jst --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-jst');</pre>",
      storyPoints: 5,
      creatorId: "c2ba76b7-4aee-48f9-ba40-6b926f40335c",
      assigneeId: "6788f7ed-0296-432c-ba98-688e093afdc6",
      tags: [
        "d91a31c9-aa3b-458d-9143-9fdfaf088181", // Universe -> Api -> Development
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "47d5298e-9975-4f88-8309-2debef8cf318",
      name: "Check JavaScript Code Style",
      description: "<h4><b>Getting started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.2</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-jscs --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks(&#34;grunt-jscs&#34;);</pre>",
      storyPoints: 10,
      creatorId: "7b13132f-6bc2-4841-b223-e726c73a5013",
      assigneeId: "5684f405-269a-4735-adad-10a91e2b3fae",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "40941ca2-a602-4b27-b4b3-dbed2a1f5db4",
      name: "Compile Jade templates",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-jade --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-jade');</pre>",
      storyPoints: 8,
      creatorId: "a1bb1a57-183b-4b82-94fe-d0a60b6ae444",
      assigneeId: "e37134f4-ef5c-4a3e-883d-7ed7e23dc294",
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "bf6f05b6-0f8f-4725-a9c4-aedb9f32c228",
      name: "Precompile Handlebars templates to JST file",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-handlebars --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-handlebars');</pre>",
      storyPoints: 14,
      creatorId: "3a028719-610e-4b01-9d33-fc6afa52100f",
      assigneeId: "3aaeeb32-8c7c-47f1-90c3-b4476f19e552",
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "5116e9e5-ca17-4ca5-89db-21e9da68d6ee",
      name: "Optimize Bower packages installation",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>If you haven't used grunt before, be sure to check out the Getting Started guide.</p><p><br/></p><p>Please note, this plugin works only with grunt 0.4+. If you are using grunt 0.3.x then consider an upgrade to 0.4.</p><p><br/></p><p>From the same directory as your project's Gruntfile and package.json, install this plugin with the following command:</p><pre>npm install grunt-bower-task --save-dev</pre><p>Once that's done, add this line to your project's Gruntfile:</p><pre>grunt.loadNpmTasks('grunt-bower-task');</pre><p>If the plugin has been installed correctly, running grunt --help at the command line should list the newly-installed plugin's task or tasks. In addition, the plugin should be listed in package.json as a devDependency, which ensures that it will be installed whenever the npm install command is run.</p>",
      storyPoints: 20,
      creatorId: "3aaeeb32-8c7c-47f1-90c3-b4476f19e552",
      assigneeId: "562b98b3-79dc-40d6-91c3-908b908bd5be",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "8a36af87-7b58-4d0d-b942-b34ae823b205",
      name: "Compile Stylus files to CSS",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-stylus --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-stylus');</pre>",
      storyPoints: 8,
      creatorId: "cac8ff1a-7d87-41bb-b753-1047b95527fb",
      assigneeId: "761a6b5c-d5e9-484f-923f-079e6780bf65",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "62e17523-f215-4884-b194-4e65e4a2efc0",
      name: "Inject Bower dependencies into HTML",
      description: "<h4><b>What is this?</b></h4><p><br/></p><p>Grunt is great.</p><p><span>Bower is great.</span><br/></p><p><br/></p><p>And now they work great together.</p><p><br/></p><p>grunt-bower-install is a Grunt plug-in, which finds your components and injects them directly into the HTML file you specify.</p><p><br/></p><p>Whether you're already using Bower and Grunt, or new to both, grunt-bower-install will be easy to plug in, as you will see in the steps below.</p><p><br/></p><p>do note: Bower is still a young little birdy, so things are changing rapidly. Authors of Bower components must follow certain conventions and best practices in order for this plug-in to be as accurate as possible. It's not a perfect world out there, so needless to say, some Bower components may not work as well as others.</p><p><br/></p><h4><b>Getting Started</b></h4><p><br/></p><p>*If you are new to Grunt, you will find a lot of answers to your questions in their getting started guide.</p><p><br/></p><p>To install the module:</p><pre>npm install --save-dev grunt-bower-install</pre><p>Include the task in your Gruntfile:</p><pre>grunt.loadNpmTasks('grunt-bower-install');</pre>",
      storyPoints: 13,
      creatorId: "ed2243d8-faf8-45e4-a80a-d39ff17803de",
      assigneeId: "ede8e906-03f4-40e8-9b8e-46c1aad72de6",
      tags: [
        "e12ba8d6-b125-4692-94bb-616f7fbc8ab5", // Universe -> Frontend -> In progress
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "3c67683b-d292-4dde-8886-f7b454f41896",
      name: "Lint CoffeeScript",
      description: "<h4><b>Installation</b></h4><p><br/></p><p>Install npm package, next to your project's Gruntfile.js file:</p><pre>npm install grunt-coffeelint</pre><p>Add this line to your project's Gruntfile.js:</p><pre>grunt.loadNpmTasks('grunt-coffeelint');</pre>",
      storyPoints: 8,
      creatorId: "1d58b82e-eeec-4065-9386-5309a6aede48",
      assigneeId: "3aaeeb32-8c7c-47f1-90c3-b4476f19e552",
      workRecords: {
        "2e0a4738-749b-445c-b6d4-84c3837e4462": {
          id: "2e0a4738-749b-445c-b6d4-84c3837e4462",
          name: "development",
          description: "Linting new functionality",
          startDate: new Date("2015-07-10T00:00:00.000Z"),
          time: 60,
          status: "active",
          userId: "3aaeeb32-8c7c-47f1-90c3-b4476f19e552"
        }
      },
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "0e458f77-8756-480f-ad48-22cf8a302d9d",
      name: "Provide a http proxy as middleware for the grunt-contrib-connect plugin",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>NOTE: This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-connect-proxy --save-dev</pre><p>One the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-connect-proxy');</pre>",
      storyPoints: 10,
      creatorId: "5684f405-269a-4735-adad-10a91e2b3fae",
      assigneeId: "a533213f-214f-47da-b275-f47220096ce9",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "604461a4-0475-4ca2-9d86-8df73339c727",
      name: "Integrate the generation of comments based documentation into your Grunt build",
      description: "<h4><b>To your attention</b></h4><p><br/></p><p>The grunt team ask me to change the plugin name into NPM. The grunt-contrib namespace is now reserved to the tasks developed by the Grunt Team. I'll in a first time, deprecate the module in NPM and then update the name to grunt-jsdoc. You'll have to upgrade your package.json once the plugin will be removed from NPM.</p><p><br/></p><h4><b>Install</b></h4><p><br/></p><p>You need grunt &gt;= 0.4 as well as node and npm installed and running on your system.</p><p><br/></p><p>You also need java installed and available in your PATH.</p><p><br/></p><p>Install this grunt plugin next to your project's Gruntfile.js with:</p><pre>npm install grunt-jsdoc --save-dev</pre><h4><b>jsdoc3 3.3.0</b></h4><p><br/></p><p>The jsdoc3 team is working on the 3.3.0 version that works on node.js and doesn't need Rhino (Java) anymore. This version is not yet stable (flagged as alpha). If you want this plugin to use this version, you can install the beta tag of this grunt plugin (branch 0.6.x).</p><pre>npm install grunt-jsdoc@beta --save-dev</pre>",
      storyPoints: 13,
      creatorId: "cf02437a-380a-4d83-b258-e3dcdc9e408a",
      assigneeId: "dbfdc23a-d442-4970-a214-ff14f0d1875c",
      tags: [
        "d91a31c9-aa3b-458d-9143-9fdfaf088181", // Universe -> Api -> Development
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "68aa5599-2896-4af0-b1d8-5c0f650f66d6",
      name: "Validate JSON files on build",
      description: "<h4><span><b>Install</b></span><br/></h4><p><br/></p><pre>npm install grunt-jsonlint --save-dev</pre><p><br/></p><p>NOTE: Requires grunt 0.4</p>",
      storyPoints: 10,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "5758d0bf-81b3-4152-9db0-32de57f09812",
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "db534b44-01d6-41dd-92ee-78f731be1be6",
      name: "Run tests via Sauce Labs' Cloudified Browsers",
      description: "<h4><b>About the tool</b></h4><p><br/></p><p>The grunt-contrib-qunit task runs QUnit based test suites on PhantomJS. The saucelabs-qunit task is very similar but runs the test suites on the cloudified browser environment provided by Sauce Labs. This ensures that subject of the test runs across different browser environment. The task also uses Sauce Connect to establish a tunnel between Sauce Labs browsers and the machine running Grunt to load local pages. This is typically useful for testing pages on localhost that are not publicly accessible on the internet. The saucelabs-jasmine runs Jasmine tests in the Sauce Labs browser. The saucelabs-jasmine task requires jasmine-1.3.0. There are also saucelabs-mocha and saucelabs-yui tasks that let you run your Mocha and YUI tests on Sauce Labs cloudified browser environment.</p><p><br/></p><h4><b>Usage</b></h4><p><br/></p><p>This task is available as a node package and can be installed as npm install grunt-saucelabs. It can also be included as a devDependency in package.json in your node project.</p><p><br/></p><p>To use the task in grunt.js, load the npmTask.</p><pre>grunt.loadNpmTasks('grunt-saucelabs');</pre>",
      storyPoints: 5,
      creatorId: "c2ba76b7-4aee-48f9-ba40-6b926f40335c",
      assigneeId: "1950c7e7-8019-4e8b-976f-06c3ffd8671b",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "52f2d884-f268-4933-a36e-20cb960cedd8",
      name: "Compile YUIDoc Documentation",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-contrib-yuidoc --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-contrib-yuidoc');</pre>",
      storyPoints: 3,
      creatorId: "737af73c-d1b6-4215-900d-8e0ec820e0c9",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "da2f64bd-1d70-4415-a232-95b2f35366ba", // Universe -> Frontend -> To do
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "a9fbc537-4c67-49e1-8cc2-a5f7344ef078",
      name: "Inspect node stack using node-inspector",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>If you haven't used grunt before, be sure to check out the Getting Started guide, as it explains how to create a gruntfile as well as install and use grunt plugins. Once you're familiar with that process, install this plugin with this command:</p><pre>npm install grunt-node-inspector --save-dev</pre><p>Then add this line to your project's Gruntfile.js gruntfile:</p><pre>grunt.loadNpmTasks('grunt-node-inspector');</pre>",
      storyPoints: 5,
      creatorId: "a01f6dcc-87d4-41c3-b120-047c367edac9",
      assigneeId: "8e6002c6-e5ef-4552-8bfe-f785a810767a",
      tags: [
        "cc1ed2c3-a2f9-4d93-9e93-859140f0e6e7", // Universe -> Api -> Review
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "ad686f50-f9e4-4530-ad1f-656341dfb0a7",
      name: "Start (and supervise) an Express.js web server using grunt.js",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-express --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-express');</pre>",
      storyPoints: 8,
      creatorId: "612d2124-d3d0-403a-adaa-f0d9b7df0409",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "705b5f6a-2448-4309-98ed-47acc0806886", // Universe -> Api -> To do
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "3545ce3e-584e-4708-9ef4-b9677dc017b2",
      name: "Evaluate code maintainability using Halstead and Cyclomatic metrics",
      description: "<h4><b>Usage</b></h4><p><br/></p><pre>npm install grunt-complexity --save</pre>",
      storyPoints: 3,
      creatorId: "e4b3d9ff-f3b8-46b8-be49-70087982888d",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "da2f64bd-1d70-4415-a232-95b2f35366ba", // Universe -> Frontend -> To do
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "80deeebb-ea01-4920-aee9-121cd67e91ae",
      name: "Compile markdown to html. Provide GFM and code highlighting support",
      description: "<p>This task takes a set of markdown files and converts them to HTML. It supports GFM with code highlighting. The code highlighting is done using highlight.js.</p><p><br/></p><h4><b>Getting Started</b></h4><p><br/></p><p>Install this grunt plugin next to your project's grunt.js gruntfile with:</p><pre>npm install grunt-markdown --save-dev</pre><p>Then add this line to your gruntfile:</p><pre>grunt.loadNpmTasks('grunt-markdown');</pre>",
      storyPoints: 5,
      creatorId: "f8179313-a809-4e84-927d-722bdcc2dd7f",
      assigneeId: "e312afc1-6073-4198-aa3e-c9aef12cb562",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "8de29e23-537c-4e7d-b207-bdcc37a84856",
      name: "Setup selenium to be run from Grunt",
      description: "<p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-selenium --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-selenium');</pre>",
      storyPoints: 10,
      creatorId: "f235b9a0-0db9-4fca-a964-97d5795a805a",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "da2f64bd-1d70-4415-a232-95b2f35366ba", // Universe -> Frontend -> To do
        "e53be40b-b712-4a5a-8b99-c5b400b9773e", // Universe -> Frontend -> Sprint 0.0.2
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "aad2f3cf-fc3c-4aa1-a88d-73e693cd5047",
      name: "Proof running jasmine-node",
      description: "<p>A task to run your jasmine feature suite using jasmine-node.</p><p><br/></p><h4><b>Getting Started</b></h4><p><br/></p><p>Install this grunt plugin next to your project's grunt.js gruntfile with: npm install grunt-jasmine-node</p><p><br/></p><p>Then add this line to your project's Gruntfile.js grunt file:</p><pre>grunt.initConfig({&#10;<span>  jasmine_node: {&#10;</span><span>    options: {&#10;</span><span>      forceExit: true,&#10;</span><span>      match: '.',&#10;</span><span>      matchall: false,&#10;</span><span>      extensions: 'js',&#10;</span><span>      specNameMatcher: 'spec',&#10;</span><span>      jUnit: {&#10;</span><span>        report: true,&#10;</span><span>        savePath : &#34;./build/reports/jasmine/&#34;,&#10;</span><span>        useDotNotation: true,&#10;</span><span>        consolidate: true&#10;</span><span>      }&#10;</span><span>    },&#10;</span><span>    all: ['spec/']&#10;</span><span>  }&#10;</span><span>});&#10;</span><span>&#10;grunt.loadNpmTasks('grunt-jasmine-node');&#10;</span><span>grunt.registerTask('default', ['jasmine_node']);</span></pre>",
      storyPoints: 13,
      creatorId: "4ede7e15-6124-4b8d-8812-5cfa6d627db3",
      assigneeId: "3770ec25-7919-474a-b517-594cde46a506",
      tags: [
        "261cfeb0-8c47-48ac-acfa-6a59a140235e", // Universe -> Frontend -> Done
        "6fdbebe4-4f1b-4304-86a5-e67fc6333c49", // Universe -> Frontend -> Sprint 0.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "ee25ef4b-fd96-4f71-9227-9fd85f238dc0",
      name: "Compile Handlebars templates for Ember in Grunt",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.0</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-ember-templates --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-ember-templates');</pre>",
      storyPoints: 10,
      creatorId: "77935068-6f4e-47a7-968f-619f2b638102",
      assigneeId: "32a00583-3ae2-40ef-88c4-590a2617c7e2",
      tags: [
        "ff5f7ad5-88f2-42e0-9e4d-25008f3e97e8", // Universe -> Api -> Done
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "a8769e78-818c-4762-9f96-5bda035648eb",
      name: "Convert a set of images into a spritesheet and corresponding CSS variables",
      description: "<p>Convert a set of images into a spritesheet and corresponding CSS variables.</p><p><br/></p><p>A folder of icons processed by grunt-spritesmith:</p><p><br/></p><p>Fork icon + GitHub icon + Twitter icon =</p><p><br/></p><p>generates a spritesheet:</p><p><br/></p><p>Spritesheet</p><p><br/></p><p>and CSS variables (available in CSS, JSON, SASS, SCSS, LESS, Stylus):</p><p><br/></p><p>$fork_offset_x = 0px;</p><p>$fork_offset_y = 0px;</p><p>$fork_width = 32px;</p><p>$fork_height = 32px;</p><p>...</p><p>$github_offset_x = -32px;</p><p>$github_offset_y = 0px;</p><p>$github_width = 32px;</p><p>$github_height = 32px;</p><p>...</p>",
      storyPoints: 10,
      creatorId: "ac9180c4-3e17-4094-9548-d75dab0f7f35",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "705b5f6a-2448-4309-98ed-47acc0806886", // Universe -> Api -> To do
        "8e4fe830-283b-4fea-994c-b421250bfa00", // Universe -> Api -> Sprint 1.0.0
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "eaac129c-9d97-4f12-8802-6b536ee1d51e",
      name: "Lint PHP files",
      description: "<h4><b>Example Gruntfile<\/b><\/h4><h4><br\/><\/h4><pre>var cfg = {&#10;\u00a0 \u00a0 phplint: {&#10;\u00a0 \u00a0 \u00a0 \u00a0 good: [&#34;test\/rsrc\/*-good.php&#34;],&#10;\u00a0 \u00a0 \u00a0 \u00a0 bad: [&#34;test\/rsrc\/*-fail.php&#34;]&#10;\u00a0 \u00a0 }&#10;};&#10;&#10;grunt.initConfig(cfg);&#10;&#10;grunt.loadNpmTasks(&#34;grunt-phplint&#34;);&#10;&#10;grunt.loadTasks(&#34;.\/tasks&#34;);&#10;&#10;grunt.registerTask(&#34;default&#34;, [&#34;phplint:good&#34;]);<\/pre>",
      storyPoints: 34,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "79d1eb29-056b-4c10-a3c5-7c8f6098f34d",
      workRecords: {
        "7bfac1c2-1f88-48a6-ab22-f956d328d8d6": {
          name: "development",
          description: "Adding linting PHP files to build script",
          startDate: new Date("2015-07-22T00:00:00.000Z"),
          time: 30,
          status: "active",
          userId: "76039cc1-96eb-4721-81b3-211b6979d018"
        }
      },
      tags: [
        "d91a31c9-aa3b-458d-9143-9fdfaf088181", // Universe -> Api -> Development
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "c9c1a852-6fd5-4f2b-a8d8-a95ecec67676",
      name: "Combine matching media queries into one media query definition",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-combine-media-queries --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-combine-media-queries');</pre>",
      storyPoints: 13,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "7b2e3ba5-331e-4e0b-b05b-0301b35ca561",
      tags: [
        "cc1ed2c3-a2f9-4d93-9e93-859140f0e6e7", // Universe -> Api -> Review
        "71f1b0f1-cc82-4056-b349-c3422ed814ea", // Universe -> Api -> Sprint 1.0.1
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "13203d29-ad21-4392-aa0c-b2c8fb15355e",
      name: "Read and manipulate HTML with CSS selectors",
      description: "<p>Read and manipulate HTML documents using CSS selectors.</p><p><br/></p><p>Use this task to read and transform your HTML documents. Typical use cases include:</p><ul><li>Read the references from your script or link tags and pass those to concat,uglify, etc automatically.</li></ul><ul><li>Update HTML to remove script references or anything that is not intended for your production builds.</li></ul><ul><li>Add, update, or remove any DOM elements for any reason.</li></ul><p><br/></p><h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.1 and Node &gt;=0.8.</p><pre>npm install grunt-dom-munger --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-dom-munger');</pre>",
      storyPoints: 10,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "076ef7ce-845d-4237-b3ad-1ce0bb5e37f1", // Universe -> Backlog -> Must
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "f5f9a0e3-98e5-4bc0-865f-f33755fd860f",
      name: "Add JavaScript codecoverage tool for Grunt",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.1</p><p><br/></p><p>Install this grunt plugin next to your project's Gruntfile.js with: npm install grunt-istanbul</p><p><br/></p><p>Then add this line to your project's Gruntfile.js gruntfile:</p><pre>grunt.loadNpmTasks('grunt-istanbul');</pre>",
      storyPoints: 5,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "076ef7ce-845d-4237-b3ad-1ce0bb5e37f1", // Universe -> Backlog -> Must
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "6d8c656c-a22b-4761-81b9-847666b23f76",
      name: "Utilise static site generator for Grunt.js, Yeoman and Node.js.",
      description: "<h4><b>Why use Assemble?</b></h4><p><br/></p><ul><li>Most popular site generator for Grunt.js and Yeoman. Assemble is used to build hundreds of web projects, ranging in size from a single page to 14,000 pages (that we're aware of!). Let us know if you use Assemble.</li></ul><ul><li>Allows you to carve your HTML up into reusable fragments: partials, includes, sections, snippets... Whatever you prefer to call them, Assemble does that.</li></ul><ul><li>Optionally use layouts to wrap your pages with commonly used elements and content.</li></ul><ul><li>&#34;Pages&#34; can either be defined as HTML/templates, JSON or YAML, or directly inside the Gruntfile.</li></ul><ul><li>It's awesome. Lol just kidding. But seriously, Assemble... is... awesome! and it's fun to use.</li></ul><p>...and of course, we use Assemble to build the project's own documentation http://assemble.io:</p><p><br/></p><p>For more: hear Jon Schlinkert and Brian Woodward discuss Assemble on Episode 98 of the Javascript Jabber Podcast.</p>",
      storyPoints: 20,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "734f9494-d16f-49e1-9535-fab7a124bb2d", // Universe -> Backlog -> Should
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "d5342b3d-0d4c-43f4-ae47-f1c0e0aa1ed9",
      name: "Lint LESS files using CSS Lint from Grunt",
      description: "<p>There's a plugin that compiles your LESS files, runs the generated CSS through CSS Lint, and outputs the offending LESS line for any CSS Lint errors found.</p><p><br/></p><h4><b>Installing</b></h4><div><p><br/></p></div><pre>npm install grunt-lesslint</pre><p><br/></p><h4><b>Building</b></h4><p><br/></p><ul><li>Clone the repository</li></ul><ul><li>Run npm install</li></ul><ul><li>Run grunt to compile the CoffeeScript code</li></ul><ul><li>Run grunt test to run the specs</li></ul>",
      storyPoints: 13,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "734f9494-d16f-49e1-9535-fab7a124bb2d", // Universe -> Backlog -> Should
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "71d045ee-ad7e-44ff-9723-cd3eea037050",
      name: "Process ES6 module import/export syntax into one of AMD, CommonJS, YUI or globals using the es6-module-transpiler",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-es6-module-transpiler --save-dev</pre><p>To use add the transpile task to your Grunt configuration.</p>",
      storyPoints: 20,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "36ef2805-6c7e-4f4d-8550-65cacc637ea7",
      name: "Find plugin that performs search and replace on files.",
      description: "<h4><b>Installation</b></h4><p><br/></p><p>Install grunt-sed using npm:</p><pre>$ npm install grunt-sed</pre><p>Then add this line to your project's Gruntfile.js:</p><pre>grunt.loadNpmTasks('grunt-sed');</pre>",
      storyPoints: 13,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "5050297c-f8e0-49e6-a9e6-5f0aaede1cae",
      name: "Compile TypeScript using Grunt",
      description: "<h4><b>Documentation</b></h4><p><br/></p><p>You'll need to install grunt-typescript first:</p><pre>npm install grunt-typescript --save-dev</pre><p>or add the following line to devDependencies in your package.json</p><pre>&#34;grunt-typescript&#34;: &#34;&#34;,</pre><p>Then modify your Gruntfile.js file by adding the following line:</p><pre>grunt.loadNpmTasks('grunt-typescript');</pre>",
      storyPoints: 8,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "84c413e4-7031-4569-af59-0763b714cad5", // Universe -> Backlog -> Won't
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "f1c4ff92-7eba-4205-bd76-39f1b337a01a",
      name: "Optimize Google closure compile process with Grunt",
      description: "<h4><b>Google Closure Tools for grunt:</b></h4><p><br/></p><ul><li>Compiler Compile your JS code using the powerful google closure compiler</li></ul><ul><li>Builder Concatenate your JS codebase to a single file, optionally also compile it</li></ul><ul><li>DepsWriter Calculate dependencies of your JS files and generate deps.js</li></ul><ul><li>Build Status</li></ul><p><br/></p><h4><b>Getting Started</b></h4><p><br/></p><p>Install the module with: npm install grunt-closure-tools</p><pre>npm install grunt-closure-tools --save-dev</pre><p>Then register the task by adding the following line to your grunt.js:</p><pre>grunt.loadNpmTasks('grunt-closure-tools');</pre>",
      storyPoints: 30,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "734f9494-d16f-49e1-9535-fab7a124bb2d", // Universe -> Backlog -> Should
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "ce2f83cc-464f-43a7-9a1a-6c0f20e3ee69",
      name: "Sort CSS properties in specific order",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt 0.4.x.</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-csscomb --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-csscomb');</pre>",
      storyPoints: 20,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "b5db7e5c-81e3-4e0b-b5ce-2e8d1446c228",
      name: "Bind Grunt tasks to Git hooks",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-githooks --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-githooks');</pre>",
      storyPoints: 20,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "84c413e4-7031-4569-af59-0763b714cad5", // Universe -> Backlog -> Won't
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "24ffb845-2626-4f9f-8d40-af1ad5f8afe9",
      name: "Interpolate template files with data and save the result to file",
      description: "<h4><b>Getting started</b></h4><h4><br/></h4><p>This plugin requires Grunt v0.4.0+.</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-template --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><p><br/></p><p>grunt.loadNpmTasks('grunt-template');</p>",
      storyPoints: 23,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "00bd0e77-67e5-4f2b-9584-aed71daf0fca",
      name: "Process html files at build time to modify them depending on the release environment",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt ~0.4.1</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-processhtml --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-processhtml');</pre>",
      storyPoints: 20,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      tags: [
        "734f9494-d16f-49e1-9535-fab7a124bb2d", // Universe -> Backlog -> Should
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    },
    {
      id: "563655d2-be47-4efd-b2e7-ebc261d383d7",
      name: "Lint .scss files",
      description: "<h4><b>Getting Started</b></h4><p><br/></p><p>This plugin requires Grunt &gt;= 0.4.0 and scss-lint &gt;= 0.18.0.</p><p><br/></p><p>If you haven't used Grunt before, be sure to check out the Getting Started guide, as it explains how to create a Gruntfile as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:</p><pre>npm install grunt-scss-lint --save-dev</pre><p>Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:</p><pre>grunt.loadNpmTasks('grunt-scss-lint');</pre>",
      storyPoints: 28,
      creatorId: "254bf500-12f8-406d-98ea-ba87bac3d148",
      assigneeId: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      workRecords: {
        "917bf55e-91f6-42ef-b66c-991445891d14": {
          id: "917bf55e-91f6-42ef-b66c-991445891d14",
          name: "development",
          description: "Adding linting SCSS files to build script",
          startDate: new Date("2015-07-22T00:00:00.000Z"),
          time: 40,
          status: "active",
          userId: "76039cc1-96eb-4721-81b3-211b6979d018"
        },
        "c75f58e6-6572-46c9-ae68-bb40953c765a": {
          id: "c75f58e6-6572-46c9-ae68-bb40953c765a",
          name: "bugfixing",
          description: "Fixing linting SCSS files in build script",
          startDate: new Date("2015-07-22T00:00:00.000Z"),
          time: 20,
          status: "active",
          userId: "4baf7c31-84a9-4fd5-9584-c401b0030f5b"
        }
      },
      tags: [
        "5519554f-faf2-4ac2-8e3b-fa4ff5fb6a71", // Universe -> Backlog -> Could
        // no sprints in backlog
      ],
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    }
  ];

  return r.table("Ticket").insert(tickets).run();
});
