
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("Tag").then(function () {

  return r.table("Tag").indexCreate("name").run();

}).then(function () {

  var resources = [
    {
      id: "322b84e5-4122-4a6d-891d-b7d83fb182a0",
      name: "tag",
      description: "Tag",
      status: "active",
      createdAt: new Date("2015-08-22T18:34:53.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

});
