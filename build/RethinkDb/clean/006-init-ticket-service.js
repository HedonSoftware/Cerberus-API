
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("Ticket").then(function () {

  var resources = [
    {
      id: "8073805a-3feb-4877-868a-73e94e5fa68a",
      name: "ticket",
      description: "Ticket",
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

});
