
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("Project").then(function () {

  var resources = [
    {
      id: "cf5061be-b52c-445c-a45b-8f83e2183600",
      name: "project",
      description: "Project",
      status: "active",
      createdAt: new Date("2015-08-22T21:34:32.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

});
