
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("AclRole").then(function () {

  return r.tableCreate("AclResource");

}).then(function () {

  return r.tableCreate("AclRule");

}).then(function () {

  var resources = [
    {
      id: "5cf59503-4e7c-4927-951d-d539cfb615d4",
      name: "aclRoles",
      description: "AclRoles",
      status: "active",
      createdAt: new Date("2015-08-22T12:43:27.000Z"),
      updatedAt: null
    },
    {
      id: "bee26bea-69a1-4e11-b7ac-384a291be11c",
      name: "aclResources",
      description: "AclResources",
      status: "active",
      createdAt: new Date("2015-08-22T15:36:11.000Z"),
      updatedAt: null
    },
    {
      id: "34233ae5-a0e2-45cf-93b5-510a4ac21160",
      name: "aclRules",
      description: "AclRules",
      status: "active",
      createdAt: new Date("2015-08-22T19:04:43.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

}).then(function () {

  var roles = [
    {
      id: "c0fb437f-9c05-4977-a63f-2dad99c88f26",
      name: "apiAdmin",
      description: "API administrator",
      parent: null,
      status: "active",
      createdAt: new Date("2015-08-22T04:21:54.000Z"),
      updatedAt: null
    },
    {
      id: "21395ab6-1041-44f1-95cf-6a7c2f8c6c74",
      name: "apiUser",
      description: "API user",
      parent: "91bf4f95-60a6-4d58-aa72-b49bcb8c5e0b",
      status: "active",
      createdAt: new Date("2015-08-22T01:09:11.000Z"),
      updatedAt: null
    },
    {
      id: "6eeac08b-818b-480f-896f-8e4ea3f57267",
      name: "admin",
      description: "Application administrator",
      parent: null,
      status: "active",
      createdAt: new Date("2015-08-22T23:12:31.000Z"),
      updatedAt: null
    },
    {
      id: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
      name: "user",
      description: "Application user",
      parent: "d16db438-35a9-4dcc-946c-c2460aa7dda7",
      status: "active",
      createdAt: new Date("2015-08-22T12:17:02.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclRole").insert(roles).run();

});
