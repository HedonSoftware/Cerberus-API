
"use strict";

var appRootPath = require("app-root-path");
var r           = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

module.exports = r.tableCreate("User").then(function () {

  return r.table("User").indexCreate("username").run();

}).then(function () {

  var resources = [
    {
      id: "d3a108ae-48d7-4398-b24e-00875f2599fe",
      name: "user",
      description: "User",
      status: "active",
      createdAt: new Date("2015-08-22T13:54:25.000Z"),
      updatedAt: null
    }
  ];

  return r.table("AclResource").insert(resources).run();

}).then(function () {

  var users = [
    {
      id: "8d28e55b-1e77-479f-a3f6-c499c40be3ea",
      username: "unknown",
      password: "",
      email: "",
      firstName: "Unknown",
      middleName: null,
      lastName: "Unknown",
      avatar: "/static/images/avatars/00.png",
      status: "active",
      roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
      createdAt: new Date("2015-08-22T04:23:12.000Z"),
      updatedAt: null
    },
    {
      id: "9f894edc-23cb-4704-aa65-39e90d3a078a",
      username: "demo",
      password: "0279ea92252a164ca3384ea0846d0ad82dd3c795",
      email: "demo@hedonsoftware.com",
      firstName: "Demo",
      middleName: "D",
      lastName: "Demo",
      avatar: "/static/images/avatars/03.png",
      status: "active",
      roleId: "0c6acdbd-525d-4e15-84d2-7f630b7a6548",
      createdAt: new Date("2015-08-22T14:11:43.000Z"),
      updatedAt: null
    },
    {
      id: "e066e6ad-95f1-4ebc-b133-e52702459b35",
      username: "admin",
      password: "904ac5b21e30834611fff38edd410cb36a7b88e1",
      email: "admin@hedonsoftware.com",
      firstName: "Admin",
      middleName: "A",
      lastName: "Admin",
      avatar: "/static/images/avatars/04.png",
      status: "active",
      roleId: "6eeac08b-818b-480f-896f-8e4ea3f57267",
      createdAt: new Date("2015-08-22T22:24:15.000Z"),
      updatedAt: null
    }
  ];

  return r.table("User").insert(users).run();

});
