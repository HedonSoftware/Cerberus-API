#!/usr/bin/env node

/**
 * Cerberus API (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/Cerberus-API for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md Proprietary software
 */

/**
 * Setup database script can be run against any database that is
 * defined in configuration file as it will be automatically
 * fetched based on NODE_ENV system variable.
 *
 * It can be used in following way:
 * - setting up testing database
 * $ export NODE_ENV=test && build/setupDb.js
 * - setting up development database
 * $ export NODE_ENV=app && build/setupDb.js
 */

"use strict";

var appRootPath = require("app-root-path");
var fs          = require("fs");

// getting possible command line args
var scriptsDir = "clean";
if (process.argv[2]) {
  scriptsDir = process.argv[2];
}

var config = require(appRootPath + "/lib/config/config");
var dbName = config.get("rethinkDb").db;
var files  = fs.readdirSync(__dirname + "/" + scriptsDir);
var async  = require("async");

var r = require(appRootPath + "/lib/dataProvider/rethinkDbDataProvider");

console.log("------------------------------------------------");
console.log("|                   DB Build                   |");
console.log("------------------------------------------------");

r.dbList().run().then(function (list) {
  if (list.indexOf(dbName) > -1) {
    console.log("Dropping database " + dbName);
    return r.dbDrop(dbName).run();
  }
}).then(function() {
  console.log("Creating empty database " + dbName);
  return r.dbCreate(dbName).run();
}).then(function() {
  async.mapSeries(
    files,
    function (file, callback) {

      console.log("INFO: Executing file " + scriptsDir + "/" + file);

      require(__dirname + "/" + scriptsDir + "/" + file).then(function() {
        return callback(null, "ok");
      });
    },
    function (err) {
      if (err) {
        console.log(err);
        throw new Error("Error ocurred" + err);
      }

      console.log("Build completed!");
      process.exit(0);
    }
  );
});
