
Cerberus API
============

[![Build Status](https://gitlab.com/HedonSoftware/Cerberus-API/badges/v2.0.0/build.svg)](https://gitlab.com/HedonSoftware/Cerberus-API/builds)

License
-------

Code in this repository (Cerberus API source code - the "Software") is intellectual property of HedonSoftware Limited and is protected by copyright laws and international treaty provisions. Therefore, you must treat the Software like any other copyrighted material.

Summary of license:

- **Software is intellectual property of HedonSoftware Limited.**
- **Commercial use in not allowed.**
- **The software is licensed, not sold.**
- **Source-code cannot be resold or distributed.**
- **Feel free to use the software for non-commercial purposes.**
- **If you intend to use Cerberus API for commercial purposes, please contact us at info@hedonsoftware.com.**
- **By contributing to this repository you agree to the Cerberus API Contributor Agreement available in [CONTRIBUTOR_AGREEMENT.md](https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/CONTRIBUTOR_AGREEMENT.md) file.**

Copyright (c) 2014-2016, HedonSoftware Limited

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

**For more information please refer to [LICENSE.md](https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/LICENSE.md) file.**

Installation
------------

![Docker Logo](https://d3oypxn00j2a10.cloudfront.net/0.17.3/images/pages/brand_guidelines/small_v.png)

The easiest way to install Cerberus API is to use Docker container. Please follow this link to get more details how to do it:
[Docker registry: HedonSoftware/Cerberus API](https://registry.hub.docker.com/u/hedonsoftware/cerberus-api/)

otherwise here's a step by step guide how to set up your environment:

Prerequistments:
- Node.js 6.0.0+
- RethinkDb 2.3+

Git clone this repository:

```
git clone https://gitlab.com/HedonSoftware/Cerberus-API.git
```

Install all dependencies:

```
npm install
```

Edit config in `config/app.json' to have correct details of your current platform. Load database structure using CLI tool:

```
export NODE_ENV=app && node ./build/setupDb.js

// or export NODE_ENV=app && node build/setupDb.js test-sql
// to init project with sample data
```

Run the application(from main repository dir):

```
node app.js
```

After installation you should be able to start using Cerberus API.

Usage
-----

Cerberus API is an RESTful API with multiple resources. You can query the API with usage of prepared URL.

List of available resources:
- api-accounts
- boards
- comments
- lanes
- lane-tag-links
- projects
- sprints
- tags
- tickets
- ticket-activities
- ticket-links
- ticket-statuses
- time-logs
- users

Sample queries:

Get all comments:

GET: http://your-url.com/tickets/:ticketId/comments

Get all boards for project 1:

GET: http://your-url.com/projects/:projectId/boards?conditions[projectId]=1

Get all projects created after 1st of Jan 2014:

GET: http://your-url.com/projects?conditions[createdAt][gt]=01.01.2014

Get all tickets ordered descending by created date:

GET: http://your-url.com/tickets?order[createdAt]=desc

Get last 3 sprints ordered descending by created date:

GET: http://your-url.com/tickets?order[createdAt]=desc&limit=3

Get ticket's comments with joined with user

GET: http://your-url.com/ticket/:ticketId/comments/:commentId?fields[]=t.id&fields[]=t.name&fields[]=t.status&fields[]=t.userId&fields[u.username]=user&joins[]=user

The table below lists the standard methods that have a well-defined meaning for all resources and collections.


|  Method |   Scope    |     reThinkDb            |              Semantics                                    |
|:-------:|:----------:|:------------------------:|:---------------------------------------------------------:|
|   GET   | collection |        r.table()         | Retrieve all resources in a collection                    |
|   GET   |  resource  |     r.table().get()      | Retrieve a single resource                                |
|   HEAD  | collection |           -              | Retrieve all resources in a collection (header only)      |
|   HEAD  |  resource  |           -              | Retrieve a single resource (header only)                  |
|   POST  | collection |    r.table().insert()    | Create a new resource in a collection (drop id if passed) |
|   PUT   |  resource  |    r.table().get() &&    | Replace resource                                         |
|         |            |     record.replace()     |                                                           |
|  PATCH  |  resource  | r.table().get().update() | Update a resource                                         |
|  DELETE |  resource  |     r.get().delete()     | Delete a resource                                         |
| OPTIONS |     any    |           -              | Return available HTTP methods and other options           |

For more example please refer to test/functional where you can find single test suite file
per resource.

Todo
----

* Rework Route, Service and Molk modules
* Implement HAWK authentication

Contributing
------------

We would love to see your contributions to this repository, so if you found a bug, created some fix/functionality or just have an idea, please feel free to create pull request or issue in GitHub. Please remember that by contributing to this repository you agree to the Cerberus API Contributor Agreement available in [CONTRIBUTOR_AGREEMENT.md](https://gitlab.com/HedonSoftware/Cerberus-API/blob/master/CONTRIBUTOR_AGREEMENT.md) file.

Thanks in advance,

HedonSoftware team
